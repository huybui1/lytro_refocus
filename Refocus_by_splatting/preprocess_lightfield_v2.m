function output = preprocess_lightfield_v2(img_folder, calib_folder, filename)
% read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau
[LFP, ExtraSections] = LFReadLFP([img_folder, filename]);
img = LFP.RawImg;
img = double(img);

GAMMA = LFP.Metadata.image.color.gamma;
options.RAW_SIZE_COL = LFP.ImgSize(2);
options.RAW_SIZE_ROW = LFP.ImgSize(1);
options.GAMMA = GAMMA;

% vignetting correction
load([calib_folder, 'vig.mat']);
img = img.*vig;

% rescale
img = img./max(img(:));

% gamma correction
img  = img.^(GAMMA);

% hot pixel correction
load([calib_folder, 'mask_black.mat']);
hot_img = hotPixelCorrection(img, mask_black, options);

% demosaic
demo_img = demosaic(uint16(hot_img.*double(intmax('uint16'))), 'bggr');

% rotate image
load([calib_folder, 'rotation.mat']);
rotated_img = imrotate(demo_img, an*180/pi, 'bicubic');

% crop
if offset.Y1(1)>offset.Y3(1)
    rotated_img = rotated_img(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
else
    rotated_img = rotated_img(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
end

output = rotated_img;
