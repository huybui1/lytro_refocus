% v2 - read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau

clear all
clc
close all

create_video = 1;
write_images_to_disk = 1;

% image file
img_name = 'IMG_0001.lfp';
img_folder = './my_camera/raw_folder/Nov_16/';
calib_folder = './my_camera/calib_folder/';
output_folder = './Results/Nov_16/';
load([calib_folder, 'center.mat']);




center_rows = height;
center_cols = width;

processed_LF = preprocess_lightfield_v4(img_folder, calib_folder, img_name);
processed_LF = double(processed_LF);
% processed_LF = processed_LF./max(processed_LF(:));

% figure;
% imagesc(processed_LF);
% title('Preprocessed raw light field image');

image_height = size(processed_LF, 1);
image_width = size(processed_LF, 2);
map = [calib_folder, 'map_', num2str(image_height'), '_', num2str(image_width),'.mat'];

if ~exist(map, 'file')
  tic
  generate_centers_array(image_height, image_width, centerX, centerY, center_rows, center_cols, calib_folder);   
  toc
end
load(map);

% kernel
rad = 4;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
% kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;

if (create_video == 1)
    vidObj = VideoWriter([output_folder, img_name(1:end-4), '_rad_', num2str(rad),'.avi'], 'Uncompressed AVI');
    vidObj.FrameRate = 2;
    vidObj.VideoCompressionMethod
    open(vidObj);

    fig100 = figure(100);
    set(fig100, 'Visible', 'off');
end

f = 25e-6;
% for F = 10e-6:10e-6:100e-6
F = 30e-6;
    tic
     
    result = splat(single(processed_LF), single(u), single(v), ...
                   single(kernel), single(F), single(f));
    toc
   % histogram equalization   
    final_result = uint16(result*double(intmax('uint16')));    
   
    if (create_video == 1)
        figure(100); 
        imshow(final_result); 
        title(['Refocus distance = ', num2str(F)]);
        axis image;
        axis off;
        pause(0.01);
        currFrame = getframe;
        writeVideo(vidObj, currFrame);
    end
    
        
    if (write_images_to_disk == 1)
        if (F >= 0)
            imwrite(final_result, [output_folder, img_name(1:end-4), '_' , num2str(F*1e+6),'um_rad_', num2str(rad),'.png'] );
        else
            imwrite(final_result, [output_folder, img_name(1:end-4), '__' , num2str(abs(F)*1e+6),'um_rad_', num2str(rad),'.png'] );
        end
    end
% end %for F

if (create_video == 1)
    close(vidObj);
end