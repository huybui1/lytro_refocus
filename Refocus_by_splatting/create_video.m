clear all
clc
close all

aviobj = avifile('IMG_0001_rad_4.avi', 'compression', 'None', 'fps', 1);

fig100 = figure(100)


for d = 10:10:200
   filename = ['IMG0001_rad_4/IMG_0001_', num2str(d),'um_rad_4.png'];
   img = imread(filename);
   figure(100);
   imagesc(img);
   title(['Focus distance = ', num2str(d), 'um']);
   axis image;
   axis off;
   pause(0.1);
   aviobj = addframe(aviobj, fig100);
end

aviobj = close(aviobj);


    