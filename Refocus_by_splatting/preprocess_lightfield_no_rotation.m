function output = preprocess_lightfield_no_rotation(img_folder, calib_folder, img_name)

% read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau
[LFP, ExtraSections] = LFReadLFP([img_folder, img_name]);

% metadata
BLACK_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.black.gr;
WHITE_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.white.gr;
% color correct
GAMMA = LFP.Metadata.image.color.gamma;
COL_MATRIX = reshape(LFP.Metadata.image.color.ccmRgbToSrgbArray, 3, 3);
COL_BALANCE = [LFP.Metadata.image.color.whiteBalanceGain.b, ...
               LFP.Metadata.image.color.whiteBalanceGain.gb;
               LFP.Metadata.image.color.whiteBalanceGain.gr, ...
               LFP.Metadata.image.color.whiteBalanceGain.r];        

% decode img
img = LFP.RawImg;
img = double(img);

white_img = imread([calib_folder, 'white_image.tif']);
white_img = double(white_img);
white_img = (white_img./max(white_img(:)))*4095; %12bit

% linearize
white_img = (white_img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);
img = (img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);

% Devignette
img = img./white_img;

%Clip
img = min(1, max(0, img));

% White balancing
wb_img = white_balance(img, COL_BALANCE);

% demosaic
demo_img = demosaic(uint16((wb_img./max(wb_img(:))).*double(intmax('uint16'))), 'bggr');
demo_img = double(demo_img);
demo_img = demo_img./ double(intmax('uint16'));
% color correct
% col_img = LFColourCorrect(demo_img, COL_MATRIX, COL_BALANCE, GAMMA);
col_img = color_correct(demo_img, COL_MATRIX, COL_BALANCE, GAMMA);
% col_img = col_img./max(col_img(:));

col_img = LFHistEqualize(col_img);


output = col_img;
