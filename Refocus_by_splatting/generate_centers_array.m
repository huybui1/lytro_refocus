function [cx, cy, u, v] = generate_centers_array3(img_height, img_width, centerX, centerY, center_rows, center_cols, dir) 
  cx = zeros(img_height*img_width, 1);
  cy = zeros(img_height*img_width, 1);
  
  [xx, yy] = meshgrid(1:img_width, 1:img_height);

  
  % rearrange centers
  [centerX, centerY, center_rows, center_cols] = rearrange_centers(centerX, centerY, center_rows, center_cols);

  parfor index = 1:img_height*img_width
    x = xx(index);
    y = yy(index);
    [cx1, cy1] = find_closest_point(x, y, centerX, centerY);
    
    cx(index) = cx1;
    cy(index) = cy1;
  end

  cx = reshape(cx, img_height, img_width);
  cy = reshape(cy, img_height, img_width);
  u = xx - cx;
  v = yy - cy;
   
  save([dir, 'map_',num2str(img_height), '_', num2str(img_width),'.mat'], ...
       'u', 'v', 'cx', 'cy');
         
end