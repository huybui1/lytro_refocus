function result = hotPixelCorrection(img,mask_black,options)
    
    RAW_SIZE_ROW = options.RAW_SIZE_ROW;
    RAW_SIZE_COL = options.RAW_SIZE_COL;
        
    mask_black(1:2,:) = 0;
    mask_black(:,1:2) = 0;
    mask_black(end-1:end,:) = 0;
    mask_black(:,end-1:end) = 0;
    index = find(mask_black == 1);
    
    [Y X] = ind2sub([RAW_SIZE_ROW RAW_SIZE_COL],index);
    
    img(index) = 0;
    
    cnt = 1;
    sum = 0;
    for col=-1:1:1
        for row=-1:1:1
            w = exp(-sqrt(col^2+row^2));
            sum = sum + exp(-sqrt(col^2+row^2));
            ind = sub2ind([RAW_SIZE_ROW RAW_SIZE_COL],Y+row,X+col);
            img(index) = img(index) + img(ind).*w;    
            cnt = cnt+1;            
        end
    end
    
    img(index) = img(index)./sum;
    result = img;
    
end