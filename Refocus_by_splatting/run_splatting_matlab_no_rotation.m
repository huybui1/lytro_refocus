clear all
clc
close all

% image file
image_name = 'IMG_0001.lfp';
img_folder = './my_camera/raw_folder/Nov_16/';
calib_folder = './my_camera/calib_folder/';
result_folder = './Results/no_rotation/';
% load([calib_folder, 'center.mat']);
load([calib_folder, 'microlens_center_list.mat']);
centerX = center_list(1,:);
centerY = center_list(2,:);
processed_LF = preprocess_lightfield_no_rotation(img_folder, calib_folder, image_name);

%crop
% processed_LF = double(processed_LF(1350:1477, 600:727, :));
processed_LF = double(processed_LF);

figure;
imagesc(processed_LF/max(processed_LF(:))); 
title('Preprocessed raw light field image');


height = size(processed_LF, 1);
width = size(processed_LF, 2);
map = ['map_no_rotation_', num2str(height'), '_', num2str(width),'.mat'];

if ~exist(map, 'file')
  tic
  generate_centers_array_no_rotation(height, width, centerX, centerY, calib_folder);    
  toc
end
load([calib_folder, map]);

% kernel
rad = 4 
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
% kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;

f = 25e-6;
F = 5e-6;
% for F = -10e-6:1e-6:-6e-6
    tic
    D = 1 + F/f;

    result = zeros(height, width, 3);
    weight = zeros(height, width);

    for h = 1:height
        for w = 1:width
            sx = w - D*u(h, w);
            sy = h - D*v(h, w);
            I1 = processed_LF(h, w, 1);
            I2 = processed_LF(h, w, 2);
            I3 = processed_LF(h, w, 3);
                    
            sy = round(sy);
            sx = round(sx);
            for j = max(1, sy - rad):min(height, sy + rad)
                for i = max(1, sx - rad) : min(width, sx + rad)
                   wi = kernel(rad + j - sy + 1, rad + i - sx + 1);
                   result(j, i, 1) = result(j, i, 1) + I1*wi;     
                   result(j, i, 2) = result(j, i, 2) + I2*wi;     
                   result(j, i, 3) = result(j, i, 3) + I3*wi;
                   weight(j, i) = weight(j, i) + wi;
                end
            end                
        end
    end

    for j = 1:height
        for i = 1:width
            if (weight(j,i) > 0)
                result(j, i, 1) = result(j, i, 1)./weight(j, i);
                result(j, i, 2) = result(j, i, 2)./weight(j, i);
                result(j, i, 3) = result(j, i, 3)./weight(j, i);
            end
        end
    end

    normalized_result = result./max(result(:));

    figure; imshow(normalized_result); title(['refocus distance = ', num2str(F)]);
    figure; imagesc(weight);
    
    if (F >= 0)
      imwrite(normalized_result, [result_folder, image_name(1:end-5), '_' , num2str(F*1e+6),'um_rad_', num2str(rad),'_no_rotation.png'] );
    else
      imwrite(normalized_result, [result_folder, image_name(1:end-5), '__' , num2str(abs(F)*1e+6),'um_rad_', num2str(rad),'_no_rotation.png'] );
    end
    toc
% end %for F