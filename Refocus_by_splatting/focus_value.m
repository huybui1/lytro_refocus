function FV = focus_value(image)
% compute the focus value of an image

% compute gradient with Sobel filter
h1 = repmat([-1.0 2.0 1.0], 3 ,1);
Ix = imfilter(image, h1);

h2 = repmat([-1.0; 2.0; 1.0], 1, 3);
Iy = imfilter(image, h2);

FV = sum(Ix(:).^2 + Iy(:).^2);
FV = FV/prod(size(image));
      