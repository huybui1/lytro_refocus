function out = color_correct(in, ColMatrix, ColBalance, Gamma)
  inSize = size(in);
  ndims = numel(inSize);
  in = reshape(in, [prod(inSize(1:ndims-1)), 3]);
  temp = in * ColMatrix;
  temp = reshape(temp, [inSize(1:ndims-1), 3]);
  
  % saturating
  satlevel = ColBalance*ColMatrix;
  satlevel = min(satlevel);
  
  temp = min(satlevel, max(0, temp));
%   temp  = temp./max(temp(:));
%   temp = min(1, max(0, temp));
  out = temp.^Gamma;
  
end