function output = preprocess_lightfield(img_folder, calib_folder, filename)

GAMMA = 0.41666;
options.RAW_SIZE_COL = 3280;
options.RAW_SIZE_ROW = 3280;
options.GAMMA = GAMMA;

% calibration data
% calib_folder = './downloaded_data/calib_folder/';

img = imread([img_folder, filename]);
img = double(img);

% vignetting correction
load([calib_folder, 'vig.mat']);
img = img.*vig;

% rescale
img = img./max(img(:));

% gamma correction
img  = img.^(GAMMA);

% hot pixel correction
load([calib_folder, 'mask_black.mat']);
hot_img = hotPixelCorrection(img, mask_black, options);

% demosaic
demo_img = demosaic(uint16(hot_img.*double(intmax('uint16'))), 'bggr');

% rotate image
load([calib_folder, 'rotation.mat']);
rotated_img = imrotate(demo_img, an*180/pi, 'bicubic');

% crop
if offset.Y1(1)>offset.Y3(1)
    rotated_img = rotated_img(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
else
    rotated_img = rotated_img(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
end

output = rotated_img;
