function [cx, cy, u, v] = generate_centers_array_no_rotation(img_height, img_width, centerX, centerY, dir) 
  cx = zeros(img_height*img_width, 1);
  cy = zeros(img_height*img_width, 1);
  u  = zeros(img_height*img_width, 1);
  v = zeros(img_height*img_width,1);
  
  [xx, yy] = meshgrid(1:img_width, 1:img_height);

  
  % rearrange centers
  for index = 1:img_height*img_width
    x = xx(index);
    y = yy(index);
    [u1, v1, cx1, cy1] = getUV(x, y, centerX, centerY);
    
    cx(index) = cx1;
    cy(index) = cy1;
    u(index) = u1;
    v(index) = v1;
  end

  cx = reshape(cx, img_height, img_width);
  cy = reshape(cy, img_height, img_width);
  u = reshape(u, img_height, img_width);
  v = reshape(v, img_height, img_width);
   
  save([dir, 'map_no_rotation_',num2str(img_height), '_', num2str(img_width),'.mat'], ...
       'u', 'v', 'cx', 'cy');
         
end