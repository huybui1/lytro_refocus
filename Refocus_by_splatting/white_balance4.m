function out = white_balance4(in, col_balance_matrix)
  inSize = size(in);
  out = zeros(inSize);
  
  out(:,:,1) = in(:,:,1)*col_balance_matrix(1);
  out(:,:,2) = in(:,:,2)*col_balance_matrix(2);
  out(:,:,3) = in(:,:,3)*col_balance_matrix(3);
end
