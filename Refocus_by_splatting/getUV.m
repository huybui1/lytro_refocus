function [u, v, cx, cy] = getUV(x, y, centerX, centerY)
dist = (centerX - x).^2 + ...
       (centerY - y).^2;
[minDist, index] = min(dist);
cx = centerX(index);
cy = centerY(index);
u = x - centerX(index);
v = y - centerY(index);
end
