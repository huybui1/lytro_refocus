#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>
#include <float.h>

#define INDEX2D(x, y, height) ((x)*(height) + (y))
void find_closest_point(float x, float y, 
                        const float *centerX,
						const float *centerY,
						float *cx,
						float *cy,
						int height,
						int width) {
  // find point on grid (centerX, centerY) closest to (x, y)
  
  // x direction
  int left = 0;
  int right = width - 1;
  int mid;
  while (left < right) {
    mid = (left + right)/2;
	
	if (centerX[INDEX2D(mid, 0, height)] < x)
	  left = mid + 1;
	else
	  right = mid - 1;
  }
  right = (mid < (width - 2))? (mid + 2):width-1;
  left =  (mid > 1) ? (mid - 2):0;
  
  // y direction
  int up = 0;
  int down = height - 1;
  
  while (up < down) {
    mid = (up + down)/2;
	
	if (centerY[INDEX2D(0, mid, height)] < y)
	  up = mid + 1;
	else
	  down = mid - 1;
  }
  down = (mid < (height - 2))? (mid + 2):height-1;
  up  = (mid > 1)? (mid - 2): 0;
  
  float min_distance2 = FLT_MAX;
  float distance2 = 0.0f;
  for (int i = left; i <= right; ++i) {
    for (int j = up; j <= down; ++j) {
	  float cur_x = centerX[INDEX2D(i, j, height)];
	  float cur_y = centerY[INDEX2D(i, j, height)];
	  distance2 = (cur_x - x) * (cur_x - x) + (cur_y - y) * (cur_y - y);
	  if (distance2 < min_distance2) {
	    *cx = cur_x;
		*cy = cur_y;
		min_distance2 = distance2;
	  }
	}
  }					
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // [cx, cy]  = find_closest_point(x, y, centerX, centerY)

  // get x and y
  float x = *((float *)mxGetData(prhs[0]));
  float y = *((float *)mxGetData(prhs[1]));

  int height = mxGetM(prhs[2]);
  int width = mxGetN(prhs[3]);

  float *center_x = (float *) mxGetData(prhs[2]);
  float *center_y = (float *)mxGetData(prhs[3]);

  // output
  plhs[0] = mxCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  float *cx = (float *)mxGetData(plhs[0]);

  plhs[1] = mxCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  float *cy = (float *)mxGetData(plhs[1]);

  find_closest_point(x, y, center_x, center_y, cx, cy, height, width);
}