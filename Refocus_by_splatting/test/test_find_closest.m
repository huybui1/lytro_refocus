load ..\my_camera\calib_folder\center.mat
length_x = length(centerX);
centerX1 = reshape(centerX(1:length_x/2), height, width);

length_y = length(centerY);
centerY1 = reshape(centerY(1:length_y/2), height, width);


w = 100;
h = 200;

cx1 = zeros(h, w); cy1 = zeros(h, w);
cx2 = zeros(h, w); cy2 = zeros(h, w);
cx3 = zeros(h, w); cy3 = zeros(h, w);

disp('Find closest point');
tic
for y = 1:h
    for x = 1:w
        [cx1(y, x), cy1(y,x)] = find_closest_point(x, y, centerX1, centerY1);
    end
end
toc

disp('Find closest point brute force');
tic
for y = 1:h
    for x = 1:w
        [cx2(y, x), cy2(y,x)] = find_closest_point_bruteforce(x, y, centerX1, centerY1);
    end
end
toc

disp('Find closest point MEX');
tic
for y = 1:h
    for x = 1:w
        [cx3(y, x), cy3(y,x)] = find_closest_point_mex(single(x), single(y), single(centerX1), single(centerY1));
    end
end
toc


            