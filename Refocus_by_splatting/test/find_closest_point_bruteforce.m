function [cx, cy] = find_closest_point_bruteforce(x, y, centerX, centerY)
    dist = (centerX(:) - repmat(x, length(centerX(:)), 1)).^2 + ...
           (centerY(:) - repmat(y, length(centerY(:)), 1)).^2;
    [minDist, index] = min(dist);
    cx = centerX(index);
    cy = centerY(index);
end
