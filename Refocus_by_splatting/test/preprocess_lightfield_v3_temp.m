function output = preprocess_lightfield_v3(img_folder, calib_folder, img_name)
% read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau
[LFP, ExtraSections] = LFReadLFP([img_folder, img_name]);

% metadata
BLACK_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.black.gr;
WHITE_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.white.gr;
% color correct
GAMMA = LFP.Metadata.image.color.gamma^0.5;
COL_MATRIX = reshape(LFP.Metadata.image.color.ccmRgbToSrgbArray, 3, 3);
COL_BALANCE = [LFP.Metadata.image.color.whiteBalanceGain.r, ...
               LFP.Metadata.image.color.whiteBalanceGain.gb, ...
               LFP.Metadata.image.color.whiteBalanceGain.b];
           

% decode img
img = LFP.RawImg;
img = double(img);
% load white img
white_img = loadWhiteImage([calib_folder, 'white_folder/'], size(img,1), size(img,2));
white_img = (white_img./max(white_img(:)))*4095;

% white_img = double(white_img);
% white_img = white_img./max(white_img(:));
% white_img = white_img*4095; % 12-bit
white_img = (white_img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);
img = (img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);

% Devignette
img = img./white_img;

%Clip
img = min(1, max(0, img));
% img = img.^GAMMA;
% demosaic
demo_img = demosaic(uint16(img.*double(intmax('uint16'))), 'bggr');
demo_img = double(demo_img);

demo_img = demo_img./ double(intmax('uint16'));
% demo_img = LFHistEqualize(demo_img);
% color correct
% col_img = LFColourCorrect(demo_img, COL_MATRIX, COL_BALANCE, 1);
hist_img = LFHistEqualize(col_img);

% rotate image
load([calib_folder, 'rotation.mat']);
rotated_img = imrotate(hist_img, an*180/pi, 'bicubic');

% crop
if offset.Y1(1)>offset.Y3(1)
    rotated_img = rotated_img(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
else
    rotated_img = rotated_img(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
end

%clip
rotated_img = min(1, max(rotated_img, 0));
output = rotated_img;
