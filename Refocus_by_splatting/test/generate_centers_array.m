function [cx, cy, u, v] = generate_centers_array(height, width, centerX, centerY) 
  u = zeros(height, width);
  v = zeros(height, width);
  cx = zeros(height, width);
  cy = zeros(height, width);
  
  for h = 1:height
      for w = 1:width
          [ui, vi, cxi, cyi] = getUV(w, h, centerX, centerY);
          u(h, w) = ui;
          v(h, w) = vi;
          cx(h, w) = cxi;
          cy(h, w) = cyi;
      end
  end
   
  save(['map_',num2str(height), '_', num2str(width),'.mat'], ...
       'u', 'v', 'cx', 'cy');
         
end