function [cx, cy] = find_closest_point(x, y, centerX, centerY)
    disp('Find closest point');
    [height, width] = size(centerX);
    
    % find region closed to (cx , cy)
    left = 1;
    right = width;
    
    while (left < right) 
        mid = floor((left + right)/2);
%         centerX(1, mid)
        if (centerX(1, mid) < x)
            left = mid + 1;
        else
            right = mid - 1;
        end
    end
    right = left - 2;
    left = right + 2;
    
    
    up = 1;
    down = height;
    column = mid;
    
    while (up < down)
        mid = floor((up + down)/2);
        if (centerY(mid, column) < y)
            up = mid + 1;
        else
            down = mid - 1;
        end
    end
    up = down - 2;;
    down = up + 2;
    
    candidateX = centerX(max(up, 1):min(down, height), max(left, 1):min(right, width));
    candidateY = centerY(max(up, 1):min(down, height), max(left, 1):min(right, width));
    
    figure; imagesc(candidateX); title('candidate X')
    figure; imagesc(candidateY); title('candidate Y')
    pause
    
    dist = (candidateX(:) - repmat(x, length(candidateX(:)), 1)).^2 + ...
           (candidateY(:) - repmat(y, length(candidateY(:)), 1)).^2;
    [minDist, index] = min(dist);
    cx = candidateX(index);
    cy = candidateY(index);
end
