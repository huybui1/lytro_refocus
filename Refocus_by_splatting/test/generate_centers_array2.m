function generate_centers_array2(height, width, centerX, centerY) 
  u = zeros(height*width, 1);
  v = zeros(height*width, 1);
  cx = zeros(height*width, 1);
  cy = zeros(height*width, 1);
  
  [hh, ww] = meshgrid(1:height, 1:width);

  parfor index = 1:height*width
      [ui, vi, cxi, cyi] = getUV(hh(index), ww(index), centerX, centerY);
          u(index) = ui;
          v(index) = vi;
          cx(index) = cxi;
          cy(index) = cyi;
  end
  reshape(u, height, width);
  reshape(v, height, width);
  reshape(cx, height, width);
  reshape(cy, height, width);
   
%   save(['map_',num2str(height), '_', num2str(width),'.mat'], ...
%        'u', 'v', 'cx', 'cy');
         
end