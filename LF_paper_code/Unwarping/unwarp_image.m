clear all
clc
close all



% path to particle system toolbox
addpath('../../DownloadedCodes/particles_2_1/');
addpath('../../DownloadedCodes/tpsWarp/');
PIXEL_PITCH = 1.4e-3; % mm
cx = 3280/2;
cy = 3280/2;
IMAGE_DIR = '../Results/June_19/';
RESULTS_DIR = '../Results/June_19/';

img_no = 16;
date = 'Dec23';
IMG_FILENAME = [date, '_', num2str(img_no, '%04i'),'_refocused_rad_6_with_captured_depth.png'];
DIST_FILENAME = ['Upsampled_distance_', date, '_', num2str(img_no, '%04i'), '.mat'];
DEPTH_FILENAME = ['Upsampled_depth_', date, '_', num2str(img_no, '%04i'), '.mat'];
CALIBRATION_DIR = ['../../Lytro_plus_depth/Calibration/',  date, '/'];
CALIB_FILE = 'IntParamLF.mat';


% estimate parameters of the light field camera
[F, L] = get_LF_calib_params([CALIBRATION_DIR, CALIB_FILE]);


input_img = imread([IMAGE_DIR,IMG_FILENAME]);
load([IMAGE_DIR, DIST_FILENAME]);
distance_map = upsampled_distance;

load([IMAGE_DIR, DEPTH_FILENAME]);
depth_map = upsampled_depth;

figure(2);
imagesc(distance_map);
% title('Input distance map');
axis image;
axis off;
% colormap gray;

figure(1);
imshow(input_img);
% title('Input image');
axis image;
axis off;

disp('Please select 4 corners of the document region');
hold on
corners  = [];
for i = 1:4
  [x y]  = ginput(1);
  plot(x, y, 'b+');
  hold on;
  corners = [corners; x y];
end
hold off

grid_width = 10;
grid_height = 10;
% grid_pts = create_grid(corners, grid_height, grid_width);

grid_x = linspace(floor(min(corners(:, 1))), ceil(max(corners(:, 1))), grid_width);
grid_y = linspace(floor(min(corners(:, 2))), ceil(max(corners(:, 2))), grid_height);
[grid_pts_x, grid_pts_y] = meshgrid(grid_x, grid_y);
grid_pts = [grid_pts_x(:), grid_pts_y(:)];

% Create original mesh and particle system for physical-based unwarping
%
% gravity: [0, 0, -0.4]
% aerodynamic resistance (drag): 0.1
% axes limits: +/- 5

% particle_system(gravity, drag, axes_limit, collision_w_ground,
% collision_dectection_threshold, restitution_coef, display_axis, 
% apply_horizontal_drag, horizontal_drag_coefficient)
Particle_System = particle_system([0, 0, -1], 0.02, 0.02, true, 0.0, 0.02, true, true, 0.02);
local_depth = depth_map(min(round(grid_pts(:,2))) : max(round(grid_pts(:,2))), ...
              min(round(grid_pts(:,1))) : max(round(grid_pts(:,1))));

% min_depth = min(local_depth(:));
max_depth = max(local_depth(:));

for row = 1:grid_height
    for col = 1:grid_width
        index = (row - 1)* grid_width + col;
        u = round(grid_pts(index, 1));
        v = round(grid_pts(index, 2));
        d = L - distance_map(v, u);
        % map to 3d
%         x = -(u - cx) * PIXEL_PITCH * depth_map(v, u) / d;
%         y = -(v - cy) * PIXEL_PITCH * depth_map(v, u) / d;
        x = (u - cx) * PIXEL_PITCH * depth_map(v, u) / d;
        y = (v - cy) * PIXEL_PITCH * depth_map(v, u) / d;
        z = max_depth - depth_map(v, u);
%         z = depth_map(v, u) - min_depth;
        samples_u(row, col) = u;
        samples_v(row, col) = v;
        
        samples_x(row, col) = x;
        samples_y(row, col) = y;
        samples_z(row, col) = z;

        % create a particle
        % mass: 1
        % initial position: 3d position of the vertice
        % initial velocity: [0, 0, 0]
        % not fixed
        % lifespan: inf
        Particles{row, col} = particle(Particle_System, 1, ...
            [x, y, z], ...
            [0, 0, 0], false, inf);
    end
end


%% display original grid
figure(1);
hold on;
for row = 1:grid_height
    for col = 1:grid_width

        plot(samples_u(row, col), samples_v(row, col), 'b*');
        hold on
        
        if (row > 1)
            line([samples_u(row - 1, col), samples_u(row, col)], ...
                 [samples_v(row - 1, col), samples_v(row, col)], 'Color', 'b');
        end
        hold on
        if (col > 1)
            line([samples_u(row, col-1), samples_u(row, col)], ...
                 [samples_v(row, col-1), samples_v(row, col)], 'Color', 'b');
        end
        hold on
    end
end

pause
% axis([0, 3280, 0, 3280, 0, max(samples_z(:)) + 10]);
% axis([0, 3280*PIXEL_PITCH, 0, 3280*PIXEL_PITCH, 0, max(samples_z(:))]);
min_x  = min(samples_x(:));
min_y  = min(samples_y(:));
min_z  = min(samples_z(:));

max_x = max(samples_x(:));
max_y = max(samples_y(:));
max_z = max(samples_z(:));
pad_x = 0.1*(max_x - min_x);
pad_y = 0.1*(max_y - min_y);

min_x = min_x - pad_x;
max_x = max_x + pad_x;
min_y = min_y - pad_y;
max_y = max_y + pad_y;
figure(Particle_System.graphics_handle);
axis([min_x, max_x, min_y, max_y, -1, max_z]);


 %% create springs
 ks =100; % strength
 kd = 2; % damping
  % rest length = original distance
 for row = 1:grid_height
     for col = 1:grid_width
         
         % vertical springs
         if (row > 1)
           rv = sqrt((samples_x(row - 1, col) - samples_x(row, col))^2 + ...
                     (samples_y(row - 1, col) - samples_y(row, col))^2 + ...
                     (samples_z(row - 1, col) - samples_z(row, col))^2);
           Springs_vertical{row, col} = spring(Particle_System, ...
                                          Particles{row - 1, col}, ...
                                          Particles{row, col}, ...
                                          rv, ks, kd);
           % first diagonal
           if (col > 1)
               rd1 = sqrt((samples_x(row - 1, col - 1) - samples_x(row, col))^2 + ...
                          (samples_y(row - 1, col - 1) - samples_y(row, col))^2 + ...
                          (samples_z(row - 1, col - 1) - samples_z(row, col))^2);
               Springs_diag1{row, col} = spring(Particle_System, ...
                                           Particles{row - 1, col - 1}, ...
                                           Particles{row, col}, ...
                                           rd1, ks, kd);
           end
           
           % second diagonal
           if (col < grid_width)
               rd2 = sqrt((samples_x(row - 1, col + 1) - samples_x(row, col))^2 + ...
                          (samples_y(row - 1, col + 1) - samples_y(row, col))^2 + ...
                          (samples_z(row - 1, col + 1) - samples_z(row, col))^2);
                      
               Springs_diag2{row, col} = spring(Particle_System, ...
                                           Particles{row - 1, col + 1}, ...
                                           Particles{row, col}, ...
                                           rd2, ks, kd);
           end
         end
         % horizontal springs
         if (col > 1)
           rh = sqrt((samples_x(row, col) - samples_x(row, col - 1))^2 + ...
                     (samples_y(row, col) - samples_y(row, col - 1))^2 + ...
                     (samples_z(row, col) - samples_z(row, col - 1))^2);
           Springs_horizontal{row, col} = spring(Particle_System, ...
                                          Particles{row, col - 1}, ...
                                          Particles{row, col}, ...
                                          rh, ks, kd);
         end
     end
 end

%  pause
 disp('Running simulation')
 tic
 step_time = 0.05;
 h_threshold = 1e-3;
 max_iter = 100000
 for i = 1:max_iter
     Particle_System.advance_time(step_time);
     particle_positions = Particle_System.get_particles_positions();
     particle_height = particle_positions(3:3:end);
     if (mod(i,10) == 0)
         pause;
     end

     if (max(particle_height) <= h_threshold)
         disp('All particles dropped to ground');
         break;
     end
     
     if Particle_System.stop_moving(4e-3) % velocity_threshold
         disp('Achieved equilibrium');
         break;
     end
 end
 toc
 disp(['Number of steps = ', num2str(i)]);

 
%% display final grid
final_particles_position = Particle_System.get_particles_positions();
% final_samples_x = zeros(grid_height, grid_width);
% final_samples_y = zeros(grid_height, grid_width);
% final_samples_z = zeros(grid_height, grid_width);
final_samples_u = zeros(grid_height, grid_width);
final_samples_v = zeros(grid_height, grid_width);

% d = F*min_depth/(min_depth - F);
d = F*max_depth/(max_depth - F);

for row = 1:grid_height
    for col = 1:grid_width  
        index = (row - 1)*grid_width + col;
        x = final_particles_position(3*index - 2);
        y = final_particles_position(3*index - 1);
%         u =  -(x * d) / (min_depth*PIXEL_PITCH) + cx;
%         v =  -(y * d) / (min_depth*PIXEL_PITCH) + cy;        
%         u =  (x * d) / (min_depth*PIXEL_PITCH) + cx;
%         v =  (y * d) / (min_depth*PIXEL_PITCH) + cy;
        u =  (x * d) / (max_depth*PIXEL_PITCH) + cx;
        v =  (y * d) / (max_depth*PIXEL_PITCH) + cy;        
%         final_samples_x(row, col) = x;
%         final_samples_y(row, col) = y;
%         final_samples_z(row, col) = z;
        final_samples_u(row, col) = u;
        final_samples_v(row, col) = v;
        

    end
end
disp('Warping ...');

% output_affine = Local_affine_warp(input_img, samples_u, samples_v, final_samples_u, final_samples_v);
% figure; imagesc(output_affine);

% sigma = 3.0;
% input_img = double(input_img);
% input_img = input_img./max(input_img(:));
% output = RBF_warp(input_img, samples_u, samples_v, final_samples_u, final_samples_v, sigma);
% figure; imagesc(output);

% TPS warping
input_img = double(input_img);
input_img  = input_img./max(input_img(:));
H = size(input_img, 1);
W = size(input_img, 2);

outdim = [W, H];
source_landmarks = [samples_u(:), samples_v(:)];
dest_landmarks = [final_samples_u(:), final_samples_v(:)];

% interpolation params
interp.method = 'invdist'; %'nearest', 'none'
interp.radius = 4; % radius or median filter dimension
interp.power = 2; % power for inverse weighing interpolation method
tic
[output_tps, output_tps_w_holes, hole_map] = tpswarp(input_img, outdim, ...
                source_landmarks, dest_landmarks, interp);
toc
figure; imagesc(output_tps);
axis image; 
axis off;

counter = 1;
OUTFILE = [RESULTS_DIR, 'IMG_', num2str(img_no, '%04i'), '_grid_size_', ...
    num2str(grid_height), 'x', num2str(grid_width), '_captured_depth_' ,num2str(counter), '.png'];
while (exist(OUTFILE, 'file'))
    counter = counter + 1;
    OUTFILE = [OUTFILE(1:end - 5), num2str(counter), '.png'];
end
imwrite(uint8(output_tps.*double(intmax('uint8'))), OUTFILE);
% imwrite(uint8(output_affine.*double(intmax('uint8'))), %[RESULTS_DIR, 'IMG_', num2str(img_no, '%04i'), '_grid_size_', num2str(grid_height), 'x', num2str(grid_width),'_affine.png']);


%% refocused image with est depth
IMG2_FILENAME = [date, '_', num2str(img_no, '%04i'),'_refocused_rad_6_with_estimated_depth.png'];
input_img2 = imread([IMAGE_DIR,IMG2_FILENAME]);
input_img2 = double(input_img2);
input_img2 = input_img2./max(input_img2(:));

disp('Unwarping image 2');
tic
[output_tps2, output_tps_w_holes2, hole_map2] = tpswarp(input_img2, outdim, ...
                source_landmarks, dest_landmarks, interp);
toc

toc
figure; imagesc(output_tps2);
axis image; 
axis off;

counter2 = 1;
OUTFILE2 = [RESULTS_DIR, 'IMG_', num2str(img_no, '%04i'), '_grid_size_', ...
    num2str(grid_height), 'x', num2str(grid_width),'_estimated_depth_' ,num2str(counter), '.png'];
while (exist(OUTFILE2, 'file'))
    counter2 = counter2 + 1;
    OUTFILE2 = [OUTFILE2(1:end - 5), num2str(counter), '.png'];
end
imwrite(uint8(output_tps2.*double(intmax('uint8'))), OUTFILE2);
% imwrite


%% overlay grid on result image
figure; imagesc(output_tps);
axis image; 
axis off;
hold on

for row = 1:grid_height
    for col = 1:grid_width
        plot(final_samples_u(row, col), final_samples_v(row, col), 'b*');
        hold on
        
        if (row > 1)
            line([final_samples_u(row - 1, col), final_samples_u(row, col)], ...
                 [final_samples_v(row - 1, col), final_samples_v(row, col)], ...
                 'Color', 'b');
        end
        hold on
        if (col > 1)
            line([final_samples_u(row, col-1), final_samples_u(row, col)], ...
                 [final_samples_v(row, col-1), final_samples_v(row, col)], ...
                 'Color', 'b');
        end
        hold on
    end
end
hold off;
pause