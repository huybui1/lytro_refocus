function [output, gamma_corrected_white_img] = preprocess_lightfield(img_folder, white_folder, img_name)
% read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau
[LFP, ExtraSections] = LFReadLFP([img_folder, img_name]);

% metadata
BLACK_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.black.gr;
WHITE_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.white.gr;
% color correct
GAMMA = LFP.Metadata.image.color.gamma^0.5;
COL_MATRIX = reshape(LFP.Metadata.image.color.ccmRgbToSrgbArray, 3, 3);
COL_BALANCE = [LFP.Metadata.image.color.whiteBalanceGain.b, ...
               LFP.Metadata.image.color.whiteBalanceGain.gb;
               LFP.Metadata.image.color.whiteBalanceGain.gr, ...
               LFP.Metadata.image.color.whiteBalanceGain.r];        

% decode img
img = LFP.RawImg;
img = double(img);

% white_img = imread([white_folder, 'white_image.tif']);
% white_img = double(white_img);

white_img = LFReadRaw([white_folder, 'white_image.RAW'], '12bit');
white_img = double(white_img);
white_img = (white_img./max(white_img(:)))*4095; %12bit

% linearize
white_img = (white_img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);
img = (img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);

% Devignette
img = img./white_img;
gamma_corrected_white_img = white_img.^GAMMA;

%Clip
img = min(1, max(0, img));
% output = img;

wb_img = white_balance(img, COL_BALANCE);
wb_img = min(1, max(0, wb_img));

output = wb_img;