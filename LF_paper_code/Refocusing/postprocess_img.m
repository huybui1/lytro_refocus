function output = postprocess_img(img_folder, img_name, refocused_result)
% read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau
[LFP, ExtraSections] = LFReadLFP([img_folder, img_name]);

% metadata
BLACK_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.black.gr;
WHITE_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.white.gr;
% color correct
GAMMA = LFP.Metadata.image.color.gamma;
COL_MATRIX = reshape(LFP.Metadata.image.color.ccmRgbToSrgbArray, 3, 3);
COL_BALANCE = [LFP.Metadata.image.color.whiteBalanceGain.b, ...
               LFP.Metadata.image.color.whiteBalanceGain.gb;
               LFP.Metadata.image.color.whiteBalanceGain.gr, ...
               LFP.Metadata.image.color.whiteBalanceGain.r]       

% wb_img = white_balance(refocused_result, COL_BALANCE);
% wb_img = min(1, max(0, wb_img));

% demosaic
demo_img = demosaic(uint16(double(refocused_result).*double(intmax('uint16'))), 'bggr');
demo_img = double(demo_img);
col_img = demo_img./ double(intmax('uint16'));

col_img = color_correct(col_img, COL_MATRIX, COL_BALANCE, GAMMA^0.5);
col_img = min(1, max(0, col_img));

% col_img = LFHistEqualize(col_img);

output = col_img;