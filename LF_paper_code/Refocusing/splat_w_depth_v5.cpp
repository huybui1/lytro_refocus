// do splatting using depth
// + weight ray using white image intensity
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void splat(const float *LF, /* input raw light field */
           const float *white_img,
           const float *distance_map,  /* distance map associate with raw image */
           const float *kernel,
           const float *u, const float *v, /* u and v array */
           float *result,
           float *result_dist,
           float f,
           int rad,
           int height, int width) {
  
  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      int pixel_index = w * height + h;
      float refocused_distance = distance_map[pixel_index];
      float D = 1.0f + refocused_distance / f;
	
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];
      float confidence = white_img[pixel_index];
      confidence *= confidence;
      float I1 = LF[pixel_index];
      float I2 = LF[width * height + pixel_index];
      float I3 = LF[2 * width * height + pixel_index];
	  
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);
      
      for (int i = 0; i < 2*rad + 1; ++i) {
        int cur_x = cx - rad + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = 0; j < 2*rad + 1; ++j) {
          int cur_y = cy - rad + j;
           
          if ( cur_y >= 0 && cur_y < height ) {
            float wi = kernel[ i * (2*rad + 1) + j ];
            weight[ cur_x * height + cur_y ] += wi*confidence;
            result[ cur_x * height + cur_y ] += I1*wi*confidence;
            result[ width * height + cur_x * height + cur_y] += I2*wi*confidence;
            result[ 2 * width * height + cur_x * height + cur_y] += I3*wi*confidence;
            result_dist[ cur_x * height + cur_y ] += refocused_distance*wi*confidence;
          }
        }
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        result[ w * height + h ] /= wi;
        result[ width * height + w * height + h] /= wi;
        result[ 2 * width * height + w * height + h] /= wi;
        result_dist[ w * height + h ] /= wi;
		  }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = splat(LF, u, v, distance_map, kernel, mla_to_sensor_dist, white_img)
  // mexPrintf("nrhs = %d", nrhs);
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get radius of kernel
  int rad = (mxGetM(prhs[4]) - 1) / 2;

  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[1]);
  float *v = (float *) mxGetData(prhs[2]);
  
  // get distance map
  float *distance_map = (float *) mxGetData(prhs[3]);

  // get kernel array
  float *kernel = (float *) mxGetData(prhs[4]);

  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[5]));

  // get white image 
  float *white_img = (float *)mxGetData(prhs[6]);
  
  // allocate output
  plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);

  plhs[1] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result_dist = (float *) mxGetData(plhs[1]);
  
  // splatting
  splat(LF, white_img, distance_map, kernel, 
        u, v, result, result_dist, f, rad, height, width);
}