function [F, L] = get_LF_calib_params(calib_file)

% return the focal length of main lens (F) and distance between main lens
% and microlens (L). Unit: mm

load(calib_file);

K1 = IntParamLF(1);
K2 = IntParamLF(2);

mu = 25e-3; % distance from MLA to sensor

% L*(L/mu + 1) = K2
coefs = [1/mu, 1, -K2];
r = roots(coefs);

if (r(1) > 0)
    L = r(1);
else
    L = r(2);
end

F = 1/(K1 + 1/L);