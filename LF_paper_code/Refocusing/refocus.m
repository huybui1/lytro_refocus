% splat each ray to corresponding distance (depth) + weight ray using white image
% return also a distance (depth) map 
clear all
clc
close all


use_captured_depth = false;

LF_DIR = '../../Data/Images/Mar17_LF_Data/';
CALIBRATION_DIR = ['../../Lytro_plus_depth/Calibration/', LF_DIR(end-13:end-9), '/'];
DISTANCE_DIR = ['../Results/June_22/',LF_DIR(end-13:end-9), '/' ];
RESULTS_DIR = ['../Results/June_22/',LF_DIR(end-13:end-9), '/' ];


% kernel for splatting
rad = 6;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;

% load white image
white_img = LFReadRaw([CALIBRATION_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);
% normalize
white_img = white_img./max(white_img(:));

% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    load([CALIBRATION_DIR, 'microlens_center_list.mat']);
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end

mla_to_sensor_dist = 25e-3;
for img_no = 1:50

    if (use_captured_depth)
        OUTPUT_FILE = [LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'),'_refocused_rad_', num2str(rad),'_with_captured_depth.png'];
    else
        OUTPUT_FILE = [LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'),'_refocused_rad_', num2str(rad),'_with_estimated_depth.png'];
    end    
    

    if (exist([RESULTS_DIR, OUTPUT_FILE], 'file'))
        disp('Already processed file.');
        continue;
    end
    
    % Raw light-field image
    RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

    % Distance map
    if (use_captured_depth) 
        DISTANCE_FILE = ['Mapped_dist_', LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'), '.mat'];
    else
        DISTANCE_FILE = ['Est_dist_', LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'), '.mat'];
    end

    if (~exist([DISTANCE_DIR, DISTANCE_FILE], 'file'))
        continue;
    else
        disp(['Processing ', RAW_LF_FILE]);
    end
    
    % load mapped distance from file
    load([DISTANCE_DIR, DISTANCE_FILE]);



    figure;
    imagesc(mapped_distance);
    title('Mapped distance on sensor via mla');                

    % refocus by splatting



    disp('Preprocessing light field image ...');
    tic
    processed_LF = preprocess_lightfield(LF_DIR, CALIBRATION_DIR, RAW_LF_FILE);
    toc

    figure; imshow(processed_LF);
    title('Preprocessed raw light field image');

    

    disp('Splatting ...');
    tic
    [result, result_dist] = splat_w_depth_v5(single(processed_LF), single(u), single(v), ...
                           single(mapped_distance), ...
                           single(kernel), single(mla_to_sensor_dist), single(white_img));
    toc

    final_result = uint16(result*double(intmax('uint16')));
    figure;
    imshow(final_result); title('Refocused result');
    axis image;
    axis off;

    imwrite(final_result, [RESULTS_DIR, OUTPUT_FILE]);

    if (~use_captured_depth) 
      % get F and L
      CALIB_FILE = 'IntParamLF.mat'
      [F, L] = get_LF_calib_params([CALIBRATION_DIR, CALIB_FILE]);

      % map distance to depth
      D = L - result_dist;
      upsampled_depth = D*F/(D - F);
      upsampled_distance = result_dist;
      save([RESULTS_DIR, 'Splatted_estimated_depth_', LF_DIR(end-13:end-9), '_', num2str(img_no, '%04i'), '.mat'], 'upsampled_depth')
      save([RESULTS_DIR, 'Splatted_estimated_distance_', LF_DIR(end-13:end-9), '_', num2str(img_no, '%04i'), '.mat'], 'upsampled_distance')
    end
end