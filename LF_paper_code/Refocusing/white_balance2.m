function out = white_balance2(in, col_balance_matrix)
  % input = [R G B] image
  inSize = size(in);
  out = zeros(inSize);
  out(:,:,1) = in(:,:, 1)*col_balance_matrix(2,2);
  out(:,:,2) = in(:,:, 2)*col_balance_matrix(1,2);
  out(:,:,3) = in(:,:, 3)*col_balance_matrix(1,1);
  
%   out = in.*repmat(col_balance_matrix, inSize(1)/size(col_balance_matrix,1), ...
%                                        inSize(2)/size(col_balance_matrix,2));
end
