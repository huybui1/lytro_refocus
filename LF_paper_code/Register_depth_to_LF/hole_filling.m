function out  = hole_filling(input, rad)

% try hole filling using normalized convolution


[height, width] = size(input);


mask = ones(height, width);
mask(input == 0) = 0;
out = zeros(height, width);
weight = zeros(height, width);


for r = 1:height
    for w = 1:width
        for i  = max(1, r-rad):min(height, r+ rad)
            for j = max(1, w-rad):min(width, w+rad)
                weight(r, w) = weight(r, w) + mask(i, j);
                out(r,w) = out(r,w) + input(i,j)*mask(i,j);                
            end
        end        
    end
end

for r = 1:height
    for w = 1:width
        if (weight(r, w) > 0.0)
            out(r,w) = out(r,w)/weight(r,w);
        end
    end
end
        

end