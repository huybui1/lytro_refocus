% register depth from Kinect to LF raw image
% 1. select corresponding region
% 2. project to sub-aperture camera
% 3. depth -> distance
% 4. upsample distance 
% 5. project to raw image

clear all
clc
close all



LF_DIR = '../../Data/Images/Mar17_LF_Data/';
CALIBRATION_DIR = ['../../Lytro_plus_depth/Calibration/', LF_DIR(end-13:end-9), '/'];
DEPTH_DIR = ['../../Data/Depth/', LF_DIR(end-13:end-9), '_Depth_Data/'];
OUTPUT_DIR = ['../Results/June_22/',LF_DIR(end-13:end-9), '/' ];;
JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 29;
OUTPUT_FILE = ['Mapped_dist_', LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'), '.mat']
if (exist([OUTPUT_DIR, OUTPUT_FILE], 'file'))
    disp('Already processed.');
    return;
end
% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
figure;
imagesc(subaperture_img);
axis image; axis off

load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

%% preprocess depth
% threshold depth
% depth(depth >= 800) = 0;
% denoising depth with BF
filter_width = 6;
sigma_s = 3;
sigma_r = 20;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));

% fill in invalid depth value
% filtered_depth = hole_filling(filtered_depth, 1);

% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc


;lkkl
figure; 
% subplot(1,2,1);
imagesc(subaperture_img);
% title('Center-view subaperture image');
axis image;
axis off;

figure;
% subplot(1,2,2);
imagesc(projected_depth); 
% title('Projected depth map');
% colormap gray;
axis image;
axis off;



[xx, yy] = meshgrid(1:328, 1:328);
figure;
mesh(xx, yy, -projected_depth); axis([0 400 0 400 -1200 -600]);


% map depth to distance
disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);
figure; 
imagesc(distance_map);
axis image;
axis off;

% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);

% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end

mla_to_sensor_dist = 25e-3;
upsampled_distance = upsample_distance(distance_map, 5, img_height, img_width);
upsampled_depth  = upsample_distance(projected_depth, 5, img_height, img_width);

figure; imagesc(upsampled_depth); axis image; axis off;
title('Upsampled depth');

figure; imagesc(upsampled_distance); axis image; axis off;
title('Upsampled distance');

if (~exist([OUTPUT_DIR, OUTPUT_FILE], 'file'))
    disp('Mapping distance to raw LF image ...');
   tic
    mapped_distance = map_distance_to_lf_image(single(upsampled_distance), ...
                        single(center_list(1,:)), single(center_list(2,:)), ...
                        int32(size(center_list,2)), single(mla_to_sensor_dist));
    toc
    save([OUTPUT_DIR, OUTPUT_FILE], 'mapped_distance');
end

figure;
imagesc(mapped_distance);
title('Mapped distance on sensor via mla');                

save([OUTPUT_DIR, 'Upsampled_depth_', LF_DIR(end-13:end-9), '_', num2str(img_no, '%04i'), '.mat'], 'upsampled_depth')
save([OUTPUT_DIR, 'Upsampled_distance_', LF_DIR(end-13:end-9), '_', num2str(img_no, '%04i'), '.mat'], 'upsampled_distance')

