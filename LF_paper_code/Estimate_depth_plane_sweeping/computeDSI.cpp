#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

// interpolate the input_image to get value at sx, sy
void interpolate(const float *input_image,  /* input image (height x width x 3) */
                 float height, float width,
                 float sx, float sy,  /* location */
                 float* I1, float* I2, float* I3 /* ouput values */
                 ) {
  int img_size = height * width;
  float sx0 = floor(sx);
  float sx1 = sx0 + 1.0;
  float sy0 = floor(sy);
  float sy1 = sy0 + 1.0;

  int i;
  float I1_00, I1_01, I1_10, I1_11;
  float I2_00, I2_01, I2_10, I2_11;
  float I3_00, I3_01, I3_10, I3_11;

  // 00
  i = (int)sx0 * height + (int)sy0;
  if ( i > 0 && i < img_size) {
    I1_00 = input_image[i];
    I2_00 = input_image[i + img_size];
    I3_00 = input_image[i + 2*img_size];
  } else {
    I1_00 = 0.0;
    I2_00 = 0.0;
    I3_00 = 0.0;
  }

  // 01
  i = (int)sx0 * height + (int) sy1;
  if ( i > 0 && i < img_size) {
    I1_01 = input_image[i];
    I2_01 = input_image[i + img_size];
    I3_01 = input_image[i + 2*img_size];
  } else {
    I1_01 = 0.0;
    I2_01 = 0.0;
    I3_01 = 0.0;
  }

  // 10
  i = (int)sx1 * height + (int) sy0;
  if ( i > 0 && i < img_size) {
    I1_10 = input_image[i];
    I2_10 = input_image[i + img_size];
    I3_10 = input_image[i + 2*img_size];
  } else {
    I1_10 = 0.0;
    I2_10 = 0.0;
    I3_10 = 0.0;
  }

  // 11
  i = (int)sx1 * height + (int) sy1;
  if ( i > 0 && i < img_size) {
    I1_11 = input_image[i];
    I2_11 = input_image[i + img_size];
    I3_11 = input_image[i + 2*img_size];
  } else {
    I1_11 = 0.0;
    I2_11 = 0.0;
    I3_11 = 0.0;
  }

  // do interpolation
  float u = sx - sx0;
  float v = sy - sy0;

  *I1  = (1 - u) * (1 - v) * I1_00 +
         (1 - u) * (    v) * I1_01 +
         (    u) * (1 - v) * I1_10 +
         (    u) * (    v) * I1_11;

  *I2  = (1 - u) * (1 - v) * I2_00 +
         (1 - u) * (    v) * I2_01 +
         (    u) * (1 - v) * I2_10 +
         (    u) * (    v) * I2_11;

  *I3  = (1 - u) * (1 - v) * I3_00 +
         (1 - u) * (    v) * I3_01 +
         (    u) * (1 - v) * I3_10 +
         (    u) * (    v) * I3_11;
}

// compute disparity space image, using raw light field image
// and the refocused image at the same distance
void computeDSI( const float *u, const float *v,   /* u and v array */
            const float *LF, /* input light field image */
            const float *refocused_img, /* refocused image at the same dist. */
            const float *kernel,
            float *disparity_img,
            float refocused_dist, float f,
            int rad, int height, int width ) {
  float D = 1.0f + refocused_dist / f;

  float *weight = (float *)mxMalloc(height*width*sizeof(float));

  // set all disparities to -1.0 to indicate invalid value
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      disparity_img[w*height + h] = -1.0;     
    }
  }
  
  // splatting
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float sx = w - D*u[w * height + h];
      float sy = h - D*v[w * height + h];

      // get ray value I
      float I1 = LF[w * height + h];
      float I2 = LF[width * height + w * height + h];
      float I3 = LF[2 * width * height + w * height + h];

      // interpolate the refocused image at splatting location -> E_i
      float E1, E2, E3;
      interpolate(refocused_img, height, width, sx, sy,
                  &E1, &E2, &E3);
                  
      // compute ssd 
      float SSD = (E1 - I1) * (E1 - I1) +
                  (E2 - I2) * (E2 - I2) +
                  (E3 - I3) * (E3 - I3);

      // splatting SSD to disparity image
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);

      for (int i = 0; i < 2*rad + 1; ++i) {
        int cur_x = cx - rad + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = 0; j < 2*rad + 1; ++j) {
          int cur_y = cy - rad + j;

          if ( cur_y >= 0 && cur_y < height ) {
            float wi = kernel[ i * (2*rad + 1) + j ];

            if (disparity_img[cur_x * height + cur_y]  == -1.0)
                disparity_img[ cur_x * height + cur_y ] = SSD * wi;
            else
                disparity_img[ cur_x * height + cur_y ] += SSD * wi;
            
            weight[ cur_x * height + cur_y ] += wi;
            
          }
        }
      }
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        disparity_img[ w * height + h ] /= wi;
      }
    }
  }

   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // disparity_img = computeDSI(LF, refocused_img, u, v, kernel, refocused_distance, mla_to_sensor_dist)

  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];

  // get radius of kernel
  int rad = (mxGetM(prhs[4]) - 1) / 2;

  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);
  
  // get the refocused image at that distance (computed by splatting)
  float *refocused_img = (float *) mxGetData(prhs[1]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[2]);
  float *v = (float *) mxGetData(prhs[3]);

  // get kernel array
  float *kernel = (float *) mxGetData(prhs[4]);

  // refocused_distance
  float F = *((float *)mxGetData(prhs[5]));

  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[6]));

  // allocate output

  // disparity space image (DSI) at that distance
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *disparity_img = (float *) mxGetData(plhs[0]);

  // splatting
  computeDSI( u, v, LF, refocused_img, kernel, disparity_img,
              F, f, rad, height, width );
}