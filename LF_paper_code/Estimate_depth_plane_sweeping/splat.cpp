#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void splat( const float *u, const float *v,   /* u and v array */
            const float *LF, /* input light field image */
            const float *kernel,
            float *result,
            float refocused_dist, float f, 
            int rad, int height, int width,
            const float *white_img) {
  float D = 1.0f + refocused_dist / f;

  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float sx = w - D*u[w * height + h];
      float sy = h - D*v[w * height + h];
      float I1 = LF[w * height + h];
      float I2 = LF[width * height + w * height + h];
      float I3 = LF[2 * width * height + w * height + h];
      float ci = white_img[w * height + h];
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);
      
      for (int i = 0; i < 2*rad + 1; ++i) {
        int cur_x = cx - rad + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = 0; j < 2*rad + 1; ++j) {
          int cur_y = cy - rad + j;
           
          if ( cur_y >= 0 && cur_y < height ) {
            float wi = kernel[ i * (2*rad + 1) + j ]*ci;
 
            weight[ cur_x * height + cur_y ] += wi;
            result[ cur_x * height + cur_y ] += I1*wi;
            result[ width * height + cur_x * height + cur_y] += I2*wi;
            result[ 2 * width * height + cur_x * height + cur_y] += I3*wi;
          }
        }
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        result[ w * height + h ] /= wi;
        result[ width * height + w * height + h] /= wi;
        result[ 2 * width * height + w * height + h] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = splat(LF, u, v, kernel, refocused_distance, mla_to_sensor_dist)
  
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get radius of kernel
  int rad = (mxGetM(prhs[3]) - 1) / 2;

  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[1]);
  float *v = (float *) mxGetData(prhs[2]);
  
  // get kernel array
  float *kernel = (float *) mxGetData(prhs[3]);

  // refocused_distance
  float F = *((float *)mxGetData(prhs[4]));
 
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[5]));
  
  // white image
  float *white_img = (float *)mxGetData(prhs[6]);


  // allocate output
  plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  
  // splatting
  splat( u, v, LF, kernel, result, 
         F, f, rad, height, width, white_img );
}