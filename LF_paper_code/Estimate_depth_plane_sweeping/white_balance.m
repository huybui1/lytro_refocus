function out = white_balance(in, col_balance_matrix)
  inSize = size(in);
  out = in.*repmat(col_balance_matrix, inSize(1)/size(col_balance_matrix,1), ...
                                       inSize(2)/size(col_balance_matrix,2));
end
