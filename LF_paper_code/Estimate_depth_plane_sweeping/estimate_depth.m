clear all
clc
close all
rad = 10;
% image file
% img_no = 45;
LF_DIR = '../../Data/Images/Mar17_LF_Data/';
OUTPUT_DIR = ['../Results/June_22/',LF_DIR(end-13:end-9), '/' ];;
CALIBRATION_DIR = ['../../Lytro_plus_depth/Calibration/', LF_DIR(end-13:end-9), '/'];
load([CALIBRATION_DIR, 'microlens_center_list.mat']);

% load white image
white_img = LFReadRaw([CALIBRATION_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);
% normalize
white_img = white_img./max(white_img(:));

% kernel
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;
    
imgs = [28, 29];

for img_indx = 1:length(imgs)
    img_no = imgs(img_indx);

    
    IMG_NAME = ['IMG_', num2str(img_no, '%04i'), '.lfp'];
    OUTPUT_NAME = ['Est_dist_', LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'), '.mat'];
    
    if (~exist([LF_DIR, IMG_NAME], 'file') || exist([OUTPUT_DIR, OUTPUT_NAME], 'file'));
        continue;
    else
        disp(['Estimating depth for ', LF_DIR, IMG_NAME]);
    end
    
    

    processed_LF = preprocess_lightfield(LF_DIR, CALIBRATION_DIR, IMG_NAME);
    img_height = size(processed_LF, 1);
    img_width = size(processed_LF, 2);

    MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];
    f = 25e-6;  % distance from mla to sensor

    if (~exist([CALIBRATION_DIR, MAP_FILE], 'file'))
        disp('Generating neccessary maps ...');
        tic
        [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
        toc
    else
        load([CALIBRATION_DIR, MAP_FILE]);
    end





    distance_array =-100e-6:1e-6:100e-6;
    % distance_array = 1e-6:5e-6:11e-6;

    % compute disparity images
    DISPARITY_DIR = [OUTPUT_DIR, '/Temp/'];

    parfor F_index = 1:length(distance_array)
        DISPARITY_NAME = ['DSI_',LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'),'_', num2str(F_index, '%03i'), '_', num2str(rad), '.mat'];
        if ~exist([DISPARITY_DIR, DISPARITY_NAME], 'file')
            F = distance_array(F_index);
            disp(['Refocused Distance: ', num2str(F) , ' (m).']);

            % create refocused image at F
            tic
            refocused_img = splat(single(processed_LF), single(u), single(v), ...
                           single(kernel), single(F), single(f), single(white_img));
            toc

            % create disparity space image
            disp('Computing disparity space image (DSI) ...');
            tic
            disparity_img = computeDSI(single(processed_LF), ...
                                       single(refocused_img), ...
                                       single(u), single(v), single(kernel), ...
                                       single(F), single(f));
            toc   

            disparity_dist = F; % distance corresponds to this disparity map
            save_disparity_map(DISPARITY_DIR, DISPARITY_NAME, disparity_img, disparity_dist);
    %         clear disparity_img;
        end
    end %for F

    % estimate the depth map
    depth = zeros(img_height, img_width); % depth map to estimate
    %     min_disparity_map = - ones(img_height, img_width); % min disparity for each pixel
    min_disparity_map = 1e+6*ones(img_height, img_width);

    for index = 1:length(distance_array)
        DISPARITY_NAME = ['DSI_',LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'),'_', num2str(index, '%03i'), '_', num2str(rad), '.mat'];
        load([DISPARITY_DIR, DISPARITY_NAME]);

       tic
       [depth, min_disparity_map] = update_depth(single(depth), ...
                                                 single(min_disparity_map), ...
                                                 single(disparity_img), ...
                                                 single(disparity_dist), ...
                                                 single(u), single(v), ...
                                                 single(f));

        clear disparity_img;
        delete([DISPARITY_DIR, DISPARITY_NAME]);
    end

%     figure;
%     imagesc(depth);
%     axis image; axis off;
%     title('Estimated depth map');

    mapped_distance = depth;
    save([OUTPUT_DIR, OUTPUT_NAME], 'mapped_distance');
    clear DSIs;
end