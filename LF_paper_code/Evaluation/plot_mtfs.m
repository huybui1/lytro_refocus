clear all
clc
close all

addpath('..\..\DownloadedCodes\sfrmat3_post\');

disp('Compute MTF for Lytro all-focus');
[status_allfocus, dat_allfocus, ~,~,~,~,~, nn2out_allfocus, nfreq_allfocus] = sfrmat3();
freq_allfocus = dat_allfocus(:, 1);
mtf_allfocus = dat_allfocus(:, 5);

disp('Compute MTF for captured-depth refocus');
[status_cap, dat_cap, ~,~,~,~,~, nn2out_cap, nfreq_cap] = sfrmat3();
freq_cap = dat_cap(:, 1);
mtf_cap = dat_cap(:, 5);


disp('Compute MTF for estimated-depth refocus');
[status_est, dat_est, ~,~,~,~,~, nn2out_est, nfreq_est] = sfrmat3();
freq_est = dat_est(:, 1);
mtf_est = dat_est(:, 5);


max_y = max([max(mtf_allfocus(:)), max(mtf_cap(:)), max(mtf_est(:))]);
max_freq = max([freq_allfocus(round(0.75*nn2out_allfocus)), ...
                freq_cap(round(0.75*nn2out_cap)), ...
                freq_est(round(0.75*nn2out_est))]);
nfreq = mean([nfreq_allfocus, nfreq_cap, nfreq_est]);

            
figure(100);
plot(freq_allfocus(1:nn2out_allfocus), mtf_allfocus(1:nn2out_allfocus), 'k-o');
hold on
plot(freq_cap(1:nn2out_cap), mtf_cap(1:nn2out_cap), 'r-*');
hold on
plot(freq_est(1:nn2out_est), mtf_est(1:nn2out_est), 'b-+');
hold on
line([nfreq ,nfreq],[.05,0]);
hold on
text(.95*nfreq,+.08,'Half-sampling'),
hold off
axis([0 max_freq 0 max_y]);
legend('Lytro all-focus', 'Captured-depth refocus result', 'Estimated-depth refocus result');
grid on;

save('Dec16_0020.mat', 'freq_allfocus', 'mtf_allfocus', 'nn2out_allfocus', ...
                       'freq_cap', 'mtf_cap', 'nn2out_cap', ...
                       'freq_est', 'mtf_est', 'nn2out_est');
                      
