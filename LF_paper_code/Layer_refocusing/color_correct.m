function out = color_correct(in, ColMatrix, ColBalance, GAMMA)
  inSize = size(in);
  ndims = numel(inSize);
  in = reshape(in, [prod(inSize(1:ndims-1)), 3]);
  temp = in * ColMatrix;
  temp = reshape(temp, [inSize(1:ndims-1), 3]);
  
  % saturating
%   satlevel = [ColBalance(1), ColBalance(2), ColBalance(4)]*ColMatrix;
%   satlevel = min(satlevel);
%   
%   temp = min(satlevel, max(0, temp));
  temp = min(1, max(0, temp));
  out = temp.^GAMMA;
end