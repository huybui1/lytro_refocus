% refocus using depth
% splat to a specified distance + smooth (Gaussian) mask to indicate which 
% pixels in focus near that distance + weight pixel w. white image intensity
clear all
clc
close all


use_captured_depth = false;


LF_DIR = '../../Data/Images/Dec23_LF_Data/';
CALIBRATION_DIR = ['../../Lytro_plus_depth/Calibration/', LF_DIR(end-13:end-9), '/'];
DISTANCE_DIR = '../Results/June_16/';
RESULTS_DIR = '../Results/June_16/';

img_no = 24;

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Distance map
if (use_captured_depth) 
    DISTANCE_FILE = ['Mapped_dist_', LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'), '.mat']
else
    DISTANCE_FILE = ['Est_dist_', LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'), '.mat']
end

% load mapped distance from file
load([DISTANCE_DIR, DISTANCE_FILE]);

% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    load([CALIBRATION_DIR, 'microlens_center_list.mat']);
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end

mla_to_sensor_dist = 25e-3;

figure;
imagesc(mapped_distance);
title('Mapped distance on sensor via mla');                

% refocus by splatting

% load white image
white_img = LFReadRaw([CALIBRATION_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);
% normalize
white_img = white_img./max(white_img(:));

disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, CALIBRATION_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

% kernel for splatting
rad = 5;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;

disp('Splatting in layers...');

min_distance = min(mapped_distance(:)) - 1e-4;
max_distance = max(mapped_distance(:)) + 1e-4;

no_layers = 10;
delta_distance = (max_distance - min_distance)/(no_layers - 1);
epsilon = delta_distance/4;

all_distances = min_distance:delta_distance:max_distance;
layers = zeros(img_height, img_width, 3, no_layers);
masks  = zeros(img_height, img_width, no_layers);

for k = 1:no_layers
    disp(['Refocusing on layer ', num2str(k), '...']);
    
    refocus_distance = all_distances(k);
    tic
    [this_layer, this_mask] = splat_1_layer(single(processed_LF), ...
                                    single(u), single(v), ...
                                    single(mapped_distance), ...
                                    single(kernel), ...
                                    single(mla_to_sensor_dist), ...
                                    single(refocus_distance), ...
                                    single(epsilon), single(white_img));
    toc                                     
%     imwrite(uint16(this_layer*double(intmax('uint16'))), ...
%             [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_layer_', num2str(k),'_rad_', num2str(rad), '_v4.png']);   
%     imwrite(uint8(this_mask*255), ...
%             [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_mask_layer_', num2str(k),'_rad_', num2str(rad), '_v4.png']);
%     figure; imshow(this_layer); title(['Layer ', num2str(k)]); 
%     axis image; axis off;
%     drawnow;
%     de
%     figure; imagesc(this_mask); colormap gray;
%     title(['Mask for layer ', num2str(k)]);
%     axis image; axis off;
%     drawnow;
    
%     pause;
    
    layers(:,:, :, k) = this_layer;
    masks(:,:, k) = this_mask;
end

% merging
disp('Merging layers ...')

final_result = merge_layers(layers, masks);
final_result = uint16(final_result*double(intmax('uint16')));

figure;
imshow(final_result); title('Refocused result');
axis image;
axis off;
if (use_captured_depth)
    OUTPUT_FILE = [LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'),'_layer_refocused_rad_', num2str(rad),'_with_captured_depth.png'];
else
    OUTPUT_FILE = [LF_DIR(end-13:end-9),'_', num2str(img_no, '%04i'),'_layer_refocused_rad_', num2str(rad),'_with_estimated_depth.png'];
end
imwrite(final_result, [RESULTS_DIR, OUTPUT_FILE]);

