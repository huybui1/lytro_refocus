clear all
clc
close all

DIR = 'Test_Depth/';
num_imgs = 22;

for i = 1:num_imgs
    MODEL_FILE = ['model_plane_', num2str(i, '%04i'), '.mat'];
    IR_FILE = ['IMG_', num2str(i, '%04i'), '_IR.tif'];
    load([DIR, MODEL_FILE]);
    ir_frame = imread([DIR, IR_FILE]);
    
    figure(200);
    imagesc(model_plane); axis image; axis off;
    colormap gray;
    title('Model plane');
    
    figure(202);
    imshow(ir_frame); axis image; axis off;
    title('IR image');
    
    pause;
end