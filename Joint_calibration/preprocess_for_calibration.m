clc
close all
clear all

DIR = '../Data/Images/Dec4_LF_Calib/';
num_imgs = 18;
calib_folder = '../Data/calib_folder/';


white_img = imread([calib_folder, 'white_image.tif']);
white_img = double(white_img);
white_img = (white_img./max(white_img(:)))*4095; %12bit

image_size = [];
for i = 19:22
   filename = ['IMG_', num2str(i, '%04i'),'.lfp'];
   disp([DIR, filename]);
   if exist([DIR, filename], 'file')
       disp(['Processing ', filename, '...']);
       [LFP, ExtraSections] = LFReadLFP([DIR, filename]);

       % metadata
       BLACK_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.black.gr;
       WHITE_LEVEL = LFP.Metadata.image.rawDetails.pixelFormat.white.gr;
       
       % decode img
       img = LFP.RawImg;
       img = double(img);

%        figure; imagesc(img); colormap gray;

       % linearize
%        white_img = (white_img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);
       white_img = white_img./max(white_img(:));
       img = (img - BLACK_LEVEL)./(WHITE_LEVEL - BLACK_LEVEL);
%        keyboard
       % Devignette
       img = img./white_img;
%        figure; imshow(img); title('devignetting');
       %Clip
       img = min(1, max(0, img));
        
       % demosaic
       demo_img = demosaic(uint16(img.*double(intmax('uint16'))), 'bggr');
       demo_img = double(demo_img);
       demo_img = demo_img./ double(intmax('uint16'));

%        figure; imshow(demo_img); title('demosaic');
       
       % rotate image
       load([calib_folder, 'rotation.mat']);
       rotated_img = imrotate(demo_img, an*180/pi, 'bicubic');
       rotated_white = imrotate(white_img, an*180/pi, 'bicubic');
       % crop
       if offset.Y1(1)>offset.Y3(1)
         rotated_img = rotated_img(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
         rotated_white = rotated_white(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
       else
         rotated_img = rotated_img(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
         rotated_white = rotated_white(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
       end

       %clip
       processed_LF = min(1, max(rotated_img, 0));
       vig = min(1, max(rotated_white, 0))*255;
       save([DIR, 'vig_mean.mat'], 'vig');
       processed_LF = uint8(processed_LF.*double(intmax('uint8')));
       imwrite(processed_LF, [DIR, 'IMG_', num2str(i, '%04i'), '.png']);
       image_size = size(processed_LF);
       
       figure;
       imshow(processed_LF); 
       title(['Processed LF image ', num2str(i)]);
%        pause;
   end  
end

% process centers 
load([calib_folder, 'center.mat']);
radius  = 5;

[center_list, center_connection] = process_center_list(centerX, centerY, height, width, radius);
save([DIR,'microlens_center_list.mat'],'center_list','center_connection','radius','image_size');

