function output = model_plane_w_depth(depth_frame)
  depth_frame = single(depth_frame);
  [height, width] = size(depth_frame);
  w = 5; sigma_s = 10; sigma_r = 100;
  filtered_depth = BF_C(depth_frame, int32(w), single(sigma_s), single(sigma_r));
  filtered_depth = double(filtered_depth);
  figure(100); imagesc(filtered_depth); axis image; axis off; title('Filtered Depth');
  
  
  disp('Select random points on the plane');
  
  [x,  y] = ginput();
  
  A = [];
  
  for i = 1:length(x)
    xi = round(x(i));
    yi = round(y(i));
    
    if ((1 <= xi) && (xi <= width) && ...
        (1 <= yi) && (yi <= height))
       zi = filtered_depth(yi, xi);
       A = [A; xi yi zi 1.0];
    end    
  end
  
  [U S V] = svd(A);
  p = V(:,end);
  scale = sqrt(sum(p(1:3).^2));
  p = p/scale;
  
  output = zeros(height, width);
  
  for i = 1:height
      for j = 1:width
          if (p(3) ~= 0)
            output(i, j) =  -[p(1), p(2), p(4)]*[j;i;1]/p(3);
          end
      end
  end
  % clip
  output = max(output, 0);
end

