% clear all
clc
close all

% intrinsic matrix of depth camera
% fx = 288.59475;
% fy = 289.52164;
% cx = 159.80147;
% cy = 122.90624;

% subaperture intrinsics
% fsub_x = 1405.08879;
% fsub_y = 1404.51890;
% csub_x = 178.67767;
% csub_y = 115.64576;
% Ksub = [fsub_x,      0, csub_x;
%         0     , fsub_y, csub_y;
%         0     ,      0,      1];
% % Transformation between depth and lytro cam
% R= [0.998937612326458  -0.009848994882501   0.045018262731015;
%       0.023037092293171   0.952796856596036  -0.302733285978581
%       0.039911640635160  -0.303448755740600  -0.952011509164302];
% 
% t = [0.000090687582750;
%      0.000212329418521;
%      0.003356615912499];
T = [R, t];
 

% LF_DIR = 'Test_LF/';
% DEPTH_DIR = 'Test_Depth/';
% img_no = 1;

%  parameter for finding corners
% wintx = 5;
% winty = 5;

% number of squares (default)
% n_sq_x_default = 3;
% n_sq_y_default = 3;

i = 2;

disp(['Processing image ', num2str(i,'%04i')]);
IR_FILE = [IR_DIR, 'IR_' , num2str(i, '%04i'), '.tif'];
MODEL_DEPTH_FILE = [DEPTH_DIR, 'Modeled_depth_', num2str(i, '%04i'), '.mat'];
LF_SUB_FILE = [LF_DIR, 'CI_IMG_', num2str(i, '%04i'), '.bmp'];

ir_img = imread(IR_FILE);
load(MODEL_DEPTH_FILE);
depth = modeled_depth;

% extract corners from IR (for depth)
fig_no = 100;
img_type = 'IR image';
disp('Extract corners from IR image');
grid_pts_d = extract_corners_from_image(ir_img, wintx, wintx, n_sq_x_default, n_sq_y_default, fig_no, img_type);
xd = grid_pts_d(1,:);
yd = grid_pts_d(2,:);

num_pts = length(xd);

pts_3d_d = zeros(4, num_pts);

for k = 1:num_pts
    % project 2d corners on depth map to 3D points
    Zd = max(depth(round(yd(k)), round(xd(k))), 0);
    Xd = Zd/fx *( xd(k) - cx);
    Yd = Zd/fy *( yd(k) - cy);
   
    pts_3d_d(:, k) = [Xd; Yd; Zd; 1]; 
end

% transform to points in subaperture camera's coordinate
pts_3d_sub = T*pts_3d_d;

% backproject point on subaperture image

sub_img = imread(LF_SUB_FILE);
figure(300);
imagesc(sub_img); colormap gray;
pts_sub = Ksub*pts_3d_sub;

for k = 1:num_pts
    xs = pts_sub(1,k)/pts_sub(3,k);
    ys = pts_sub(2,k)/pts_sub(3,k);
    figure(300); hold on
    plot(xs, ys, 'r*');
end




    
    