clear all
clc
close all

alpha = 0.35;

DEPTH_DIR = 'Depth/';
IR_DIR = 'IR/';
num_imgs = 26;

for i = 1:num_imgs
    disp(['Processing image ', num2str(i,'%04i')]);
    IR_FILE = [IR_DIR, 'IR_' , num2str(i, '%04i'), '.tif'];
    DEPTH_FILE = [DEPTH_DIR,'Depth_', num2str(i,'%04i'),'.mat'];
    
    ir_img = imread(IR_FILE);
    load(DEPTH_FILE);
    depth = depth_frame;
    
    combine = depth*alpha + double(ir_img)*(1-alpha);
    combine = combine./max(combine(:));
    figure(300);
    title(['Image ', num2str(i)]);
    
    subplot(2,2,1);
    imagesc(depth_frame);
    subplot(2,2,2);
    imagesc(ir_img); colormap gray
    subplot(2,2,3);
    imagesc(combine);
    
    
    pause;
    
    
end