clear all
clc
close all

DIR = '../Data/Depth/Dec15_Depth_Calib/';
num_imgs = 35;

for i = 1:num_imgs
    
    FILENAME = ['Depth_', num2str(i, '%04i'),'.mat'];
    OUTPUT = ['Modeled_depth_', num2str(i, '%04i'), '.mat'];
    
    if ~exist([DIR, FILENAME], 'file')
        continue;
    end
    disp(['Processing file ', FILENAME]);
    load([DIR, FILENAME]);

    output = model_plane_w_depth(depth_frame);
    
    [height, width ] = size(depth_frame);
    
    x = 1:width;
    y = 1:height;
    [xx yy] = meshgrid(x, y);

    figure(100); 
    mesh(xx, yy, double(depth_frame));
    axis([1, width, 1, height, 0, max(depth_frame(:)), 0 1]);
    title('Input');
    

    figure(200); mesh(xx, yy, double(output)); 
    axis([1, width, 1, height, 0, max(depth_frame(:)), 0 1]);
    title('Output');
    
    modeled_depth = double(output);
    save([DIR, OUTPUT], 'modeled_depth');
    
    pause;
    
end