function grid_pts = extract_corners_from_image(img, wintx, winty,n_sq_x, n_sq_y, fig_no, img_type)  
    I = img;
    figure(fig_no);
    imshow(I); colormap gray; title(img_type);
    disp('Click on the four extreme corners of the pattern (first is origin) ...');

    x = []; y = [];
    figure(fig_no); hold on;
    for count = 1:4,
        [xi,yi] = ginput(1);
        [xxi] = cornerfinder([xi;yi],I,winty,wintx);
        xi = xxi(1);
        yi = xxi(2);
        figure(fig_no);
        plot(xi,yi,'+','color',[ 1.000 0.314 0.510 ],'linewidth',2);
        plot(xi + [wintx+.5 -(wintx+.5) -(wintx+.5) wintx+.5 wintx+.5],yi + [winty+.5 winty+.5 -(winty+.5) -(winty+.5)  winty+.5],'-','color',[ 1.000 0.314 0.510 ],'linewidth',2);
        x = [x;xi];
        y = [y;yi];
        plot(x,y,'-','color',[ 1.000 0.314 0.510 ],'linewidth',2);
        drawnow;
    end;
    plot([x;x(1)],[y;y(1)],'-','color',[ 1.000 0.314 0.510 ],'linewidth',2);
    drawnow;
    hold off;

    [Xc,good,bad,type] = cornerfinder([x';y'],I,winty,wintx); % the four corners

    x = Xc(1,:)';
    y = Xc(2,:)';


    % Sort the corners:
    x_mean = mean(x);
    y_mean = mean(y);
    x_v = x - x_mean;
    y_v = y - y_mean;

    theta = atan2(-y_v,x_v);
    [junk,ind] = sort(theta);

    [junk,ind] = sort(mod(theta-theta(1),2*pi));

    ind = ind([4 3 2 1]); %-> New: the Z axis is pointing uppward

    x = x(ind);
    y = y(ind);
    x1= x(1); x2 = x(2); x3 = x(3); x4 = x(4);
    y1= y(1); y2 = y(2); y3 = y(3); y4 = y(4);


    % Find center:
    p_center = cross(cross([x1;y1;1],[x3;y3;1]),cross([x2;y2;1],[x4;y4;1]));
    x5 = p_center(1)/p_center(3);
    y5 = p_center(2)/p_center(3);

    % center on the X axis:
    x6 = (x3 + x4)/2;
    y6 = (y3 + y4)/2;

    % center on the Y axis:
    x7 = (x1 + x4)/2;
    y7 = (y1 + y4)/2;

    % Direction of displacement for the X axis:
    vX = [x6-x5;y6-y5];
    vX = vX / norm(vX);

    % Direction of displacement for the X axis:
    vY = [x7-x5;y7-y5];
    vY = vY / norm(vY);

    % Direction of diagonal:
    vO = [x4 - x5; y4 - y5];
    vO = vO / norm(vO);

    delta = 30;


    figure(fig_no + 1); 
    imagesc(I); colormap gray;
    hold on;
    plot([x;x(1)],[y;y(1)],'g-');
    plot(x,y,'og');
    hx=text(x6 + delta * vX(1) ,y6 + delta*vX(2),'X');
    set(hx,'color','g','Fontsize',14);
    hy=text(x7 + delta*vY(1), y7 + delta*vY(2),'Y');
    set(hy,'color','g','Fontsize',14);
    hO=text(x4 + delta * vO(1) ,y4 + delta*vO(2),'O','color','g','Fontsize',14);
    hold off;  
  
%     n_sq_x = input(['Number of squares along the X direction ([]=' num2str(n_sq_x_default) ') = ']); %6
%     if isempty(n_sq_x), n_sq_x = n_sq_x_default; end;
%     n_sq_y = input(['Number of squares along the Y direction ([]=' num2str(n_sq_y_default) ') = ']); %6
%     if isempty(n_sq_y), n_sq_y = n_sq_y_default; end; 
    
    % Compute the inside points through computation of the planar homography (collineation)
    a00 = [x(1);y(1);1];
    a10 = [x(2);y(2);1];
    a11 = [x(3);y(3);1];
    a01 = [x(4);y(4);1];


    % Compute the planar collineation: (return the normalization matrix as well)

    [Homo,Hnorm,inv_Hnorm] = compute_homography([a00 a10 a11 a01],[0 1 1 0;0 0 1 1;1 1 1 1]);


    % Build the grid using the planar collineation:

    x_l = ((0:n_sq_x)'*ones(1,n_sq_y+1))/n_sq_x;
    y_l = (ones(n_sq_x+1,1)*(0:n_sq_y))/n_sq_y;
    pts = [x_l(:) y_l(:) ones((n_sq_x+1)*(n_sq_y+1),1)]';

    XX = Homo*pts;
    XX = XX(1:2,:) ./ (ones(2,1)*XX(3,:));
    
    % Complete size of the rectangle
    Np = (n_sq_x+1)*(n_sq_y+1);

    disp('Corner extraction...');
    grid_pts = cornerfinder(XX,I,winty,wintx); %%% Finds the exact corners at every points!
    grid_pts = grid_pts - 1; % subtract 1 to bring the origin to (0,0) instead of (1,1) in matlab (not necessary in C)  

    ind_corners = [1 n_sq_x+1 (n_sq_x+1)*n_sq_y+1 (n_sq_x+1)*(n_sq_y+1)]; % index of the 4 corners
    ind_orig = (n_sq_x+1)*n_sq_y + 1;
    xorig = grid_pts(1,ind_orig);
    yorig = grid_pts(2,ind_orig);
    dxpos = mean([grid_pts(:,ind_orig) grid_pts(:,ind_orig+1)]');
    dypos = mean([grid_pts(:,ind_orig) grid_pts(:,ind_orig-n_sq_x-1)]');


    x_box_kk = [grid_pts(1,:)-(wintx+.5);grid_pts(1,:)+(wintx+.5);grid_pts(1,:)+(wintx+.5);grid_pts(1,:)-(wintx+.5);grid_pts(1,:)-(wintx+.5)];
    y_box_kk = [grid_pts(2,:)-(winty+.5);grid_pts(2,:)-(winty+.5);grid_pts(2,:)+(winty+.5);grid_pts(2,:)+(winty+.5);grid_pts(2,:)-(winty+.5)];


    figure(fig_no + 2);
    imagesc(I); colormap gray; hold on;
    plot(grid_pts(1,:)+1,grid_pts(2,:)+1,'r+');
    for i = 1:size(grid_pts,2)
        h = text(grid_pts(1,i)+10*vO(1),grid_pts(2,i)+10*vO(2),num2str(i));
        set(h,'Color','m','FontSize',9);
    end
        
        
    plot(x_box_kk+1,y_box_kk+1,'-b');
    plot(grid_pts(1,ind_corners)+1,grid_pts(2,ind_corners)+1,'mo');
    plot(xorig+1,yorig+1,'*m');
    h = text(xorig+delta*vO(1),yorig+delta*vO(2),'O');
    set(h,'Color','m','FontSize',14);
    h2 = text(dxpos(1)+delta*vX(1),dxpos(2)+delta*vX(2),'dX');
    set(h2,'Color','g','FontSize',14);
    h3 = text(dypos(1)+delta*vY(1),dypos(2)+delta*vY(2),'dY');
    set(h3,'Color','g','FontSize',14);
    xlabel('Xc (in camera frame)');
    ylabel('Yc (in camera frame)');
    title('Extracted corners');
    zoom on;
    drawnow;
    hold off;
end
