clear all
clc
close all


% intrinsic matrix of depth camera
K = [288.59475,       0.0, -159.80147;
           0.0, 289.52164, -122.90624;
           0.0,       0.0,   1.0 ];
K_inv = inv(K);

LF_DIR = 'Test_LF/';
DEPTH_DIR = 'Test_Depth/';
num_images = 18;
A = [];
b = [];

for i = 1:num_images
   % load extrinsic parameters for Lytro (from LightFieldCalib
   ExtFile = [LF_DIR, 'ExtParamLF_IMG_', num2str(i, '%04i'),'.mat'];
   if ~exist(ExtFile, 'file')
       continue;
   end
   load(ExtFile);
   
   % load corresponding depth frame
   DepthFile = [DEPTH_DIR, 'IMG_', num2str(i, '%04i'), '_Depth.mat'];
   if ~exist(DepthFile, 'file')
       continue;
   end
   load(DepthFile);
   
   disp(['Extrinsic parameter for light field image# ', num2str(i), ': '] );
   disp(ExtParamLF);
   Ri = ExtParamLF(1:3, 1:3);
   ti = ExtParamLF(1:3, 4);
   
   figure(100);
   imagesc(depth_frame);
   axis image;
   axis off;
   title(['Depth frame ', num2str(i)]);
   
   disp('Select 3 random points on the checkerboard plane: ');
   [xd, yd] = ginput(3);
   [depth_height, depth_width] = size(depth_frame);
   
   xd = round(xd);
   yd = round(yd);
   for p = 1:length(xd)
       xp = xd(p);
       yp = yd(p);
       if ( (xp >= 1) && (xp <= depth_width) && ...
            (yp >= 1) && (yp <= depth_height))
         zp = depth_frame(yp, xp);
         
         if (zp > 0.0)
             % back project to 3d point in depth cam's coordinate system
             pts_d = zp*K_inv*[xp; yp; 1];
             
             % build system of linear eqs
             A_row = [Ri(1,3)*pts_d', Ri(2,3)*pts_d', Ri(3,3)*pts_d', ...
                      Ri(1,3), Ri(2,3), Ri(3,3)]
             b_row = Ri(1,3)*ti(1) + Ri(2,3)*ti(2) + Ri(3,3)*ti(3)
             
             A = [A; A_row];
             b = [b; b_row];
         end
       end       
   end
end
res = pinv(A)*b;
R_temp = reshape(res(1:9), 3,3)';
[U S V] = svd(R_temp);
R = U*V'
t = res(10:12)