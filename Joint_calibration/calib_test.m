clear all
clc
close all

LF_DIR = 'LF_CenterView/';
DEPTH_DIR = 'Depth/';
IR_DIR = 'IR/';

% depth camera intrinsics
load([IR_DIR, 'Calib_Results.mat']);
fc_d = fc;
cc_d = cc;
kc_d = kc;
alpha_c_d = alpha_c;
KK_d = KK;

% center subaperture view intrinsics
load([LF_DIR, 'Calib_Results.mat']);
fc_s  = fc;
cc_s = cc;
kd_s = kc;
alpha_c_s = alpha_c;
KK_s = KK;


num_imgs = 26;
excluded = [3, 7, 16, 21, 26];

%  parameter for finding corners
wintx = 5;
winty = 5;

% number of squares (default)
n_sq_x_default = 3;
n_sq_y_default = 3;

P = [];
frames = [];
for i = 1:num_imgs
    if isempty(find(excluded == i))
        disp(['Processing image ', num2str(i,'%04i')]);
        IR_FILE = [IR_DIR, 'IR_' , num2str(i, '%04i'), '.tif'];
        MODEL_DEPTH_FILE = [DEPTH_DIR, 'Modeled_depth_', num2str(i, '%04i'), '.mat'];
        LF_SUB_FILE = [LF_DIR, 'CI_IMG_', num2str(i, '%04i'), '.bmp'];
        DEPTH_FILE = [DEPTH_DIR,'Depth_', num2str(i,'%04i'),'.mat'];
        ir_img = imread(IR_FILE);
        sub_img = imread(LF_SUB_FILE);
        load(MODEL_DEPTH_FILE);
        depth = modeled_depth;
            % extract corners from IR (for depth)     

        fig_no = 100;
        img_type = 'IR image';
        disp('Extract corners from IR image');
        grid_pts_d = extract_corners_from_image(ir_img, wintx, wintx, n_sq_x_default, n_sq_y_default, fig_no, img_type);
        
        xd = grid_pts_d(1,:);
        yd = grid_pts_d(2,:);
        figure(150);
        imagesc(depth); 
        hold on;
        plot(xd,yd,'r+');
        hold off;
        title('Selected pixels in depth map');
        
        % extract corners from sub aperture image
        fig_no = 200;
        img_type = 'Subaperture Lytro image';
        disp('Extract corners from Subaperture Lytro image');
        disp('Select corners in the same order as in IR image');
        grid_pts_s = extract_corners_from_image(sub_img, wintx, wintx, n_sq_x_default, n_sq_y_default, fig_no, img_type);
        xs = grid_pts_s(1,:);
        ys = grid_pts_s(2,:);

        num_pts = size(grid_pts_s, 2);

        pts_3d_n = normalize(grid_pts_d, fc_d, cc_d, kc_d, alpha_c_d);
        
        for k = 1:num_pts
            % project 2d corners on depth map to 3D points
            Zd = max(depth(round(yd(k)), round(xd(k))), 0);
            if (Zd ~= 0)
                Xd = Zd*pts_3d_n(1, k);
                Yd = Zd*pts_3d_n(2, k);

                P = [P; 
                     Xd, Yd, Zd, 1, zeros(1, 4)      , -xs(k)*Xd, -xs(k)*Yd, -xs(k)*Zd, -xs(k);
                     zeros(1,4),    Xd, Yd, Zd, 1    , -ys(k)*Xd, -ys(k)*Yd, -ys(k)*Zd, -ys(k)];
            end
        end
        frames(i).ir_img = ir_img;
        frames(i).modeled_depth  = modeled_depth;
        frames(i).sub_img = sub_img;
        frames(i).grid_pts_d = grid_pts_d;
        frames(i).grid_pts_s = grid_pts_s;
    end
end

[U S V] = svd(P);
m = V(:, end);
M = reshape(m, [4, 3])';

% subaperture intrinsics
fsub_x = 1408.92887;
fsub_y = 1410.18273
csub_x = 140.54734;
csub_y = 118.02312;
% 
% T = zeros(3, 4);
% T(3,:) = M(3,:); % r31 r32 r33 t3
% T(1,:) = (M(1,:) - csub_x*T(3,:))/fsub_x;
% T(2,:) = (M(2,:) - csub_y*T(3,:))/fsub_y;
% 
% R  = T(1:3, 1:3);
% t =  T(1:3, 4);

A = M(1:3, 1:3);
b = M(1:3, 4);

rho = -1/norm(A(3,:));
r3 = rho*A(3,:);

r1 = cross(A(2,:), A(3,:));
r1 = r1 / norm(r1);

r2 = cross(r3, r1);

R = [r1; r2; r3];
Ksub = [fc_s(1), alpha_c_s*fc_s(1), cc_s(1);
              0,           fc_s(2), cc_s(2);
              0,                 0,       1];
t = rho*inv(Ksub)*b;
% u0 = rho^2*A(1,:)*A(3,:)';
% v0 = rho^2*A(2,:)*A(3,:)';
% 
% cross_13  = cross(A(1,:), A(3,:));
% cross_23  = cross(A(2,:), A(3,:));
% cos_t = - (cross_13*cross_23')/(norm(cross_13)*norm(cross_23));
% sin_t = sqrt(1 - cos_t*cos_t);
% alpha = rho^2*norm(cross_13)*sin_t;
% beta = rho^2*norm(cross_23)*sin_t;
% 
% r1 = rho^2*sin_t*cross_23/beta;
% r2 = cross(r3, r1);
% 
% R = [r1; r2; r3];
% Ksub = [alpha, -alpha*cos_t/sin_t, u0;
%         0,   beta/sin_t,     v0;
%         0,  0, 1];
% t = rho*inv(Ksub)*b;


% save calib_data.mat frames P M R t fx fy cx cy fsub_x fsub_y csub_x csub_y
save calib_data.mat frames P M R t fx fy cx cy Ksub;
verify_calibration;
        
