function [center_list, center_connection] = process_center_list(centerX, centerY, rows, cols, radius)
  [centerX1, centerY1, ~, ~]  = rearrange_centers(centerX, centerY, rows, cols);
  
  center_list = [centerX1(:), centerY1(:)]';
  num_centers = size(center_list, 2);
  center_connection = zeros(7, num_centers);
  for n=1:length(center_list(1,:))
    idx7=find((center_list(1,:)-center_list(1,n)).^2+(center_list(2,:)-center_list(2,n)).^2<radius*radius*5);
    center_connection(:,n)=[idx7';n*ones(7-length(idx7),1)];
  end 
end