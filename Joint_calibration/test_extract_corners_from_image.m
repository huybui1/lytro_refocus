
clear all
clc
close all

DIR = 'Test_LF/';
FILE =  'CI_IMG_0001.bmp';

wintx = 5;
winty = 5;
n_sq_x_default = 3;
n_sq_y_default = 3;

img = imread([DIR, FILE]);
grid_pts = extract_corners_from_image(img, wintx, winty, n_sq_x_default, n_sq_y_default);

