clear all
clc
close all


 

LF_DIR = '../Data/Images/Dec9_LF_Calib/';
IR_DIR = '../Data/Depth/Dec9_Depth_Calib/';
DEPTH_DIR = '../Data/Depth/Dec9_Depth_Calib/';
CALIB_DIR = '../Lytro_plus_depth/Calibration/Dec9/';
img_no = 9;

load([CALIB_DIR, 'Calib_Results_stereo.mat']);

Tr = [R, T];

%  parameter for finding corners
wintx = 5;
winty = 5;

% number of squares (default)
n_sq_x_default = 3;
n_sq_y_default = 3;

disp(['Processing image ', num2str(img_no,'%04i')]);
IR_FILE = [IR_DIR, 'IR_' , num2str(img_no, '%04i'), '.tif'];
LF_SUB_FILE = [LF_DIR, 'CI_IMG_', num2str(img_no, '%04i'), '.bmp'];
MODEL_DEPTH_FILE = [DEPTH_DIR, 'Modeled_depth_', num2str(img_no, '%04i'), '.mat'];
DEPTH_FILE = [DEPTH_DIR, 'Depth_', num2str(img_no, '%04i'), '.mat'];
ir_img = imread(IR_FILE);
load(MODEL_DEPTH_FILE);
depth = modeled_depth;
% load(DEPTH_FILE);
% depth = depth_frame;

keyboard

[height, width ] = size(depth);
[xx yy] = meshgrid(1:width, 1:height);
figure;
mesh(xx, yy, double(depth));
keyboard

load([DEPTH_DIR, 'Depth_', num2str(img_no, '%04i'), '.mat']);
figure;
imagesc(depth_frame); axis image; axis off; colormap gray;

figure;
mesh(xx, yy, double(depth_frame));
keyboard
% extract corners from IR
fig_no = 100;
img_type = 'IR image';
disp('Extract corners from IR image');
grid_pts_d = extract_corners_from_image(ir_img, wintx, wintx, n_sq_x_default, n_sq_y_default, fig_no, img_type);
num_pts = size(grid_pts_d,2);

pts_3d_n = normalize(grid_pts_d, fc_left, cc_left, kc_left, alpha_c_left);
pts_3d_d = zeros(4, num_pts);
for k = 1:num_pts
    % project 2d corners on depth map to 3D points
    Zd = max(depth(round(grid_pts_d(2,k)), round(grid_pts_d(1,k))), 0);
    Xd = pts_3d_n(1,k)*Zd;
    Yd = pts_3d_n(2,k)*Zd;
   
    pts_3d_d(:, k) = [Xd; Yd; Zd; 1]; 
end

% transform to points in subaperture camera's coordinate
pts_3d_sub = Tr*pts_3d_d;

% backproject point on subaperture image
sub_img = imread(LF_SUB_FILE);
figure(300);
hold off;
imagesc(sub_img); colormap gray; axis image; axis off;


% pts_sub = KK_right*pts_3d_sub;
% for k = 1:num_pts
%     xs = pts_sub(1,k)/pts_sub(3,k);
%     ys = pts_sub(2,k)/pts_sub(3,k);
%     figure(300); hold on
%     plot(xs, ys, 'r*');
% end



for k = 1:num_pts
    pts_3d_s = pts_3d_sub(:, k);
    
            
    % project to pixel coordinate
    % normalize
    xs_n = [ pts_3d_s(1)/pts_3d_s(3); pts_3d_s(2)/pts_3d_s(3)];
%     ys_n = pts_3d_s(2)/pts_3d_s(3);

    % apply distortion
    xd_s = apply_distortion(xs_n, kc_right);
%     r2 = xs_n*xs_n + ys_n*ys_n;
%     xs_g  = 2*kc_right(3)*xs_n*ys_n + kc_right(4)*(r2 + 2*xs_n*xs_n);
%     ys_g = kc_right(3)*(r2 + 2*ys_n*ys_n) + 2*kc_right(4)*xs_n*ys_n;
% 
%     coef = (1 + kc_right(1)*r2 + kc_right(2)*r2^2 + kc_right(5)*r2^3);
%     xs_k = coef*xs_n + xs_g;
%     ys_k = coef*ys_n + ys_g;
% 
%     u_s = fc_right(1)*(xs_k + alpha_c_right*ys_k) + cc_right(1);
%     v_s = fc_right(2)*ys_k + cc_right(2);
    u_s = fc_right(1)*(xd_s(1) + alpha_c_right*xd_s(2)) + cc_right(1);
    v_s = fc_right(2)*xd_s(2) + cc_right(2);

    
    figure(300); hold on
    plot(u_s, v_s, 'r*', 'MarkerSize', 5);
end


    
    