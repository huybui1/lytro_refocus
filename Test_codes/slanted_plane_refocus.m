clear all
clc
close all

% try refocusing on slanted plane

d = 0.2;

% for d = 0.1:0.01:0.3
angle = -215.9*2/279.4;
disp(['d = ', num2str(d)]);
% load Data/Images/Exp3/IMG_0002__Decoded.mat
load Data/Images/Exp5/IMG_0005__Decoded.mat
LF = double(LF);

H = [0.0003671437802,0,-3.119481556e-05,0,0.004106893463;
     0,0.0003464217376,0,-3.83051885e-05,0.005565029721;
	 -0.0023360145,0,0.001830962378,0,-0.3371182605;
     0,-0.002191605232,0,0.00186790715,-0.3448782859;
	 0,0,0,0,1];

% find intersections
[x, y, z, normalization] = find_intersections_slanted_plane(LF, d, angle, H);
% save img0002_slanted_2.mat x y z angle d H
% load img0002_slanted_2.mat

max_x = max(x(:));
min_x = min(x(:));
dx = (max_x - min_x)/300;

max_y = max(y(:));
min_y = min(y(:));
dy = (max_y - min_y)/300;

% dimension of refocus result
height = ceil((max_y - min_y)/dy)
width = ceil((max_x - min_x)/dx)

LF = double(LF);
LF = LF./max(LF(:));
result = zeros(height, width, 3);

% iterate through LF pixels, update corresponding locations on result
max_i = size(LF, 1);
max_j = size(LF, 2);
max_k = size(LF, 3);
max_l = size(LF, 4);
rad = 8*max(dx, dy);
tic
for i = 3:7
    for j = 3:7
        for k=1:max_k
            for l=1:max_l
                current_x = x(i, j, k, l);
                current_y = y(i, j, k, l);
                current_z = z(i, j, k, l);
                n = normalization(i, j, k, l);
%                 n = 1;
                current_vals = squeeze(LF(i, j, k, l, 1:3));
                
                % index of closest pixel
                x_idx0 = floor((current_x - min_x)/dx) + 1;
                y_idx0 = floor((current_y - min_y)/dy) + 1;
                
                x_idx1 = x_idx0 + 1;
                y_idx1 = y_idx0 + 1;
               
                if ((x_idx0 >= 1) && (x_idx1 <= width) && ...
                   (y_idx0 >= 1) && (y_idx1 <= height))
                   
                   % rads
                   r00 = sqrt((min_x + (x_idx0 - 1)*dx - current_x)^2 + (min_y + (y_idx0 - 1)*dy - current_y)^2);
                   r01 = sqrt((min_x + (x_idx0 - 1)*dx - current_x)^2 + (min_y + (y_idx1 - 1)*dy - current_y)^2);
                   r10 = sqrt((min_x + (x_idx1 - 1)*dx - current_x)^2 + (min_y + (y_idx0 - 1)*dy - current_y)^2);
                   r11 = sqrt((min_x + (x_idx1 - 1)*dx - current_x)^2 + (min_y + (y_idx1 - 1)*dy - current_y)^2);
                    
                   
                   % weights                    
                    w00 = max( 1 - r00/rad, 0);
                    w01 = max( 1 - r01/rad, 0);
                    w10 = max( 1 - r10/rad, 0);
                    w11 = max( 1 - r00/rad, 0);
                    
                   % normalize
                   total = w00 + w01 + w10 + w11;
                   if (total ~= 0)
                    w00 = w00/total;
                    w01 = w01/total;
                    w10 = w10/total;
                    w11 = w11/total;
                   end
                   
                   % update result
                   
                   for dim = 1:3
                       result(y_idx0, x_idx0, dim) = result(y_idx0, x_idx0, dim) + w00*current_vals(dim)/(current_z^2 * n^4);
                       result(y_idx1, x_idx0, dim) = result(y_idx1, x_idx0, dim) + w01*current_vals(dim)/(current_z^2 * n^4);
                       result(y_idx0, x_idx1, dim) = result(y_idx0, x_idx1, dim) + w10*current_vals(dim)/(current_z^2 * n^4);
                       result(y_idx1, x_idx1, dim) = result(y_idx1, x_idx1, dim) + w11*current_vals(dim)/(current_z^2 * n^4);
                   end
                end                
%                 % index of closest pixel
%                 x_idx = round((current_x - min_x)/dx + 1);current_z
%                 y_idx = round((current_y - min_y)/dy + 1);
%                        
%                 if ((x_idx >= 1) && (x_idx <= width) && ...
%                         (y_idx >= 1) && (y_idx <= height))
%                     for dim = 1:3
%                       result(y_idx, x_idx, dim) = result(y_idx, x_idx, dim) + LF(i, j, k, l, dim)/current_z^2;
%                     end
%                 end
            end
        end
    end
end
toc

figure;
imagesc(squeeze(LF(4,4,:,:,1:3)));
title('A slice of original LF');

result = result./max(result(:));
figure; 
imagesc(result); colormap gray;
title(['Refocused on slanted plane at distance ', num2str(d)]);
axis image;
axis off;

% end
