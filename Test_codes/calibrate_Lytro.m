clear all
clc
close all

% Read image
I_raw = imread('IMG005.tiff');
figure; imagesc(I_raw); colormap gray; title('Raw light field image');

% Demosaic
I_demosaic = demosaic(I_raw, 'bggr');
figure; imshow(I_demosaic); title('Demosaic');

% Convert to grayscale
I_gray = rgb2gray(I_demosaic);
figure; imshow(I_gray); title('Grayscale');

% Crop
I_gray = I_gray(2457:2712, 2457:2712);
[height, width] = size(I_gray);
figure; imshow(I_gray); title('Cropped');

% Meta params
