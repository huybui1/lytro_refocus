function [x, y, z, normalization] = find_intersections_cylinder(LF, R, d, H)

%% find intersection between rays and a cylinder surface
% Input:
%      LF: light field
%      R : cylinder radius
%      d : distance from main lens to cylinder center axis
%      H : intrinsic matrix (obtained by calibration)

D = 1.0;  % distance between planes s,t and u,v

% iterate through each ray
max_i = size(LF, 1);
max_j = size(LF, 2);
max_k = size(LF, 3);
max_l = size(LF, 4);

% store the intersections' coordinates in 3 arrays
x = zeros(max_i, max_j, max_k, max_l);
y = zeros(max_i, max_j, max_k, max_l);
z = zeros(max_i, max_j, max_k, max_l);
normalization = zeros(max_i, max_j, max_k, max_l);
tic
c = 0;
parfor i = 1:max_i 
    for j = 1:max_j
        for k = 1:max_k
            for l = 1:max_l
                c = c+1
                % back projection (pixel -> ray)
                temp = H*[i; j; k; l; 1];
                s = temp(1);
                t = temp(2);
                u = temp(3);
                v = temp(4);
                
                if ((temp(5) ~= 0) && (temp(5) ~= 1))
                   s = s/temp(5);
                   t = t/temp(5);
                   u = u/temp(5);
                   v = v/temp(5);                   
                end
                   
                % find intersection with surface
                alpha_x = (v - t)/D;
                alpha_y = (u - s)/D;
                
                % equations:
                % [1 + tan(alpha_x)^2] * z^2 + 2 * [t * tan(alpha_x) - d]*z
                % + d^2 + t^2 - R^2  = 0. (1), z > 0 & z < d
                % x = t + z*tan(alpha_x). (2)
                % y = s + z*alpha_y       (3)
                
                % find root of Eq (1)
                C = [ 1 + alpha_x^2, 2*(t*alpha_x - d), d*d + t*t - R*R];
                r = roots(C);
                
                if (~isreal(r))
                    continue;
                end
                
                % choose z such that z > 0 && z < d
                if ((r(1) > 0) && (r(1) <= d))
                    this_z = r(1);
                elseif ((r(2) > 0) && (r(2) <= d))
                        this_z = r(2);
                else
                    continue;
                end
                
                this_x = t + this_z*alpha_x;
                this_y = s + this_z*alpha_y;                        
                
                x(i, j, k, l) = this_x;
                y(i, j, k, l) = this_y;
                z(i, j, k, l) = this_z;
                normalization(i, j, k, l) = this_z/sqrt(this_x^2 + this_y^2 + this_z^2);
            end
        end
    end
end
              