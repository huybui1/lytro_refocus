clear all
clc
close all

% Read color image
I_raw = imread('IMG005.tiff');
I_raw = I_raw(2457:2712, 2457:2712 );
figure; imagesc(I_raw); colormap gray; title('Raw light field image');
% % demosaic
% I_demosaic = demosaic(I_raw, 'bggr');
% 
% figure; imshow(I_demosaic); title('Demosaic raw image');

I_white = LFReadRaw('white_img_982_600.raw');
I_white = I_white(2457:2712, 2457:2712 );
figure; imagesc(I_white); colormap gray; title('White image');

% I_grayscale = rgb2gray(I_demosaic);
% figure; imshow(I_grayscale); title('Converted to grayscale');

% flat (no alignment)
I_flat_no_align = zeros(size(I_raw));
I_flat_no_align(I_white ~=0) = uint16(double(I_raw(I_white~= 0))./double(I_white(I_white~=0)));
figure; imagesc(I_flat_no_align); colormap gray; title('Flat image (no alignment)');

% processing for alignment

% filter = minimum filter
% I_grayscale = uint8(double(I_raw)*255/max(double(I_raw(:))));
I_filt = ordfilt2(I_raw, 1, ones(5,5));
figure; imagesc(I_filt); title('Filtered by minimum filter'); colormap gray

%threshold
thresh = prctile(I_filt(:), 40) % get 80th percentile 
I_thresh = double(I_filt <= thresh);
figure; imagesc(I_thresh); colormap gray; title('Thresholded');

% % fft
I_f = fftshift(fft2(double(I_thresh)));
figure; imagesc(abs(I_f)); colormap gray; title('Frequency domain');
I2 = abs(I_f);
figure; imagesc(I2(90:170, 90:170)); colormap gray


I_f2 = fftshift(log(abs(fft2(double(I_thresh)))));
figure; imagesc(abs(I_f2)); colormap gray;
%===========
% I3 = I2(90:170, 90:170);

[pks, locs] = findpeaks(I2(:),'MINPEAKDISTANCE', 20,'THRESHOLD', 1900, 'SORTSTR', 'descend');
size(locs)
[y, x] = ind2sub([256,256], locs);
Temp  = zeros(size(I2));
for i = 1:min(size(locs,1), 20)
    Temp(y(i), x(i)) = i;
end
figure; imagesc(Temp)
colormap gray
% 
% l = 25e-6; % microlens distance
% p = 1.0e-6; % photosensor pitch
% 
% A0 = l/p*[1 0.5; 0 sqrt(3)/2];
% A0_inv = inv(A0)'

options.RAW_SIZE_ROW = size(I_raw, 1);
options.RAW_SIZE_COL = size(I_raw, 2);
options.PERIOD  = 5;
options.ROI_WIN = 50;

[rotated_white, scale_y, scale_x] = rotationEstimation(double(I_raw), options);
figure; imagesc(rotated_white); colormap gray; title('Rotated white image');