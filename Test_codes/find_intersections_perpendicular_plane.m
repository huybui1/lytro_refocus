function [x, y, normalization] = find_intersections_perpendicular_plane(LF, d, H)

%% find intersection between rays and a cylinder surface
% Input:
%      LF: light field
%      d : distance from main lens to plane
%      H : intrinsic matrix (obtained by calibration)

D = 1.0;  % distance between planes s,t and u,v

% iterate through each ray
max_i = size(LF, 1);
max_j = size(LF, 2);
max_k = size(LF, 3);
max_l = size(LF, 4);

% store the intersections' coordinates in 3 arrays
x = zeros(max_i, max_j, max_k, max_l);
y = zeros(max_i, max_j, max_k, max_l);
normalization = zeros(max_i, max_j, max_k, max_l);

tic
c = 0;
for i = 1:max_i 
    for j = 1:max_j
        for k = 1:max_k
            for l = 1:max_l
                c = c+1;
                % back projection (pixel -> ray)
                temp = H*[i; j; k; l; 1];
                s = temp(1);
                t = temp(2);
                u = temp(3);
                v = temp(4);
                
                if ((temp(5) ~= 0) && (temp(5) ~= 1))
                   s = s/temp(5);
                   t = t/temp(5);
                   u = u/temp(5);
                   v = v/temp(5);                   
                end
                   
                % find intersection with surface
                alpha_x = (v - t)/D;
                alpha_y = (u - s)/D;
                
                               
                this_x = t + d*alpha_x;
                this_y = s + d*alpha_y;                        
                
                x(i, j, k, l) = this_x;
                y(i, j, k, l) = this_y;
                normalization(i, j, k, l) = d/sqrt(this_x^2 + this_y^2 + d^2);
                
            end
        end
    end
end
              