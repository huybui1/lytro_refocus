clear all
clc
close all

GAMMA = 0.41666;
options.RAW_SIZE_COL = 3280;
options.RAW_SIZE_ROW = 3280;
options.GAMMA = GAMMA;

% image file
filename = 'IMG_0005.tiff';
img_folder = './raw_folder/';

% calibration data
calib_folder = './calib_folder/';

img = imread([img_folder, filename]);
img = double(img);

figure; imagesc(img); colormap gray; title('Raw input');

% vignetting correction
load([calib_folder, 'vig.mat']);
img = img.*vig; 
figure; imagesc(img); colormap gray; title('After vignetting correction');

% rescale
img = img./max(img(:));
figure; imagesc(img); colormap gray; title('After rescaling');

% gamma correction
img  = img.^(GAMMA);
figure; imagesc(img); colormap gray; title('After gamma correction');

% hot pixel correction
load([calib_folder, 'mask_black.mat']);
hot_img = hotPixelCorrection(img, mask_black, options);
figure; imagesc(hot_img); colormap gray; title('After hot pixel correction');

% demosaic
demo_img = demosaic(uint16(hot_img.*double(intmax('uint16'))), 'bggr');
figure;
imshow(demo_img); title('Demosaic image');

% rotate image
load([calib_folder, 'rotation.mat']);
rotated_img = imrotate(demo_img, an*180/pi, 'bicubic');

% crop
if offset.Y1(1)>offset.Y3(1)
    rotated_img = rotated_img(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
else
    rotated_img = rotated_img(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
end

figure; imshow(rotated_img); title('Rotated image');


