%DEMO_1  "Free Fall" demo.
%
%   Example
%
%   DEMO_1 runs the "Free Fall" demo.
%
%   See also demo_2, demo_3, demo_4, demo_5, demo_6, demo_7, demo_8,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with default parameters:
% acceleration due to gravity: [0, 0, 0]
% aerodynamic resistance (drag): 0
% axes limits: +/- 1
Particle_System = particle_system

% Earth: acceleration due to gravity: 9.81 m/s/s
% in negative z-direction
gravity = [0 0 -9.81]

% Set the gravity of the particle system
Particle_System.gravity = gravity

% 2D-view for a 2D-motion
view (0, 0)

% Create a particle,
% with default parameters:
% mass: 1
% initial position: [0, 0, 0]
% initial velocity: [0, 0, 0]
% not fixed
% lifespan: inf
Particle = particle (Particle_System)

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 1 millisecond seems to produce a traceable motion on a 3 GHz machine
step_time = 0.001;

% Stop after 0.451 sec
for i = 1 : 451

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end