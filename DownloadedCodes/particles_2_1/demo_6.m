%DEMO_6  "Heavy Chain Mail" demo.
%
%   Example
%
%   DEMO_6 runs the "Heavy Chain Mail" demo.
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_5, demo_7, demo_8,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, -0.4]
% aerodynamic resistance (drag): 0.1
% axes limits: +/- 5
Particle_System = particle_system ([0, 0, -0.4], 0.1, 5);

% 2D-view for a 2D-motion
view (0, 0);

% Define special axes limits for a better view
axis ([0, 6, -1, 1, 0, 6]);

% Create particle matrix
% Loop over all columns
for col = 1 : 5

    % Loop over all rows
    for row = 1 : 5

        % Create a particle,
        % with the following parameters:
        % mass: 1
        % initial position: so that all particles build up a matrix
        % initial velocity: [0, 0, 0]
        % not fixed
        % lifespan: inf
        Particles{row, col} = particle (Particle_System, 1, [col, 0, row], [0, 0, 0], false, inf);

        % The upper left and right particle have to be fixed.
        if row == 5 && col == 1 || row == 5 && col == 5

            % Fix the particle
            Particles{row, col}.fixed = true;

        end

    end

end

% Create horizontal springs
% Loop over all columns
for col = 1 : 4

    % Loop over all rows
    for row = 1 : 5

        % Create a spring between two adjacent particles
        % with the following parameters
        % rest length: 1;
        % strength: 100;
        % damping: 1;
        Springs_horizontal{row, col} = spring (Particle_System, Particles{row, col}, Particles{row, col + 1}, 1, 100, 1);

    end
end

% Create vertical springs
% Loop over all columns
for col = 1 : 5

    % Loop over all rows
    for row = 1 : 4

        % Create a spring between two adjacent particles
        % with the following parameters
        % rest length: 1;
        % strength: 100;
        % damping: 1;
        Springs_vertical{row, col} = spring (Particle_System, Particles{row, col}, Particles{row + 1, col}, 1, 100, 1);

    end
end

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 100 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.1;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end