%DEMO_9  "Catch me if you Can" demo.
%
%   Example
%
%   DEMO_9 runs the "Catch me if you Can (There's a Hole in the Bucket)" demo.
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_5, demo_6, demo_7,
%   demo_8, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, -30]
% aerodynamic resistance (drag): 1
% axes limits: +/- 10
Particle_System = particle_system ([0, 0, -30], 1, 10);

% 2D-view for a 2D-motion
view (0, 0)

% Create the left bucket particle,
% with the following parameters:
% mass: 1
% initial position: [-2, 0, 2]
% initial velocity: [0, 0, 0]
% fixed (because it will be positioned with the mouse)
% lifespan: inf
Particle_left = particle (Particle_System, 1, [-2, 0, 2], [0, 0, 0], true, inf);

% Create a right bucket particle,
% with the following parameters:
% mass: 1
% initial position: [2, 0, 2]
% initial velocity: [0, 0, 0]
% fixed (because it will be positioned with the mouse)
% lifespan: inf
Particle_right = particle (Particle_System, 1, [2, 0, 2], [0, 0, 0], true, inf);

% Create the bottom bucket particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 0]
% initial velocity: [0, 0, 0]
% fixed (because it will be positioned with the mouse)
% lifespan: inf
Particle_bottom = particle (Particle_System, 1, [0, 0, 0], [0, 0, 0], true, inf);

% Create the ball particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 5]
% initial velocity: [0, 0, 0]
% not fixed
% lifespan: inf
Particle_ball = particle (Particle_System, 1, [0, 0, 5], [0, 0, 0], false, inf);

% Create a repulsion between bucket and ball particles
% with the following parameters
% strength: -100 (distraction)
% minimum distance: eps ("no" minimum distance)
Attraction_1 = attraction (Particle_System, Particle_left, Particle_ball, -100, eps);

% Create a repulsion between bucket and ball particles
% with the following parameters
% strength: -100 (distraction)
% minimum distance: eps ("no" minimum distance)
Attraction_2 = attraction (Particle_System, Particle_right, Particle_ball, -100, eps);

% Create a repulsion between bucket and ball particles
% with the following parameters
% strength: -200 (distraction)
% minimum distance: eps ("no" minimum distance)
Attraction_3 = attraction (Particle_System, Particle_bottom, Particle_ball, -100, eps);

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 10 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.01;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Determine the current 3D mouse position
    current_point = mean (get (gca, 'currentpoint'));
    
    % Make sure the bucket particles move only in 2D
    current_point(2) = 0;

    % Set the position of the left bucket particle
    % (move it around with the mouse)
    Particle_left.position = current_point + [-2 0 2];

    % Set the position of the right bucket particle
    % (move it around with the mouse)
    Particle_right.position = current_point + [2 0 2];

    % Set the position of the bottom bucket particle
    % (move it around with the mouse)
    Particle_bottom.position = current_point + [0 0 0];

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end