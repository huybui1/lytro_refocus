%DEMO_8  "Keep it up" demo.
%
%   Example
%
%   DEMO_8 runs the "Keep it up" demo.
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_5, demo_6, demo_7,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, -30]
% aerodynamic resistance (drag): 1
% axes limits: +/- 10
Particle_System = particle_system ([0, 0, -20], 0, 10);

% 2D-view for a 2D-motion
view (0, 0)

% Create a particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 0]
% initial velocity: [0, 0, 0]
% fixed (because it will be positioned with the mouse)
% lifespan: inf
Particle_1 = particle (Particle_System, 1, [0, 0, 0], [0, 0, 0], true, inf);

% Create the ball particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 0]
% initial velocity: [0, 0, 0]
% not fixed
% lifespan: inf
Particle_2 = particle (Particle_System, 1, [0, 0, 0], [0, 0, 0], false, inf);

% Create a repulsion between both particles
% with the following parameters
% strength: -100 (distraction)
% minimum distance: eps ("no" minimum distance)
Attraction = attraction (Particle_System, Particle_1, Particle_2, -100, eps);

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 10 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.01;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Determine the current 3D mouse position
    current_point = mean (get (gca, 'currentpoint'));
    
    % Make sure the "mouse particle" moves only in 2D
    current_point(2) = 0;

    % Set the position of the particle
    % (move it around with the mouse)
    Particle_1.position = current_point;

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end