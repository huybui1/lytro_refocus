%DEMO_12  "Three-Body Eight" demo.
%
%   Example
%
%   DEMO_12 runs the "Three-Body Eight" demo.
%   This is a stable(!) arrangement of three masses.
%   It was discovered quite recently (2000):
%   http://www.jstor.org/stable/2661357
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_5, demo_6, demo_7, 
%   demo_8, demo_9, demo_10, demo_11.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
Particle_System = particle_system ([0, 0, 0], 0, 2);

% 2D-view for a 2D-motion
view (0, 0);

% Define initial positions and velocities
% (numerical data from http://www.jstor.org/stable/2661357)
initial_position = [0.97000436, 0, -0.2430875];
initial_velocity = [-0.93240737, 0, -0.86473146];

% Create three particles
Particle_1 = particle (Particle_System, 1, initial_position , -initial_velocity/2, false, inf);
Particle_2 = particle (Particle_System, 1, -initial_position, -initial_velocity/2, false, inf);
Particle_3 = particle (Particle_System, 1, [0, 0, 0], initial_velocity, false, inf);

% A slight distortion of the initial position of a particle
% draws a "beautiful" stable(!) "rotating eight".
% To see it, comment the line above and uncomment the following line.
%Particle_3 = particle (Particle_System, 1, [0.01, 0, 0], initial_velocity, false, inf);

% Create attractions between all particles
% The attraction strength can be reduced slightly (to about 0.9)
% leading to distorted but still quite stable orbits
Attraction_1 = attraction (Particle_System, Particle_1, Particle_2, 1, eps);
Attraction_2 = attraction (Particle_System, Particle_1, Particle_3, 1, eps);
Attraction_3 = attraction (Particle_System, Particle_2, Particle_3, 1, eps);

% Define the step time of the simulation algorithm.
step_time = 0.05;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Simulate one single time step
    Particle_System.advance_time (step_time);

    % Create quick-and-dirty trail
    % (Comment the following lines on slow computers)
    pp = num2cell (Particle_3.position);
    text (pp{:}, '*', 'color', 'r')

end