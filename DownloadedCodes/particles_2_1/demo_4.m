%DEMO_4  "Gimme ya Energy" demo.
%
%   Example
%
%   DEMO_4 runs the "Gimme ya Energy" demo.
%
%   See also demo_1, demo_2, demo_3, demo_5, demo_6, demo_7, demo_8,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, 0]
% aerodynamic resistance (drag): 0
% axes limits: +/- 3
Particle_System = particle_system ([0, 0, 0], 0, 3);

% 2D-view for a 2D-motion
view (0, 0);

% Create a particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 0]
% initial velocity: [0, 0, 0]
% fixed
% lifespan: inf
Particle_1 = particle (Particle_System, 1, [0, 0, 0], [0, 0, 0], true, inf);

% Create a second particle,
% with the following parameters:
% mass: 10
% initial position: [1, 0, 0]
% initial velocity: [0, 0, 0]
% not fixed
% lifespan: inf
Particle_2 = particle (Particle_System, 10, [1, 0, 0], [0, 0, 0], false, inf);

% Make the particle with a mass of 10 look bigger
set (Particle_2.graphics_handle, 'markersize', 60);

% Create a third particle,
% with the following parameters:
% mass: 1
% initial position: [2, 0, 0]
% initial velocity: [0, 0, 5]
% not fixed
% lifespan: inf
Particle_3 = particle (Particle_System, 1, [2, 0, 0], [0, 0, 5], false, inf);

% Create a spring between first and second particle
% with the following parameters
% rest length: 1
% strength: 10
% damping: 1
Spring_1 = spring (Particle_System, Particle_1, Particle_2, 1, 10, 1);

% Create a second spring between second and third particle
% with the following parameters
% rest length: 1
% strength: 10
% damping: 1
Spring_2 = spring (Particle_System, Particle_2, Particle_3, 1, 10, 1);

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 100 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.1;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end