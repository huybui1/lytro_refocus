%DEMO_10  "The Hose" demo.
%
%   Example
%
%   DEMO_10 runs the "The Hose" demo.
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_5, demo_6, demo_7,
%   demo_8, demo_9, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, -10]
% aerodynamic resistance (drag): 1
% axes limits: +/- 1
Particle_System = particle_system ([0, 0, -10], 1, 1);

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 20 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.02;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Determine the current mouse position
    current_point = mean (get (gca, 'currentpoint'));

    % Create one  particle,
    % with the following parameters:
    % mass: 1
    % initial position: [0, 0, 0]
    % initial velocity: depending on mouse position and some noise
    % not fixed
    % lifespan: 0.5
    Particles{i} = particle (Particle_System, 1, [0, 0, 0], 4*current_point + 0.2*rand (1, 3), false, 0.5);

    % Blood to water
    set (Particles{i}.graphics_handle, 'color' , [0.5, 0.5, 1])
    
    % Simulate one single time step
    Particle_System.advance_time (step_time);

end