%DEMO_5  "Magic Chain Cable" demo.
%
%   Example
%
%   DEMO_5 runs the "Magic Chain Cable" demo.
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_6, demo_7, demo_8,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, -1]
% aerodynamic resistance (drag): 0.1
% axes limits: +/- 10
Particle_System = particle_system ([0, 0, -1], 0.1, 10);

% Number of chain links (particles)
n_particles = 20;

% Loop over all particles
for i = 1 : n_particles

    % Create a particle,
    % with the following parameters:
    % mass: 0.2
    % initial position: [0, 0, 0]
    % initial velocity: [0, 0, 0]
    % not fixed
    % lifespan: inf
    Particles{i} = particle (Particle_System, 0.2, [0, 0, 0], [0, 0, 0], false, inf);

    % The first and last particle have to be fixed.
    % Their position will be changed externally during the simulation.
    if i == 1 || i == n_particles
    
        % Fix the particle
        Particles{i}.fixed = true;
        
    end
    
end

% Loop over all chain link connections (springs)
for i = 1 : n_particles - 1

    % Create a spring between two adjacent particles
    % with the following parameters
    % rest length: 1
    % strength: 100
    % damping: 4
    Spring{i} = spring (Particle_System, Particles{i}, Particles{i + 1}, 1, 100, 4);

end

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 50 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.05;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf


    % Set the position of the first particle
    % (move it around in 3D space)
    Particles{1}.position = ...
        [10*sin(0.01*i), 10*sin(0.011*i), 10*sin(0.012*i)];

    % Set the position of the last particle
    % (move it around in 3D space)
    Particles{n_particles}.position = ...
        [-10*sin(0.013*i), 10*sin(0.014*i), -10*sin(0.015*i)];

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end