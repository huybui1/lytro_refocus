%DEMO_3  "Bungee Jumping" demo.
%
%   Example
%
%   DEMO_3 runs the "Bungee Jumping" demo.
%
%   See also demo_1, demo_2, demo_4, demo_5, demo_6, demo_7, demo_8,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% gravity: [0, 0, -9.81]
% aerodynamic resistance (drag): 5
% axes limits: +/- 200
Particle_System = particle_system ([0, 0, -9.81], 5, 200);

% 2D-view for a 2D-motion
% view (0, 0);

% Create a particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 150]
% initial velocity: [0, 0, 0]
% fixed
% lifespan: inf
Particle_1 = particle (Particle_System, 1, [0, 0, 150], [0, 0, 0], true, inf);

% Create a second particle,
% with the following parameters:
% mass: 70
% initial position: [100, 0, 150]
% initial velocity: [0, 0, 0]
% not fixed
% lifespan: inf
Particle_2 = particle (Particle_System, 70, [100, 0, 150], [0, 0, 0], false, inf);

% Create a spring between both particles
% with the following parameters
% rest length: 100
% strength: 6
% damping: 0.1
Spring = spring (Particle_System, Particle_1, Particle_2, 100, 6, 0.1);

% Make the spring look more like a spring
set (Spring.graphics_handle, 'linewidth', 10, 'linestyle', ':')

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 100 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.1;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end