%DEMO_2  "Bullet Time" demo.
%
%   Example
%
%   DEMO_2 runs the "Bullet Time" demo.
%
%   See also demo_1, demo_3, demo_4, demo_5, demo_6, demo_7, demo_8,
%   demo_9, demo_10, demo_11, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% no gravity: [0, 0, 0]
% aerodynamic resistance (drag): 10
% axes limits: +/- 1
Particle_System = particle_system ([0, 0, 0], 10, 1);

% 2D-view for a 2D-motion
view (0, 0);

% Create a particle,
% with the following parameters:
% mass: 1
% initial position: [-1, 0, 0]
% initial velocity: [20, 0, 0]
% not fixed
% lifespan: inf
Particle = particle (Particle_System, 1, [-1, 0, 0], [20, 0, 0], false, inf);

% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 1 millisecond seems to produce a traceable motion on a 3 GHz machine
step_time = 0.001;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Simulate one single time step
    Particle_System.advance_time (step_time);

end