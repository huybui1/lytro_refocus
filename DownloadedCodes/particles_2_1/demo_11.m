%DEMO_11  "Polyhedrons" demo.
%
%   Example
%
%   DEMO_11 runs the "Polyhedrons" demo.
%
%   See also demo_1, demo_2, demo_3, demo_4, demo_5, demo_6, demo_7,
%   demo_8, demo_9, demo_10, demo_12.

%   Copyright 2008-2008, buchholz.hs-bremen.de

% Prevent hangover
clear all
close all
clc

% Create the particle system
% that contains all particles, springs, and attractions,
% with the following parameters:
% no gravity: [0, 0, 0]
% aerodynamic resistance (drag): 1
% axes limits: +/- 2
Particle_System = particle_system ([0, 0, 0], 1, 2);

% Turn off all axis labeling, tick marks and background
axis off;

% Create the center particle,
% with the following parameters:
% mass: 1
% initial position: [0, 0, 0]
% initial velocity: [0, 0, 0]
% fixed
% lifespan: inf
Particle_center = particle (Particle_System, 1, [0, 0, 0], [0, 0, 0], true, inf);

% Define the number of vertices of the polyhedron
% n_particles = 3: Equilateral triangle
% n_particles = 4: Tetrahedron
% n_particles = 5: Deltahedron, triangular dipyramid
% n_particles = 6: Octahedron, square dipyramid
% n_particles = 8: Square antiprism (not a cube!)
% n_particles = 12: Icosahedron
% n_particles = 20: Name unknown (not a dodecahedron, 3 near squares!)
n_particles = 12;

% Loop over all vertices of the polyhedron
for i = 1 : n_particles

    % Create a particle,
    % with the following parameters:
    % mass: 1
    % initial position: random
    % initial velocity: [0, 0, 0]
    % not fixed
    % lifespan: inf
    Particle{i} = particle (Particle_System, 1, randn (1, 3), [0, 0, 0], false, inf);

    % We do not want to see the red particles 
    set (Particle{i}.graphics_handle, 'marker', 'none');

    % Create a spring between the central and one of the other particles
    % with the following parameters
    % rest length: 1
    % strength: 10
    % damping: 1
    Spring{i} = spring (Particle_System, Particle_center, Particle{i}, 1, 10, 1);

end

% This nested loops create repulsions between every particle
% and every other particle.
% Outer loop over all particles
for i = 1 : n_particles

    % Inner loop over all particles
    for j = 1 : n_particles

        % Make sure there is only one repulsion between two particles
        if i < j

            % Create an attraction between both particles
            % with the following parameters
            % strength: -10 (repulsion)
            % minimum distance: 0.01 (just in case...)
            Attraction{i, j} = attraction (Particle_System, Particle{i}, Particle{j}, -10, 0.01);

        end

    end

end


% Define the step time of the simulation algorithm.
% Since we do not have a real-time system,
% the speed of motion is directly proportional to the step time.
% 20 milliseconds seem to produce a traceable motion on a 3 GHz machine
step_time = 0.2;

% Infinite loop.
% Close figure to end or press Ctrl-C
for i = 1 : inf

    % Get the positions of all particles in one long vector
    particles_positions = Particle_System.get_particles_positions;

    % Reshape the positions into a matrix with three rows (x, y, z)
    positions = reshape (particles_positions, 3, n_particles + 1);

    % x, y, and z have to be column vectors
    vertices = positions';
    
    % Find the faces of the convex hull of the polyhedron
    faces = convhulln (vertices);

    % Buffer the number of faces of the polyhedron
    n_faces = size (faces, 1);

    % Initialize (clear) the face handle vector
    % This is necessary because the number of faces 
    % can decrease during simulation
    patch_handle = [];
    
    % This loop over all faces uses the normal vectors of the faces
    % to dye the faces. 
    for i_faces = 1 : n_faces

        % Extract the three vertices of the current face
        vertex_1 = vertices(faces(i_faces, 1), :);
        vertex_2 = vertices(faces(i_faces, 2), :);
        vertex_3 = vertices(faces(i_faces, 3), :);

        % Compute two of the three edge vectors of the current face
        edge_1 = vertex_1 - vertex_2;
        edge_2 = vertex_1 - vertex_3;

        % The cross product of two vectors is perpendicular to both vectors
        normal = cross (edge_1, edge_2);

        % Scale the normal vector into the 0..1 RGB range
        face_color = 0.5*(normal/norm (normal) + 1);
        
        % Draw one patch of the polyhedron 
        patch_handle(i_faces) = patch ( ...
        'vertices', vertices(faces(i_faces, :), :), ...
        'faces', [1 2 3], ...
        'facecolor', face_color, ...
        'facealpha', 0.9);
    
    end

    % Otherwise the polyhedron would chase the particles
    drawnow

    % Simulate one single time step
    Particle_System.advance_time (step_time);

    % Delete the "old" polyhedron
    delete (patch_handle)

end