import os;
import shutil;

newdir = "DATA";
if not os.path.exists(newdir):
    os.makedirs(newdir);

list_sub_dirs = os.walk(".").next()[1]
# print(list_sub_dirs);
count = 1;
for x in list_sub_dirs:
    # print(count)
    # print(x);
    os.chdir(x);
    for filename in os.listdir("."):
        # if os.path.isfile(filename):
        if filename.startswith("raw.lfp"):
             # print(filename)
			print(x)
			newname = "IMG_" + str(count).zfill(4) + ".lfp"
			shutil.copy(filename, "..\\" + newdir + "\\" + newname )
			count = count + 1;
    os.chdir("..")
            
