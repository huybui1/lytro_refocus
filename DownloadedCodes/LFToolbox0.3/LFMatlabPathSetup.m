% LFMatlabPathSetup - convenience function to add the light field toolbox to matlab's path
%
% It may be convenient to add this to your startup.m file.

% Part of LF Toolbox v0.3 released 10-Nov-2014
% Copyright (c) 2013, 2014 Donald G. Dansereau

function LFMatlabPathSetup

% Find the path to this script, and use it as the base path
LFToolboxPath = fileparts(mfilename('fullpath'));
fprintf('Adding paths for LF Toolbox v0.3 released 10-Nov-2014... ');

addpath( fullfile(LFToolboxPath) );
addpath( fullfile(LFToolboxPath, 'SupportFunctions') );
addpath( fullfile(LFToolboxPath, 'SupportFunctions', 'CameraCal') );

fprintf('Done.\n');