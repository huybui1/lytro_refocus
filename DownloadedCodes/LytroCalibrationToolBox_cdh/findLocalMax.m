function maxes = findLocalMax(im,win,roi)

    win = round(win);
    domain = ones([win win]);
    domain(floor(win/2)+1,floor(win/2)+1) = 0;
    maxFilter = zeros(size(im));
    maxFilter(roi.y:roi.y+roi.height,roi.x:roi.x+roi.width) = ordfilt2(im(roi.y:roi.y+roi.height,roi.x:roi.x+roi.width),win*win-1,domain);
    maxes = zeros(size(im));
    maxes(roi.y:roi.y+roi.height,roi.x:roi.x+roi.width) = im(roi.y:roi.y+roi.height,roi.x:roi.x+roi.width) >= maxFilter(roi.y:roi.y+roi.height,roi.x:roi.x+roi.width);
end