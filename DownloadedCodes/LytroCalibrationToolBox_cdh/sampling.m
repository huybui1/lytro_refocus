function sampling()
    
    clear all;
    
    options.RAW_SIZE_COL = 3280;
    options.RAW_SIZE_ROW = 3280;
    options.GAMMA = 0.42;
    calib_folder = './calib_folder/';
    img_folder = './raw_folder/';
    
    load(sprintf('%s/vig.mat',calib_folder));
    load(sprintf('%s/mask_black.mat',calib_folder));
    load(sprintf('%s/rotation.mat',calib_folder));
    load(sprintf('%s/center.mat',calib_folder));
    
    FolderList = dir(img_folder);
    for imgNum = 3:length(FolderList)
        [~, body, ext] = fileparts(FolderList(imgNum).name);
        if( strcmp('.raw',ext)  || strcmp('.RAW',ext))
            fprintf('load raw image...\n');
            img = loadRawImage(img_folder,FolderList,imgNum,vig,options);
            
            fprintf('hot pixel correction...\n');
            hot_img = hotPixelCorrection(img,mask_black,options);
            
            fprintf('demosaic...\n');
            demo_img = demosaic(uint16(hot_img.*double(intmax('uint16'))),'bggr');
            
            fprintf('rotation...\n');
            rotated_img = imrotate(demo_img,an*180/pi,'bicubic');
            if offset.Y1(1)>offset.Y3(1)
                rotated_img = rotated_img(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1),:);
            else
                rotated_img = rotated_img(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1),:);
            end
            
            fprintf('start making sub images...\n');
            makeView(rotated_img,centerY,centerX,height,width,body);
        end
    end
end