function hotPixel(avg_black)
    m = mean(avg_black(:));
    mask_black = zeros(size(avg_black));
    index = avg_black > 2*m;
    mask_black(index) = 1;
    save('./calib_folder/mask_black.mat','mask_black');
end