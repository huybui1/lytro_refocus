function avg_black = loadBlackImage(options)

    RAW_SIZE_ROW = options.RAW_SIZE_ROW;
    RAW_SIZE_COL = options.RAW_SIZE_COL;
    GAMMA = options.GAMMA;
    %%Read black image patterns
    black_folder = './black_folder/';
    black_folder_list = dir(black_folder);

    fprintf(1, 'averaging black images...\n');
    black_cnt = 0;    
    avg_black = zeros([RAW_SIZE_ROW RAW_SIZE_COL],'double');

     for file = 1 : length(black_folder_list) 
        [~, ~, ext] = fileparts(black_folder_list(file).name);
            if( strcmp('.raw',ext)  || strcmp('.RAW',ext))
                black_cnt = black_cnt + 1;
                fileId = fopen([black_folder black_folder_list(file).name]);
                data = fread(fileId, inf, '*ubit12');
                data = reshape(data,[RAW_SIZE_ROW RAW_SIZE_COL])';
                avg_black = avg_black + double(data);
                fclose(fileId);
            elseif (strcmp('.tif', ext) || strcmp('.tiff', ext))
                black_cnt = black_cnt + 1;                
                data = imread([black_folder black_folder_list(file).name]);
                avg_black = avg_black + double(data);                
            end
    end
    avg_black = round(avg_black/black_cnt); 
    avg_black = avg_black ./ max(avg_black(:));
    avg_black = avg_black .^ (GAMMA);
    
    imwrite(avg_black,'./calib_folder/averaged_black.tif');
end