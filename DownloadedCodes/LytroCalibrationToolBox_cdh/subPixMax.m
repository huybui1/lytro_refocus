function [index Xsub Ysub] = subPixMax(im,maxes,options)
 
    RAW_SIZE_ROW = options.RAW_SIZE_ROW;
    RAW_SIZE_COL = options.RAW_SIZE_COL;
    
    ind = find(maxes==1);
    [Y X] = ind2sub([RAW_SIZE_ROW RAW_SIZE_COL],ind);
    
    xCo = [-1 0 1 -1 0 1 -1 0 1];
    yCo = [-1 -1 -1 0 0 0 1 1 1];
    one = [1 1 1 1 1 1 1 1 1];
    a = [xCo.*xCo , xCo.*yCo, xCo, yCo.*yCo, yCo, one];
    A = reshape(a,[length(xCo) length(a)/length(xCo)]);
    
    x = repmat(X,1,9) + repmat(xCo,length(ind),1);
    y = repmat(Y,1,9) + repmat(yCo,length(ind),1);
    
    i = sub2ind([RAW_SIZE_ROW RAW_SIZE_COL],y,x);
    b = im(i)';
    coef = pinv(A)*b;
    
    dx = (coef(5,:).*coef(2,:)-2*coef(3,:).*coef(4,:))./(4*coef(1,:).*coef(4,:)-coef(2,:).^2);
    dy = (coef(3,:).*coef(2,:)-2*coef(5,:).*coef(1,:))./(4*coef(4,:).*coef(1,:)-coef(2,:).^2);
    
    Xsub = X+max(min(dx',1),-1);
    Ysub = Y+max(min(dy',1),-1);
    index = sub2ind([RAW_SIZE_COL RAW_SIZE_ROW],Ysub,Xsub);
end