function [rotated scale_y scale_x] = rotationEstimation(raw,options)
    
    RAW_SIZE_ROW = options.RAW_SIZE_ROW;
    RAW_SIZE_COL = options.RAW_SIZE_COL;
    PERIOD = options.PERIOD;
    ROI_WIN = options.ROI_WIN;
    
    % fft
    raw_dft = fftshift(log(abs(fft2(raw))));
    
    % initial expectation
    hPeak0 = [-RAW_SIZE_ROW/(2*PERIOD*sin(60*pi/180)) 0];
    vPeak0 = [0 RAW_SIZE_COL/PERIOD];
    
    hRoi.y = floor(hPeak0(1)+RAW_SIZE_ROW/2-ROI_WIN)+1;
    hRoi.x = floor(hPeak0(2)+RAW_SIZE_COL/2-ROI_WIN)+1;
    hRoi.height = 2*ROI_WIN;
    hRoi.width = 2*ROI_WIN;
    
    vRoi.y = floor(vPeak0(1)+RAW_SIZE_ROW/2-ROI_WIN)+1;
    vRoi.x = floor(vPeak0(2)+RAW_SIZE_COL/2-ROI_WIN)+1;
    vRoi.height = 2*ROI_WIN;
    vRoi.width = 2*ROI_WIN;
    
    % find local maximum
    
    figure; imagesc(raw_dft); colormap gray; hold on;
    line([hRoi.x, hRoi.x + hRoi.width], [hRoi.y, hRoi.y]); hold on
    line([hRoi.x, hRoi.x], [hRoi.y, hRoi.y + hRoi.height]); hold on
    line([hRoi.x + hRoi.width, hRoi.x + hRoi.width], [hRoi.y, hRoi.y + hRoi.height]); hold on
    line([hRoi.x, hRoi.x + hRoi.width], [hRoi.y + hRoi.height, hRoi.y + hRoi.height]); hold off
    
    hMaxes = findLocalMax(raw_dft,ROI_WIN,hRoi);
    
    figure; imagesc(raw_dft); colormap gray; hold on;
    line([vRoi.x, vRoi.x + vRoi.width], [vRoi.y, vRoi.y]); hold on
    line([vRoi.x, vRoi.x], [vRoi.y, vRoi.y + vRoi.height]); hold on
    line([vRoi.x + vRoi.width, vRoi.x + vRoi.width], [vRoi.y, vRoi.y + vRoi.height]); hold on
    line([vRoi.x, vRoi.x + vRoi.width], [vRoi.y + vRoi.height, vRoi.y + vRoi.height]); hold off
    
    vMaxes = findLocalMax(raw_dft,ROI_WIN,vRoi);
    
    % threshold
    meanValue = mean(raw_dft(:));
    stdValue = std(raw_dft(:));
    th = meanValue+ 3*stdValue;
    
    maxesTh = (hMaxes==1 | vMaxes==1) & raw_dft>=th ;   
    
    % sub pixel precision
    [index Xsub Ysub] = subPixMax(raw_dft,maxesTh,options);
    
    Xsub = Xsub - floor(RAW_SIZE_COL/2)-1;
    Ysub = Ysub - floor(RAW_SIZE_ROW/2)-1;
    
    % find best one
    m = min( abs((Xsub - hPeak0(2)).^2 + (Ysub - hPeak0(1)).^2) );
    index = find(m ==  abs((Xsub - hPeak0(2)).^2 + (Ysub - hPeak0(1)).^2) );
    hPeak(1) = Ysub(index);
    hPeak(2) = Xsub(index);
    
    m = min( abs((Xsub - vPeak0(2)).^2 + (Ysub - vPeak0(1)).^2) );
    index = find(m == abs((Xsub - vPeak0(2)).^2 + (Ysub - vPeak0(1)).^2));
    vPeak(1) = Ysub(index);
    vPeak(2) = Xsub(index);
    
    an = -( atan(-vPeak(1)/vPeak(2)) - atan(-hPeak(2)/hPeak(1)) )/2;
    
    rotated = imrotate(raw,an*180/pi,'bicubic');
    
    
    scale_x = (PERIOD/((RAW_SIZE_COL)/abs(vPeak(2))));
    scale_y = (PERIOD*2*sin(60*pi/180)/((RAW_SIZE_COL)/abs(hPeak(1))));
    
    
    % cut boundary
    
    sz = size(rotated);
    nzero = find(rotated~=0);               
    
    [Y X] = ind2sub([sz(1) sz(2)],nzero);
    
    m = min( abs((1 - X).^2 + (1 - Y).^2) );
    index = find(m == abs((1 - X).^2 + (1 - Y).^2)  );
    x1_y1 = nzero(index);
       
    m = min( abs((1 - X).^2 + (sz(1) - Y).^2) );
    index = find(m == abs((1 - X).^2 + (sz(1) - Y).^2)  );
    x2_y2 = nzero(index);
    
    m = min( abs((RAW_SIZE_COL - X).^2 + (1 - Y).^2) );
    index = find(m == abs((RAW_SIZE_COL - X).^2 + (1 - Y).^2)  );
    x3_y3 = nzero(index);
    
    m = min( abs((sz(2) - X).^2 + (sz(1) - Y).^2) );
    index = find(m == abs((sz(2) - X).^2 + (sz(1) - Y).^2)  );
    x4_y4 = nzero(index);
    
    [offset.Y1 offset.X1] = ind2sub([sz(1) sz(2)],x1_y1);
    [offset.Y2 offset.X2] = ind2sub([sz(1) sz(2)],x2_y2);
    [offset.Y3 offset.X3] = ind2sub([sz(1) sz(2)],x3_y3);
    [offset.Y4 offset.X4] = ind2sub([sz(1) sz(2)],x4_y4);
    
    if offset.Y1(1)>offset.Y3(1)
        rotated = rotated(offset.Y1(1):offset.Y4(1),offset.X2(1):offset.X3(1));
    else
        rotated = rotated(offset.Y3(1):offset.Y2(1),offset.X1(1):offset.X4(1));
    end
    
    imwrite(rotated,'./calib_folder/rotated.tif');
    save('./calib_folder/rotation.mat','an','offset');
end