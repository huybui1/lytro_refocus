function calibration
    options.RAW_SIZE_COL = 3280;
    options.RAW_SIZE_ROW = 3280;
    options.GAMMA = 0.41666;
    options.PERIOD = 5;
    options.ROI_WIN = 50;
    options.LM_WIN=7;
    
    avg_white = loadWhiteImage(options);  
    avg_white = uint16((avg_white).*double(intmax('uint16')));
    
    avg_black = loadBlackImage(options);
    hotPixel(avg_black);
    deme_white = demosaic(avg_white,'bggr');
    gray_white = rgb2gray(deme_white);
    norm_white = imadjust(gray_white);

    imwrite(deme_white,'./calib_folder/deme_white.tif');
    imwrite(gray_white,'./calib_folder/gray_white.tif');
    imwrite(norm_white,'./calib_folder/norm_white.tif');
    clear avg_white avg_black deme_white gray_white;
    
    [rotated_white scale_y scale_x]= rotationEstimation(norm_white,options);
    
    options.scale_y = scale_y;
    options.scale_x = scale_x;
    
    centerEstimation(rotated_white,options);
    
end