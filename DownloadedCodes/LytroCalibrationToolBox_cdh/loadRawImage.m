function img = loadRawImage(img_folder,FolderList,imgNum,vig,options)

        RAW_SIZE_ROW = options.RAW_SIZE_ROW;
        RAW_SIZE_COL = options.RAW_SIZE_COL;
        GAMMA = options.GAMMA;
        
        fileId = fopen([img_folder FolderList(imgNum).name]);
        img = fread(fileId, inf, '*ubit16');
        img = double(reshape(img,[RAW_SIZE_ROW RAW_SIZE_COL])').*vig;
        img = img ./ max(img(:));
        img = img .^ (GAMMA);
        fclose(fileId);               
end