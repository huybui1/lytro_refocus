function centerEstimation(raw,options)
    
    scale_y = options.scale_y;
    scale_x = options.scale_x;
    
    PERIOD = options.PERIOD;
    options.RAW_SIZE_ROW = size(raw,1);
    options.RAW_SIZE_COL = size(raw,2);
    se = strel('disk',round(PERIOD/2));        
    ero = imerode(double(raw),se);
    
    %ero = ero(6:end-5,6:end-5);
    Roi.y = 1;
    Roi.x = 1;
    Roi.height = size(ero,1)-1;
    Roi.width = size(ero,2)-1;
    
    % local maximum
    maxes = findLocalMax(ero,PERIOD*2-1,Roi);
    maxes(1:PERIOD+2,:) = 0;
    maxes(end-2:end,:) = 0;
    maxes(:,1:PERIOD+2) = 0;
    maxes(:,end-2:end) = 0;
    
    maxesTh = (maxes==1) ;
  
    
    % sub pixel precision
    [index Xsub Ysub] = subPixMax(ero,maxesTh,options);
    
    m = min(abs(Xsub.^2+Ysub.^2));
    index = find(m == abs(Xsub.^2+Ysub.^2));
    Xpivot = Xsub(index);
    Ypivot = Ysub(index);   
    
    grid_dy = 4*PERIOD*cos(pi*30/180)/scale_y;
    grid_x_shift  = 2*PERIOD*sin(pi*30/180)/scale_x;
    grid_y_shift  = 2*PERIOD*cos(pi*30/180)/scale_y;
    grid_dx = 2*PERIOD/scale_x;
    
    [grid.X2,grid.Y2] = meshgrid((Xpivot+grid_x_shift):grid_dx:(size(raw,2)-PERIOD-2), (Ypivot+grid_y_shift):grid_dy:(size(raw,1)-PERIOD-2));
    [grid.X1,grid.Y1] = meshgrid(Xpivot:grid_dx:(Xpivot+grid_dx*(size(grid.X2,2)-1)), Ypivot:grid_dy:(Ypivot+grid_dy*(size(grid.X2,1)-1)));
    
    Xc = Xsub;
    Yc = Ysub;
    
    DT = DelaunayTri(Xc,Yc);
    CORESP1 = nearestNeighbor(DT, grid.X1(:),grid.Y1(:));
    CORESP2 = nearestNeighbor(DT, grid.X2(:),grid.Y2(:));

    centerX = [Xc(CORESP1) ; Xc(CORESP2)];
    centerY = [Yc(CORESP1) ; Yc(CORESP2)];
    
    center = sub2ind([options.RAW_SIZE_ROW options.RAW_SIZE_COL],  round(centerY),round(centerX));
    
    nraw = uint16(raw);
    nraw(round(center)) = 0;
    
    width = size(grid.X1,2);
    height = size(grid.X1,1);
    
    imwrite(nraw,'./calib_folder/center.tif');
    save('./calib_folder/center.mat','centerX','centerY','width','height');
end