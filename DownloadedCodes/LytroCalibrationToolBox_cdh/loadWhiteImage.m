function avg_white = loadWhiteImage(options)

    RAW_SIZE_ROW = options.RAW_SIZE_ROW;
    RAW_SIZE_COL = options.RAW_SIZE_COL;
    GAMMA = options.GAMMA;
    %%Read white image patterns
    white_folder = './white_folder/';
    white_folder_list = dir(white_folder);

    fprintf(1, 'averaging white images...\n');
    white_cnt = 0;    
    avg_white = zeros([RAW_SIZE_ROW RAW_SIZE_COL],'double');

     for file = 1 : length(white_folder_list) 
        [~, NAME, ext] = fileparts(white_folder_list(file).name);
            if( strcmp('.raw',ext)  || strcmp('.RAW',ext))
                white_cnt = white_cnt + 1;
                fileId = fopen([white_folder white_folder_list(file).name]);
                data = fread(fileId, inf, '*ubit12');
                data = reshape(data,[RAW_SIZE_ROW RAW_SIZE_COL])';
                avg_white = avg_white + double(data);
                fclose(fileId);
            elseif (strcmp('.tif', ext) || strcmp('.tiff', ext))
                white_cnt = white_cnt + 1;
                data = imread([white_folder white_folder_list(file).name]);
                avg_white = avg_white + double(data);
            end  
    end
    avg_white = round(avg_white/white_cnt); 
    avg_white = avg_white ./ max(avg_white(:));
    avg_white = avg_white .^ (GAMMA);
    
    mean = sum(avg_white(:))./numel(avg_white(:));
    
    vig = zeros(size(avg_white));
    vig = mean./avg_white;
    
    avg_white = imadjust(avg_white);
    
    save('./calib_folder/vig.mat','vig');
    imwrite(avg_white,'./calib_folder/averaged_white.tif');
end