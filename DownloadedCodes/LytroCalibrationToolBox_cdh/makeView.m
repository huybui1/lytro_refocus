function makeView(raw,centerY,centerX,height,width,body)
    
    save_folder = ['./output/' body];
    mkdir(save_folder);
    raw = double(raw);
    
    fprintf('Interpolating using multi view...\n');
    hex = zeros(8*height,4*width,3);
    cnt = zeros(8*height,4*width,3);
    
    centerX1 = centerX(1:end/2);
    centerY1 = centerY(1:end/2);
    centerX2 = centerX(end/2+1:end);
    centerY2 = centerY(end/2+1:end);
    
    minX = min(centerX);
    maxX = max(centerX);
    minY = min(centerY);
    maxY = max(centerY);
    
    s = -2;
    % extract center view using multiple views
    for ch = 1:3                
        InterpSlice = interpn(squeeze(raw(:,:,ch)), centerY1,centerX1, 'cubic');
        hex(1:8:end,1:4:end,ch) =  reshape(InterpSlice, [height width]);
        cnt(1:8:end,1:4:end,ch) = cnt(1:8:end,1:4:end,ch)+1;
        InterpSlice = interpn(squeeze(raw(:,:,ch)), centerY2,centerX2, 'cubic');
        hex(5:8:end,3:4:end,ch) =  reshape(InterpSlice, [height width]);
        cnt(5:8:end,3:4:end,ch)= cnt(5:8:end,3:4:end,ch)+1;
        % x+1 direction
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY1,centerX1+0.5*s, 'cubic');
        hex(1:8:end,3:4:end,ch) =  hex(1:8:end,3:4:end,ch) +reshape(InterpSlice, [height width]);
        cnt(1:8:end,3:4:end,ch) = cnt(1:8:end,3:4:end,ch)+1;
        
        index = abs(centerX2-maxX) > 3;
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY2(index),centerX2(index)+0.5*s, 'cubic');
        hex(5:8:end,5:4:end,ch) =  hex(5:8:end,5:4:end,ch) +reshape(InterpSlice, [height width-1]);
        cnt(5:8:end,5:4:end,ch) = cnt(5:8:end,5:4:end,ch)+1;
        
        % x-1 direction
        index = abs(centerX1-minX) > 3;
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY1(index),centerX1(index)-0.5*s, 'cubic');
        hex(1:8:end,3:4:end-4,ch) =  hex(1:8:end,3:4:end-4,ch) +reshape(InterpSlice, [height width-1]);
        cnt(1:8:end,3:4:end-4,ch) = cnt(1:8:end,3:4:end-4,ch)+1;
        
        
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY2,centerX2-0.5*s, 'cubic');
        hex(5:8:end,1:4:end-3,ch) =  hex(5:8:end,1:4:end-3,ch) +reshape(InterpSlice, [height width]);
        cnt(5:8:end,1:4:end-3,ch) = cnt(5:8:end,1:4:end-3,ch)+1;
        
         % 60 direction
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY1+0.25*sqrt(3)*s,centerX1+0.25*s, 'cubic');
        hex(3:8:end,2:4:end,ch) =  hex(3:8:end,2:4:end,ch) +reshape(InterpSlice, [height width]);
        cnt(3:8:end,2:4:end,ch) = cnt(3:8:end,2:4:end,ch)+1;
        
        
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY2+0.25*sqrt(3)*s,centerX2+0.25*s, 'cubic');
        hex(7:8:end,4:4:end,ch) =  hex(7:8:end,1:4:end,ch) +reshape(InterpSlice, [height width]);
        cnt(7:8:end,4:4:end,ch) = cnt(7:8:end,1:4:end,ch)+1;
        
         % 120 direction
        index = abs(centerX1-minX) > 3;
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY1(index)+0.25*sqrt(3)*s,centerX1(index)-0.25*s, 'cubic');
        hex(3:8:end,4:4:end-4,ch) =  hex(3:8:end,4:4:end-4,ch) +reshape(InterpSlice, [height width-1]);
        cnt(3:8:end,4:4:end-4,ch) = cnt(3:8:end,4:4:end-4,ch)+1;
        
        
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY2+0.25*sqrt(3)*s,centerX2-0.25*s, 'cubic');
        hex(7:8:end,2:4:end,ch) =  hex(7:8:end,2:4:end,ch) +reshape(InterpSlice, [height width]);
        cnt(7:8:end,2:4:end,ch) = cnt(7:8:end,2:4:end,ch)+1;
        
         % 180 direction
        index = abs(centerX1-minX) > 3 & abs(centerY1-minY) > 3;
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY1(index)-0.25*sqrt(3)*s,centerX1(index)-0.25*s, 'cubic');
        hex(7:8:end-8,4:4:end-4,ch) =  hex(7:8:end-8,4:4:end-4,ch) +reshape(InterpSlice, [height-1 width-1]);
        cnt(7:8:end-8,4:4:end-4,ch) = cnt(7:8:end-8,4:4:end-4,ch)+1;
        
        
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY2-0.25*sqrt(3)*s,centerX2-0.25*s, 'cubic');
        hex(3:8:end,2:4:end,ch) =  hex(3:8:end,2:4:end,ch) +reshape(InterpSlice, [height width]);
        cnt(3:8:end,2:4:end,ch) = cnt(3:8:end,2:4:end,ch)+1;
        
         % 240 direction
        index = abs(centerY1-minY) > 3;
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY1(index)-0.25*sqrt(3)*s,centerX1(index)+0.25*s, 'cubic');
        hex(7:8:end-8,2:4:end,ch) =  hex(7:8:end-8,2:4:end,ch) +reshape(InterpSlice, [height-1 width]);
        cnt(7:8:end-8,2:4:end,ch) = cnt(7:8:end-8,2:4:end,ch)+1;
        
        
        InterpSlice = interpn(squeeze(raw(:,:,ch)),centerY2-0.25*sqrt(3)*s,centerX2+0.25*s, 'cubic');
        hex(3:8:end,4:4:end,ch) =  hex(3:8:end,4:4:end,ch) +reshape(InterpSlice, [height width]);
        cnt(3:8:end,4:4:end,ch) = cnt(3:8:end,4:4:end,ch)+1;
        
        fprintf('.');    
    end
    
    index = cnt~=0;
    hex(index) = hex(index)./cnt(index);
    
    for ch = 1:3
        img(:,:,ch)= imadjust(baryCentricMul(hex(:,:,ch),height,width)/65535)*65535;
    end
    
    imwrite(uint16(img),sprintf('%s/multiple.tif',save_folder));
    
    % extract other views.. 
    for col=-3:3 %it is possible to extract more views changing 3 into 4 or 5
        for row=-3:3
            fprintf('Interpolating row : %d, col : %d...\n',row,col);
            hex = zeros(4*height,2*width,3);
            img = zeros(floor(height*3*sqrt(3)),width*3);
            for ch = 1:3                
                InterpSlice = interpn(squeeze(raw(:,:,ch)), centerY(1:end/2)+row,centerX(1:end/2)+col, 'cubic');
                hex(1:4:end,1:2:end,ch) =  reshape(InterpSlice, [height width]);
                InterpSlice = interpn(squeeze(raw(:,:,ch)), centerY(end/2+1:end)+row,centerX(end/2+1:end)+col, 'cubic');
                hex(3:4:end,2:2:end,ch) =  reshape(InterpSlice, [height width]);
                fprintf('.');    
                img(:,:,ch)= imadjust(baryCentric(hex(:,:,ch),height,width)/65535)*65535;
            end
           imwrite(uint16(hex),sprintf('%s/hex_arr_%d_%d.tif',save_folder,row,col));
           imwrite(uint16(img),sprintf('%s/sub_img_%d_%d.tif',save_folder,row,col));
        end
    end
    
end