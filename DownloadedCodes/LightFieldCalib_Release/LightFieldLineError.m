function cost=LightFieldLineError(Param,world_h,center_h,line_h,num_h,world_v,center_v,line_v,num_v,pattern_size)

ImageNum=length(num_h);
ParamNum=8;

if length(num_v)~=ImageNum || length(Param)<ParamNum+6*ImageNum || length(pattern_size)<ImageNum
    error('LightFieldLineError : Different number of frames');
end
if sum(num_h)>length(world_h) || sum(num_h)>length(center_h) || sum(num_h)>length(line_h)
    error('LightFieldLineError : Unexpected number of features (H)');
end
if sum(num_v)>length(world_v) || sum(num_v)>length(center_v) || sum(num_v)>length(line_v)
    error('LightFieldLineError : Unexpected number of features (V)');
end

K1=Param(1);
K2=Param(2);
fx=Param(3);
fy=Param(4);
cx=Param(5);
cy=Param(6);
rd1=Param(7);
rd2=Param(8);

idxh=0;
idxv=0;
idxc=0;

cost=zeros(1,sum(num_h)+sum(num_v));

for n=1:ImageNum
    nh=num_h(n);
    nv=num_v(n);
    wh=world_h(:,idxh+1:idxh+nh);
    ch=center_h(:,idxh+1:idxh+nh);
    lh=line_h(:,idxh+1:idxh+nh);
    wv=world_v(:,idxv+1:idxv+nv);
    cv=center_v(:,idxv+1:idxv+nv);
    lv=line_v(:,idxv+1:idxv+nv);
    RT=SetAxis(Param(n*6+ParamNum-5:n*6+ParamNum));
    RT=RT(1:3,:);
    ps=pattern_size(n);
    
    Q1=RT*[wh(1,:)*ps;wh(2,:)*ps;zeros(1,nh);ones(1,nh)];
    Q2=RT*[wh(1,:)*ps-ps;wh(2,:)*ps;zeros(1,nh);ones(1,nh)];
    
    du=-lh(1,:).*lh(3,:);
    dv=-lh(2,:).*lh(3,:);
    Xs=K2*du/fx;
    Ys=K2*dv/fy;
    x=(-K1*K2*du+ch(1,:)-cx)/fx;
    y=(-K1*K2*dv+ch(2,:)-cy)/fy;
    r=x.*x+y.*y;
    x_=(1+rd1*r+rd2*r.^2).*x;
    y_=(1+rd1*r+rd2*r.^2).*y;
    cost(idxc+1:idxc+nh)=VectLineDist([Xs;Ys;zeros(1,nh)],[x_;y_;ones(1,nh)],Q1,Q2);
    idxc=idxc+nh;
    
    Q1=RT*[wv(1,:)*ps;wv(2,:)*ps;zeros(1,nv);ones(1,nv)];
    Q2=RT*[wv(1,:)*ps;wv(2,:)*ps-ps;zeros(1,nv);ones(1,nv)];

    du=-lv(1,:).*lv(3,:);
    dv=-lv(2,:).*lv(3,:);
    Xs=K2*du/fx;
    Ys=K2*dv/fy;
    x=(-K1*K2*du+cv(1,:)-cx)/fx;
    y=(-K1*K2*dv+cv(2,:)-cy)/fy;
    r=x.*x+y.*y;
    x_=(1+rd1*r+rd2*r.^2).*x;
    y_=(1+rd1*r+rd2*r.^2).*y;
    cost(idxc+1:idxc+nv)=VectLineDist([Xs;Ys;zeros(1,nv)],[x_;y_;ones(1,nv)],Q1,Q2);
    idxc=idxc+nv;

    idxh=idxh+nh;
    idxv=idxv+nv;
end
