function dist=VectLineDist(p,d,q1,q2)
q21=q2-q1;
a1=sum(d.^2,1);
b1=sum(q21.*d,1);
c1=sum((p-q1).*d);
a2=sum(d.*q21,1);
b2=sum(q21.^2,1);
c2=sum((p-q1).*q21,1);
l1=-(c1.*b2-c2.*b1)./(a1.*b2-a2.*b1);
l2=-(c1.*a2-c2.*a1)./(a1.*b2-a2.*b1);
dist=sqrt(sum(((p+([1;1;1]*l1).*d)-(q1+([1;1;1]*l2).*q21)).^2,1));

% figure;
% p1=p;
% p2=p+([1;1;1]*l1).*d;
% q=q1+([1;1;1]*l2).*q21;
% temp1=[p1(:,1),p2(:,1)];
% temp2=[q1(:,1),q(:,1),q2(:,1)];
% plot3(temp1(1,:),temp1(2,:),temp1(3,:),'b.-');
% hold on;
% plot3(temp2(1,:),temp2(2,:),temp2(3,:),'r.-');
% hold off; axis equal;
