[file1,path1]=uigetfile('*.png;*.jpg','Image','MultiSelect','on');
if length(path1)<2
    return;
end

if length(char(file1(1)))==1
    temp=file1;
    file1=cell(1,1);
    file1(1,1)=java.lang.String(temp);
end

load([path1,'microlens_center_list.mat']);

ImageNum=length(file1);
for n=1:ImageNum
    filename=char(file1(n));
    temp=imread([path1,filename]);
    source_type=class(temp);
    img_raw=double(temp);
    
    fprintf('Processing : %s',filename);
    
    img_center=CenterImage(img_raw,center_list,center_connection,radius);

    len=length(filename);
    filename=filename(1:length(filename)-3);
    switch source_type
        case 'uint8'
            img_center=uint8(round(img_center));
        case 'uint16'
            img_center=uint8(round(img_center/256));
    end
    imwrite(img_center,[path1,'CI_',filename,'bmp']);

    fprintf(' CenterImage');
    
%     corner=CheckerboardCorner(double(rgb2gray(img_center)));
%     save([path1,'CI_',filename,'mat'],'corner');
    
    fprintf(' CornerList\n');
end
