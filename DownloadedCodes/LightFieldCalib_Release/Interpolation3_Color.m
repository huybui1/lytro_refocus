function val=Interpolation3_Color(target,source)

line1=cross([source(1,1),source(2,1),1],[target(1,1),target(2,1),1]);
line2=cross([source(1,2),source(2,2),1],[source(1,3),source(2,3),1]);
temp=cross(line1,line2);
x=temp(1)/temp(3);
y=temp(2)/temp(3);
l2=norm(source(1:2,2)-[x;y]);
l3=norm(source(1:2,3)-[x;y]);
l1=norm(source(1:2,1)-target(1:2,1));
l0=norm([x;y]-target(1:2,1));
w1=l0/(l0+l1);
w2=l3/(l2+l3)*l1/(l0+l1);
w3=l2/(l2+l3)*l1/(l0+l1);
k=length(source(:,1));
val=source(3:k,1)*w1+source(3:k,2)*w2+source(3:k,3)*w3;
