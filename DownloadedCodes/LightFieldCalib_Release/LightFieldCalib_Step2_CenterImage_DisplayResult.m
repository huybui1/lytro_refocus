[file1,path1]=uigetfile('*.bmp','Image','MultiSelect','on');
if length(path1)<2
    return;
end
% close all;
if length(char(file1(1)))==1
    temp=file1;
    file1=cell(1,1);
    file1(1,1)=java.lang.String(temp);
end

ImageNum=length(file1);
for n=1:ImageNum
    filename=char(file1(n));
    img=imread([path1,filename]);
    figure; imshow(img);
    
    filename2=filename(1:length(filename)-3);
    load([path1,filename2,'mat']);
    hold on;
    idx00=find(corner(1,:)==0&corner(2,:)==0);
    idx10=find(corner(1,:)==1&corner(2,:)==0);
    idx01=find(corner(1,:)==0&corner(2,:)==1);
    idx=[idx10,idx00,idx01];
    plot(corner(3,idx),corner(4,idx),'g-','LineWidth',2);
    plot(corner(3,:),corner(4,:),'r.');
    hold off;
end
