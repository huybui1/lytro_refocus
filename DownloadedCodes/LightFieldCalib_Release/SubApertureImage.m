function [img,IntParam,ExtParam]=SubApertureImage(raw,center,connection,calib,dx,dy,scale,intparam)

K1=calib(1);
K2=calib(2);
fx=calib(3);
fy=calib(4);
cx=calib(5);
cy=calib(6);
rd1=calib(7);
rd2=calib(8);

temp=size(raw);
W=temp(2);
H=temp(1);
B=temp(3);
w=floor(W/scale);
h=floor(H/scale);
img=zeros(h,w,B);

if isempty(intparam)
    IntParam=[fx/scale,0,cx/scale;0,fx/scale,cy/scale;0,0,1];
else
    IntParam=intparam;
end

if fx>fy
    dx_=dx;
    dy_=dy*fy/fx;
else
    dx_=dx*fx/fy;
    dy_=dy;
end

x=(-K1*K2*dx_+center(1,:)-cx)/fx;
y=(-K1*K2*dy_+center(2,:)-cy)/fy;
Xs=K2*dx_/fx;
Ys=K2*dy_/fy;
r=x.^2+y.^2;
x_=(1+rd1*r+rd2*r.^2).*x;
y_=(1+rd1*r+rd2*r.^2).*y;

sample_x=x_*IntParam(1,1)+IntParam(1,3);
sample_y=y_*IntParam(2,2)+IntParam(2,3);
sample_i=Interpolation4_Color(center(1:2,:)+[dx;dy]*ones(1,length(center(1,:))),raw);
sample=[sample_x;sample_y;sample_i];

[~,idx_row_start]=min((1-sample_y).^2+(1-sample_x).^2);

for j=1:h
    for i=1:w
        if i==1
            idx_prev=idx_row_start;
        else
            idx_prev=idx;
        end
        while true
            candidate=connection(:,idx_prev)';
            [~,idx]=min((j-sample_y(candidate)).^2+(i-sample_x(candidate)).^2);
            idx=candidate(idx);
            if idx==idx_prev
                break;
            else
                idx_prev=idx;
            end
        end
        if i==1
            idx_row_start=idx;
        end
        t=[i;j];
        s=sample(1:2,idx);
        idx7=connection(:,idx)';
        s7=sample(1:2,idx7);
        for k1=1:length(idx7)-1
            s1=s7(:,k1);
            for k2=k1+1:length(idx7)
                s2=s7(:,k2);
                if (s1-s)'*(s2-s)>0
                    v1=s1-s;
                    v2=t-s;
                    v3=s2-s1;
                    v4=t-s1;
                    v5=s-s2;
                    v6=t-s2;
                    c1=v1(1)*v2(2)-v1(2)*v2(1);
                    c2=v3(1)*v4(2)-v3(2)*v4(1);
                    c3=v5(1)*v6(2)-v5(2)*v6(1);
                    if (c1>=0 && c2>=0 && c3>=0) || (c1<=0 && c2<=0 && c3<=0)
                        img(j,i,:)=Interpolation3_Color(t,sample(:,[idx,idx7(k1),idx7(k2)]));
                    end
                end
            end
        end
    end
end

ExtParam=diag(ones(1,4));
ExtParam(1:2,4)=-[Xs;Ys];
