function GenerateLinearTemplate(filename,radius)

TemplateAngle=(0.25:0.5:179.75)*pi/180;
TemplateSlope=[sin(TemplateAngle);cos(TemplateAngle)]';
TemplateDist=-radius+1.025:0.05:radius-1.025;
Template=cell(length(TemplateAngle),length(TemplateDist));
for j=1:length(TemplateAngle)
    for i=1:length(TemplateDist)
        Template(j,i)={LinearTemplate(radius-1,[TemplateSlope(j,:),TemplateDist(i)])};
    end
end
[i,j]=meshgrid(-radius+1:radius-1,-radius+1:radius-1);
TemplateWeight=exp((-i.*i-j.*j)/((radius-1)^2*3));

save(filename,'TemplateAngle','TemplateSlope','TemplateDist','Template','TemplateWeight');
