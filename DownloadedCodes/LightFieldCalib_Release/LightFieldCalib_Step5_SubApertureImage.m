clear all; close all;

DetectCorner=true;

[file1,path1]=uigetfile('*.png','Raw Image','MultiSelect','on');
if length(path1)<2
    return;
end

if length(char(file1(1)))==1
    temp=file1;
    file1=cell(1,1);
    file1(1,1)=java.lang.String(temp);
end

load([path1,'microlens_center_list.mat']);

load([path1,'IntParamLF.mat']);
location='SubAperture\';
extfile='ExtParamLF_';

if exist([path1,location],'dir')~=7
    mkdir(path1,location);
end

scale=radius*2;
[i,j]=meshgrid(-3:3,-3:3);
extraction=[i(:),j(:)];

pattern_size=load([path1,'pattern_size.txt']);

for n=1:length(file1)
    file0=char(file1(n));
    temp=imread([path1,file0]);
    source_type=class(temp);
    raw=double(temp);
    file2=file0(1:length(file0)-4);
    file_corner=['CI_',file2,'.mat'];
    extfile2=[extfile,file2,'.mat'];

    for k=1:length(extraction(:,1))
        i=extraction(k,1);
        j=extraction(k,2);
        [img,SubIntParam,SubExtParam]=SubApertureImage(raw,center_list,center_connection,IntParamLF,i,j,scale,[]);
        
        file3=sprintf('_Sub_%d_%d',j+radius,i+radius);
        switch source_type
            case 'uint8'
                imwrite(uint8(round(img)),[path1,location,file2,file3,'.png']);
            case 'uint16'
                imwrite(uint16(round(img)),[path1,location,file2,file3,'.png']);
        end
        
        if DetectCorner && exist([path1,file_corner],'file')>0 && exist([path1,extfile2],'file')>0
            load([path1,file_corner]);
            corner_num=length(corner(1,:));
            load([path1,extfile,file2,'.mat']);
            
            temp=size(img);
            sizeV=temp(1);
            sizeH=temp(2);
            corner_initial=SubIntParam*SubExtParam(1:3,:)*ExtParamLF*[corner(1:2,:)*pattern_size;zeros(1,length(corner(1,:)));ones(1,length(corner(1,:)))];
            corner_initial=[corner_initial(1,:)./corner_initial(3,:);corner_initial(2,:)./corner_initial(3,:)];
            bi=corner_initial(1,:)>8&corner_initial(1,:)<sizeH-7&corner_initial(2,:)>8&corner_initial(2,:)<sizeV-7;
            corner_winsize=zeros(1,corner_num);
            
            corner_refined3=[corner(1:2,:);fast_cornerfinder(corner_initial,img,3,3)];
            corner_refined3(5:6,:)=corner_initial;
            br3=corner_refined3(3,:)>8&corner_refined3(3,:)<sizeH-7&corner_refined3(4,:)>8&corner_refined3(4,:)<sizeV-7;
            corner_refined7=[corner(1:2,:);fast_cornerfinder(corner_initial,img,7,7)];
            corner_refined7(5:6,:)=corner_initial;
            br7=corner_refined7(3,:)>8&corner_refined7(3,:)<sizeH-7&corner_refined7(4,:)>8&corner_refined7(4,:)<sizeV-7;
            for i=1:corner_num
                if bi(i)
                    if min((corner_initial(1,[1:i-1,i+1:corner_num])-corner_initial(1,i)).^2+(corner_initial(2,[1:i-1,i+1:corner_num])-corner_initial(2,i)).^2)<50
                        if br3(i)
                            x=round(corner_refined3(3,i));
                            y=round(corner_refined3(4,i));
                            temp3=img(y-7:y+7,x-7:x+7);
                            if sum(temp3(:)==0)==0
                                corner_winsize(i)=3;
                            end
                        end
                    else
                        if br7(i)
                            x=round(corner_refined7(3,i));
                            y=round(corner_refined7(4,i));
                            temp7=img(y-7:y+7,x-7:x+7);
                            if sum(temp7(:)==0)==0
                                corner_winsize(i)=7;
                            end
                        end
                    end
                end
            end
            SubCorner=[corner_refined3(:,corner_winsize==3),corner_refined7(:,corner_winsize==7)];
            save([path1,location,file2,file3,'.mat'],'SubIntParam','SubExtParam','SubCorner');
        else
            save([path1,location,file2,file3,'.mat'],'SubIntParam','SubExtParam');
        end
    end
end
