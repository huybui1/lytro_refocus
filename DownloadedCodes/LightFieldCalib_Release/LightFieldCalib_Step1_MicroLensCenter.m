clear all; close all;

radius=5;

[file1,path1]=uigetfile('*.png;*.jpg','White Images','MultiSelect','on');
if length(path1)<2
    return;
end

if length(char(file1(1)))==1
    temp=file1;
    file1=cell(1,1);
    file1(1,1)=java.lang.String(temp);
end

ImageNum=length(file1);
for n=1:ImageNum
    temp=imread([path1,char(file1(n))]);
    if n==1
        accumulation=double(temp);
    else
        accumulation=accumulation+double(temp);
    end
end

vig=accumulation/ImageNum;

temp=size(vig);
image_size=[temp(2),temp(1)];

if exist([path1,'boundary.txt'])
    boundary=load([path1,'boundary.txt']);
else
    boundary=[1;length(vig(1,:,:));1;length(vig(:,1,:))];
end
vigb=double(rgb2gray(uint8(vig(boundary(3):boundary(4),boundary(1):boundary(2),:))));

if exist([path1,'remove.txt'])
    remove=load([path1,'remove.txt']);
    remove_num=floor(length(remove)/4);
    for n=1:remove_num
        remove(n*4-3:n*4-2)=remove(n*4-3:n*4-2)-boundary(1)+1;
        remove(n*4-1:n*4)=remove(n*4-1:n*4)-boundary(3)+1;
    end
else
    remove=[];
    remove_num=0;
end

[vig_height,vig_width]=size(vigb);
iteration_max=10;

center_num=0;
center_horizontal1=-ones(2,vig_width*2);
idxL=vig_width;
idxR=vig_width;

center=MicroLensCenter(vigb,[vig_width/2;vig_height/2],radius,iteration_max);
center_init=center;
center_horizontal1(:,vig_width)=center;
while true
    center=MicroLensCenter(vigb,center-[radius*2;0],radius,iteration_max);
    if center(1)<0
        break;
    end
    idxL=idxL-1;
    center_horizontal1(:,idxL)=center;
end
center=center_init;
while true
    center=MicroLensCenter(vigb,center+[radius*2;0],radius,iteration_max);
    if center(1)<0
        break;
    end
    idxR=idxR+1;
    center_horizontal1(:,idxR)=center;
end
center_horizontal1=center_horizontal1(:,idxL-10:idxR+10);
len1=length(center_horizontal1(1,:));

idxS=len1^2;
idxE=idxS+idxR-idxL;
center_list=zeros(2,2*idxS);

center_list(:,idxS:idxE)=center_horizontal1(:,11:len1-10);

center_horizontal1(:,1:10)=center_horizontal1(:,11:20)+(center_horizontal1(:,11)-center_horizontal1(:,21))*ones(1,10);
center_horizontal1(:,len1-9:len1)=center_horizontal1(:,len1-19:len1-10)+(center_horizontal1(:,len1-10)-center_horizontal1(:,len1-20))*ones(1,10);
idx_init1=vig_width-idxL+11;

center_init2=MicroLensCenter(vigb,center_init+[-radius;radius*1.73],radius,iteration_max);
diff_ld=center_init2-center_init;

center_horizontal2=MicroLensCenter(vigb,center_horizontal1+diff_ld*ones(1,len1),radius,iteration_max);

idx=find(center_horizontal2(1,:)>=0&center_horizontal2(2,:)>=0);
idxL=idx(1);
idxR=idx(length(idx));

center_horizontal2=center_horizontal2(:,idxL:idxR);
len2=length(center_horizontal2(1,:));
center_list(:,idxE+1:idxE+len2)=center_horizontal2;
idxE=idxE+len2;

temp1=center_horizontal2(:,1:10)+(center_horizontal2(:,1)-center_horizontal2(:,11))*ones(1,10);
temp2=center_horizontal2(:,len2-9:len2)+(center_horizontal2(:,len2)-center_horizontal2(:,len2-10))*ones(1,10);
center_horizontal2=[temp1,center_horizontal2,temp2];
idx_init2=idx_init1-idxL+11;
len2=len2+20;

center_init3=MicroLensCenter(vigb,center_init2+[radius;radius*1.73],radius,iteration_max);
diff_d2=center_init3-center_init;

% figure; imshow(uint8(vig)); hold on;

idx1=0;
idx2=0;
c1=center_horizontal1;
c2=center_horizontal2;
diff=-diff_d2;
while ~isempty([idx1,idx2])
    center_temp=MicroLensCenter(vigb,[c1,c2]+diff*ones(1,len1+len2),radius,iteration_max);
    c1r=center_temp(:,1:len1);
    c2r=center_temp(:,len1+1:len1+len2);
    for n=1:remove_num
        c1r(:,c1(1,:)>=remove(n*4-3)-diff(1)&c1(1,:)<=remove(n*4-2)-diff(1)&c1(2,:)>=remove(n*4-1)-diff(2)&c1(2,:)<=remove(n*4)-diff(2))=-1;
        c2r(:,c2(1,:)>=remove(n*4-3)-diff(1)&c2(1,:)<=remove(n*4-2)-diff(1)&c2(2,:)>=remove(n*4-1)-diff(2)&c2(2,:)<=remove(n*4)-diff(2))=-1;
    end
    
    idx2=find(c2r(1,:)>=0&c2r(2,:)>=0);
    num=length(idx2);
    center_list(:,idxS-num:idxS-1)=c2r(:,idx2);
    idxS=idxS-num;
    
    idx1=find(c1r(1,:)>=0&c1r(2,:)>=0);
    num=length(idx1);
    center_list(:,idxS-num:idxS-1)=c1r(:,idx1);
    idxS=idxS-num;

    if c1r(1,idx_init1)<0
        diff1=-diff_d2;
    else
        diff1=c1r(:,idx_init1)-c1(:,idx_init1);
    end
    if c2r(1,idx_init2)<0
        diff2=-diff_d2;
    else
        diff2=c2r(:,idx_init2)-c2(:,idx_init2);
    end
    diff=(diff1+diff2)*0.5;
    c1=c1+diff*ones(1,len1);
    c1(:,idx1)=c1r(:,idx1);
    c2=c2+diff*ones(1,len2);
    c2(:,idx2)=c2r(:,idx2);
end

% hold off;

idx1=0;
idx2=0;
c1=center_horizontal1;
c2=center_horizontal2;
diff=diff_d2;
while ~isempty([idx1,idx2])
    center_temp=MicroLensCenter(vigb,[c1,c2]+diff*ones(1,len1+len2),radius,iteration_max);
    c1r=center_temp(:,1:len1);
    c2r=center_temp(:,len1+1:len1+len2);
    for n=1:remove_num
        c1r(:,c1(1,:)>=remove(n*4-3)-diff(1)&c1(1,:)<=remove(n*4-2)-diff(1)&c1(2,:)>=remove(n*4-1)-diff(2)&c1(2,:)<=remove(n*4)-diff(2))=-1;
        c2r(:,c2(1,:)>=remove(n*4-3)-diff(1)&c2(1,:)<=remove(n*4-2)-diff(1)&c2(2,:)>=remove(n*4-1)-diff(2)&c2(2,:)<=remove(n*4)-diff(2))=-1;
    end

    idx1=find(c1r(1,:)>=0&c1r(2,:)>=0);
    num=length(idx1);
    center_list(:,idxE+1:idxE+num)=c1r(:,idx1);
    idxE=idxE+num;
    
    idx2=find(c2r(1,:)>=0&c2r(2,:)>=0);
    num=length(idx2);
    center_list(:,idxE+1:idxE+num)=c2r(:,idx2);
    idxE=idxE+num;

%     plot(c1(1,:)+diff(1),c1(2,:)+diff(2),'b.');
%     plot(c1r(1,:),c1r(2,:),'r.');
%     
%     plot(c2(1,:)+diff(1),c2(2,:)+diff(2),'b.');
%     plot(c2r(1,:),c2r(2,:),'r.');
    
    if c1r(1,idx_init1)<0
        diff1=diff_d2;
    else
        diff1=c1r(:,idx_init1)-c1(:,idx_init1);
    end
    if c2r(1,idx_init2)<0
        diff2=diff_d2;
    else
        diff2=c2r(:,idx_init2)-c2(:,idx_init2);
    end
    diff=(diff1+diff2)*0.5;
    c1=c1+diff*ones(1,len1);
    c1(:,idx1)=c1r(:,idx1);
    c2=c2+diff*ones(1,len2);
    c2(:,idx2)=c2r(:,idx2);
end

center_list=center_list(:,idxS:idxE)+[boundary(1)-1;boundary(3)-1]*ones(1,idxE-idxS+1);

center_connection=zeros(7,length(center_list(1,:)));
for n=1:length(center_list(1,:))
    idx7=find((center_list(1,:)-center_list(1,n)).^2+(center_list(2,:)-center_list(2,n)).^2<radius*radius*5);
    center_connection(:,n)=[idx7';n*ones(7-length(idx7),1)];
end

save([path1,'microlens_center_list.mat'],'center_list','center_connection','radius','image_size');
save([path1,'vig_mean.mat'],'vig');
