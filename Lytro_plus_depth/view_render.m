% generate novel view using light field + depth
clear all
clc
close all


CALIBRATION_DIR = './Calibration/Dec23/';
DEPTH_DIR = '../Data/Depth/Dec23_Depth_Data/';
LF_DIR = '../Data/Images/Dec23_LF_Data/';
RESULTS_DIR = './Results/Apr20/';
JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 14;

% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

% denoising depth with BF
filter_width = 15;
sigma_s = 6;
sigma_r = 50;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));
% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc

% fill in invalid depth value
projected_depth = hole_filling(projected_depth, 5);

figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;

subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

%
% [xx, yy] = meshgrid(1:328, 1:328);
% figure;
% mesh(xx, yy, -projected_depth);
% keyboard;


% map depth to distance

disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);

% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);
disp('Getting depth and distance corresponding to each microlens center ...');
tic
[center_depth, center_distance] = get_distance_and_depth_at_centers(center_list, 5, projected_depth, distance_map);
toc

LFP = LFReadLFP([LF_DIR,RAW_LF_FILE]);
PIXEL_PITCH = LFP.Metadata.devices.sensor.pixelPitch*10e3; % in millimeters

%% sampled points on the surface
points = zeros(3, size(center_list, 2));
points(1,:) = center_list(1,:)*PIXEL_PITCH - PIXEL_PITCH/2;
points(2,:) = center_list(2,:)*PIXEL_PITCH - PIXEL_PITCH/2;
points(3,:) = center_distance;

% specify virtual camera
virtual_height = 328;
virtual_width = 328;
virtual_cx = virtual_width/2;
virtual_cy = virtual_height/2;
virtual_fx = 10e-3 / PIXEL_PITCH;
virtual_fy = 10e-3 / PIXEL_PITCH;
virtual_K = [virtual_fx 0.0         virtual_cx;
             0.0        virtual_fy  virtual_cy;
             0.0        0.0         1.0];
inv_virtual_K = inv(virtual_K);

% Extrinsic
R = [1.0  0.0       0.0;
     0.0  cosd(0)  -sind(0);
     0.0  sind(0)  cosd(0)];
invR = inv(R);
t = [-3280*PIXEL_PITCH/2, -3280*PIXEL_PITCH/2, 125e-3]';
% Projection matrix for points
virtual_P = virtual_K*[R, t];

if (~exist('temp.mat', 'file'))
%% mapping depth to virtual camera view
% (create a depth map registered to the virtual image plane)
gen_depth_map = zeros(virtual_height, virtual_width);
gen_mla_map = zeros(virtual_height, virtual_width);
gen_weight = zeros(virtual_height, virtual_width);
% splatting
% kernel for splatting
rad = 6;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
% kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
% sigma = 5;
% sigma2 = sigma*sigma;
% kernel = exp(-(dxx.^2 + dyy.^2)./(2*sigma2));
kernel(kernel < 0) = 0.0;
tic

transformed_pts  = [R, t]*[points; ones(1, size(points, 2))];
    
for i = 1:size(points, 2)
    transformed_pt = transformed_pts(:,i);
    if (transformed_pt(3) ~= 0.0)
      proj_x = (transformed_pt(1)*virtual_fx + virtual_cx*transformed_pt(3))/transformed_pt(3);
      proj_y = (transformed_pt(2)*virtual_fy + virtual_cy*transformed_pt(3))/transformed_pt(3);

      proj_x  = round(proj_x);
      proj_y  = round(proj_y);
      for dx = -rad:rad
         for dy = -rad:rad
            px = proj_x + dx;
            py = proj_y + dy;
            w = kernel(dx + rad + 1, dy + rad + 1);
            
            if (px >= 1 && px <= virtual_width && ...
                py >= 1 && py <= virtual_height)
                gen_weight(py, px) = gen_weight(py, px) + w;
                gen_depth_map(py, px) = gen_depth_map(py, px) + w*transformed_pt(3);            
            end              
         end
      end
    end
end

toc
% figure; 
% imagesc(gen_weight)
gen_depth_map(gen_weight > 0) = gen_depth_map(gen_weight > 0)./gen_weight(gen_weight > 0);
figure;
imagesc(gen_depth_map);
%% map the mla plane to virtual camera
gen_mla_map = zeros(virtual_height, virtual_width);
gen_weight = zeros(virtual_height, virtual_width);

mla_pts = zeros(3, size(center_list, 2));
mla_pts(1,:) = points(1,:);
mla_pts(2,:) = points(2,:);

transformed_mla_pts = [R t] * [mla_pts; ones(1, size(mla_pts,2))];

for i = 1:size(mla_pts,2)
    transformed_mla_pt = transformed_mla_pts(:,i);
    if (transformed_mla_pt(3) ~= 0.0)
      proj_mla_x = (transformed_mla_pt(1)*virtual_fx + virtual_cx*transformed_mla_pt(3))/transformed_mla_pt(3);
      proj_mla_y = (transformed_mla_pt(2)*virtual_fy + virtual_cy*transformed_mla_pt(3))/transformed_mla_pt(3);

      proj_mla_x  = round(proj_mla_x);
      proj_mla_y  = round(proj_mla_y);
      for dx = -rad:rad
         for dy = -rad:rad
            px = proj_mla_x + dx;
            py = proj_mla_y + dy;
            w = kernel(dx + rad + 1, dy + rad + 1);
            
            if (px >= 1 && px <= virtual_width && ...
                py >= 1 && py <= virtual_height)
                gen_weight(py, px) = gen_weight(py, px) + w;
                gen_mla_map(py, px) = gen_mla_map(py, px) + w*transformed_mla_pt(3);            
            end              
         end
      end
    end    
end

gen_mla_map(gen_weight > 0) = gen_mla_map(gen_weight > 0)./gen_weight(gen_weight > 0);
figure;
imagesc(gen_mla_map);

save('temp.mat', 'gen_depth_map', 'gen_mla_map');
else
    load('temp.mat');
end
%% generate new image
% load white image
WHITE_DIR = './Calibration/White_Images/zoom_490/';
white_img = imread([WHITE_DIR, 'white_image.tif']);
white_img = double(white_img);

% normalize
white_img = white_img./max(white_img(:));



disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

gen_image = zeros(virtual_height, virtual_width, 3);
mla_to_sensor_dist = 25e-3; % mm
for i = 1:virtual_height
    for j = 1:virtual_width
        this_depth = gen_depth_map(j ,i);
        mla_depth = gen_mla_map(j, i);
        if (this_depth ~= 0 && mla_depth ~= 0)
            % intersection with mla plane
            sx = (j - virtual_cx)*mla_depth/virtual_fx;
            sy = (i - virtual_cy)*mla_depth/virtual_fy;
            sz = mla_depth;
            s = [sx; sy; sz];
            ss = invR*s - invR*t;
            ss = (ss(1:2) + PIXEL_PITCH/2)/PIXEL_PITCH;
            
            
            % intersection with surface
            vx = (j - virtual_cx)*this_depth/virtual_fx;
            vy = (i - virtual_cy)*this_depth/virtual_fy;
            vz = this_depth;
            v = [vx; vy; vz];
            
            % transformed to locations with respect to the mla
            vs = invR*v - invR*t;
            vs(1) = (vs(1) + PIXEL_PITCH/2)/PIXEL_PITCH;
            vs(2) = (vs(2) + PIXEL_PITCH/2)/PIXEL_PITCH;
            
            % find K closest centers to transformed_s
            K = 3;
            closest_centers = find_K_closest_centers(center_list, ss, K);
            
            temp = zeros(3, 1);
            
            w = 0;
            for k = 1:K
               cx = closest_centers(1,k);
               cy = closest_centers(2,k);
               
               if ((abs(cx - ss(1)) >= 30)||(abs(cy - ss(2)) >= 30))
                   continue;
               end
               
               vsx = vs(1);
               vsy = vs(2);
               vsz = vs(3);
               u = vsx + (cx - vsx)*(vsz + mla_to_sensor_dist)/vsz;
               v = vsy + (cy - vsy)*(vsz + mla_to_sensor_dist)/vsz;
               
               if ( abs(u - cx) < 5 && abs(v - cy) < 5 )
                    w = w + 1;
                    temp = temp + interpolate_LF(u, v, processed_LF);
               end
            end
            if (w > 0)
                gen_image(i, j, :) = temp / w;
            end
            
            if (isnan(temp))
                keyboard;
            end
        end
    end
end

% gen_image = gen_image./max(gen_image(:));
% final_result = uint16(gen_image*double(intmax('uint16')));
% figure;
% imshow(final_result); title('Refocused result');
% axis image;
% axis off;
% 
% imwrite(final_result, [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_novel_view.png']);


