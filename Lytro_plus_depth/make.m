% compile
mex splat_layer.cpp %splat to a specified distance + binary mask to indicate which pixels in focus near that distance
mex splat_layer_v2.cpp %splat to a specified distance + smooth (Gaussian) mask to indicate which pixels in focus near that distance
mex splat_layer_v3.cpp %splat to a specified distance + smooth (Gaussian) mask to indicate which pixels in focus near that distance + weight pixel w. depth difference
mex splat_layer_v4.cpp  %splat to a specified distance + smooth (Gaussian) mask to indicate which pixels in focus near that distance + weight pixel w. white image intensity
mex splat_layer_adaptive_kernel.cpp %splat to a specified distance + 
                                    % smooth (Gaussian) mask to indicate which pixels in focus near that distance 
                                    %+ weight pixel w. white image
                                    %intensity + depth adaptive kernel

mex splat_w_depth.cpp     % splat each ray to corresponding distance (depth)
mex splat_w_depth_v2.cpp  % splat each ray to corresponding distance (depth) + weight ray using white image
mex splat_w_depth_v3.cpp  % splat each ray to corresponding distance (depth) + weight ray using white image + adaptive_kernel
mex splat_w_depth_v4.cpp  % generate output depth