// map high-res distance map to raw img
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void mapping(const float * distance_map,
             const float *center_x,
             const float *center_y,           
		         int num_centers,
             float *result,
             const float *white_img,
		         float f, /* mla - sensor distance */
             int height, int width) {
    
  float *weight = (float *)mxMalloc(height*width*sizeof(int));
  int pixel_index;
  float dist, max_rad, cx, cy, d, px, py;
  float w;
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	    pixel_index = w * height + h;
      dist = distance_map[pixel_index];
      max_rad = abs(dist)*4/f; // only consider centers inside max_rad
            
      for (int k = 0; k < num_centers; k++) {
        cx = center_x[k];
        cy = center_y[k];
        d = (cx - w)*(cx - w) + (cy - h)*(cy - h);

        if (d > max_rad*max_rad)
            continue;
        
        px = w + (cx - w)*(dist + f)/dist; // pixel
        py = h + (cy - h)*(dist + f)/dist;
        

        int px0 = floor(px);
        int py0 = floor(py);
        int px1 = px0 + 1;
        int py1 = py0 + 1;
        
        if (px0 >= 0 && px0 < width && py0 >= 0 && py0 < width &&
            (px0 - cx)*(px0 - cx) + (py0 - cy)*(py0 - cy) <= 16) {
          w = white_img[px0*height + py0];
          weight[px0*height + py0] += w;
          result[px0*height + py0] += dist*w;
        }
        
        if (px0 >= 0 && px0 < width && py1 >= 0 && py1 < width &&
            (px0 - cx)*(px0 - cx) + (py1 - cy)*(py1 - cy) <= 16) {
          w = white_img[px0*height + py1];
          weight[px0*height + py1] += w;
          result[px0*height + py1] += dist*w;
        }        
        
        if (px1 >= 0 && px1 < width && py0 >= 0 && py0 < width &&
            (px1 - cx)*(px1 - cx) + (py0 - cy)*(py0 - cy) <= 16) {
          w = white_img[px1*height + py0];
          weight[px1*height + py0] += w;
          result[px1*height + py0] += dist*w;
        }        
        
        if (px1 >= 0 && px1 < width && py1 >= 0 && py1 < width &&
            (px1 - cx)*(px1 - cx) + (py1 - cy)*(py1 - cy) <= 16) {
          w = white_img[px1*height + py1];
          weight[px1*height + py1] += w;
          result[px1*height + py1] += dist*w;
        }        
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0) {
        result[ w * height + h ] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = map_distance_to_lf_image(distance_map, center_x, center_y,
  //                                   num_centers, f)               
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get input distance map
  float *distance_map = (float *) mxGetData(prhs[0]);
  
  // get coordinates of centers
  float *center_x = (float *) mxGetData(prhs[1]);
  float *center_y = (float *) mxGetData(prhs[2]);
  
  int num_centers = *((int *)mxGetData(prhs[3]));
  
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[4]));
  
  // get white img
  float *white_img = (float *) mxGetData(prhs[5]);
  // allocate output
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  

  mapping(distance_map, center_x, center_y, num_centers, 
          result, white_img, f, height, width);
}