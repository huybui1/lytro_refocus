function grid_pts = create_grid(corners, height, width) 
% create a grid of points, given 4 corners of the region of interest
%   corners: 4 x 2 array contains [x, y] coordinates of 4 corners
x = corners(:, 1);
y = corners(:, 2);

% sort corners
x_mean = mean(x);
y_mean = mean(y);

x_v = x - x_mean;
y_v = y - y_mean;

theta = atan2(-y_v,x_v);
[junk,ind] = sort(theta);

[junk,ind] = sort(mod(theta-theta(1),2*pi));

% ind = ind([4 3 2 1]); 
% ind = ind([1 2 3 4]);
ind = ind([1, 4, 3, 2]);
x = x(ind);
y = y(ind);
x1= x(1); x2 = x(2); x3 = x(3); x4 = x(4);
y1= y(1); y2 = y(2); y3 = y(3); y4 = y(4);

% Find center:
p_center = cross(cross([x1;y1;1],[x3;y3;1]),cross([x2;y2;1],[x4;y4;1]));
x5 = p_center(1)/p_center(3);
y5 = p_center(2)/p_center(3);

% center on the X axis:
x6 = (x3 + x4)/2;
y6 = (y3 + y4)/2;

% center on the Y axis:
x7 = (x1 + x4)/2;
y7 = (y1 + y4)/2;

% Direction of displacement for the X axis:
vX = [x6-x5;y6-y5];
vX = vX / norm(vX);

% Direction of displacement for the X axis:
vY = [x7-x5;y7-y5];
vY = vY / norm(vY);

% Direction of diagonal:
vO = [x4 - x5; y4 - y5];
vO = vO / norm(vO);

% Compute the inside points through computation of the planar homography (collineation)
a00 = [x(1);y(1);1];
a10 = [x(2);y(2);1];
a11 = [x(3);y(3);1];
a01 = [x(4);y(4);1];

% Compute the planar collineation: (return the normalization matrix as well)
[Homo,Hnorm,inv_Hnorm] = compute_homography([a00 a10 a11 a01],[0 1 1 0;0 0 1 1;1 1 1 1]);


% Build the grid using the planar collineation:
n_sq_x = width  - 1;
n_sq_y = height - 1;
x_l = ((0:n_sq_x)'*ones(1,n_sq_y+1))/n_sq_x;
y_l = (ones(n_sq_x+1,1)*(0:n_sq_y))/n_sq_y;
pts = [x_l(:) y_l(:) ones((n_sq_x+1)*(n_sq_y+1),1)]';

XX = Homo*pts;
XX = XX(1:2,:) ./ (ones(2,1)*XX(3,:));
grid_pts = XX';
