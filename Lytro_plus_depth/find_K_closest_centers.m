function centers = find_K_closest_centers(center_list, location, K)
% find K closest centers (in center_list) to location
% center_list: (2 x n)
% location   :  2 x 1
    dist2 = sum((center_list - repmat(location, 1, size(center_list, 2))).^2, 1);
    [sorted_dist2, index] = sort(dist2, 'descend');
    centers = center_list(:, index(1:K));
end