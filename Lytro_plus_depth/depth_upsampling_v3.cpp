// upsample centerview depth to lf resolution
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>


void depth_upsampling(const float *center_depth,
                      const float *center_distance,
                      const int* center_index,                    
                      float *result_depth,
                      float *result_distance,
                      int height,
                      int width) {
  // ray tracing 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      int pixel_index = w * height + h;
      int index_of_center = center_index[pixel_index] - 1;     

      result_depth[pixel_index] = center_depth[index_of_center];
      result_distance[pixel_index] = center_distance[index_of_center];
    }
  }  
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // [result_depth, result_dist] = depth_upsampling_v3(center_depth, center_distance, center_index, height, width)
  
  // get center depth map
  float *center_depth = (float *) mxGetData(prhs[0]);
  
  // get center distance map
  float *center_distance = (float *) mxGetData(prhs[1]);
  
  // get center index
  int *center_index = (int *) mxGetData(prhs[2]);

  // get heigth and width of output
  int height = *((int *) mxGetData(prhs[3]));
  int width  = *((int *) mxGetData(prhs[4]));
  
  // allocate output
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result_depth = (float *) mxGetData(plhs[0]);
  
  plhs[1] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result_dist = (float *) mxGetData(plhs[1]);

  // upsampling
  depth_upsampling(center_depth, center_distance, center_index, result_depth, result_dist, height, width);
}