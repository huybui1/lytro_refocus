// map high-res distance map to raw img
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>


float interpolate(const float *input, float x, float y, int height, int width) {
  int x0 = floor(x);
  int y0 = floor(y);
  int x1 = x0 + 1;
  int y1 = y0 + 1;
  
  float d00, d01, d10, d11, d0, d1;
  
  if (x0 >= 0 && x0 < width &&
      y0 >= 0 && y0 < height) 
    d00 = input[x0*height + y0];
  else
    d00 = 0.0f;
    
  if (x0 >= 0 && x0 < width &&
      y1 >= 0 && y1 < height)
    d01 = input[x0*height + y1];
  else
    d01 = 0.0f;    
    
  if (x1 >= 0 && x1 < width &&
      y0 >= 0 && y0 < height)
    d10 = input[x1*height + y0];
  else
    d10 = 0.0f;
    
  if (x1 >= 0 && x1 < width &&
      y1 >= 0 && y1 < height)
    d11 = input[x1*height + y1];
  else
    d11 = 0.0f;
    
  if (d00 == 0.0 || d01 == 0.0 || d10 == 0.0 || d11 == 0.0)
    return 0.0;

  d0 = d01*(y - y0) + d00*(y1 - y);
  d1 = d11*(y - y0) + d10*(y1 - y);
  return d0*(x1 - x) + d1*(x - x0);
}

void compute_variance(const float *LF_intensity,
                      const float * distance_map,
                      const float *center_x,
                      const float *center_y,           
                      int num_centers,
                      float *result,
                      float f, /* mla - sensor distance */
                      int height, int width) {
    
  int pixel_index;
  float dist, max_rad, cx, cy, d, px, py;
  float I, sum_I, sum_I2;
  int count;
  
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	    pixel_index = w * height + h;
      dist = distance_map[pixel_index];
      max_rad = abs(dist)*4/f; // only consider centers inside max_rad
      
      sum_I = 0.0f;
      sum_I2 = 0.0f;
      count = 0;
      for (int k = 0; k < num_centers; k++) {
        cx = center_x[k];
        cy = center_y[k];
        d = (cx - w)*(cx - w) + (cy - h)*(cy - h);
        // float dx = abs(cx - w);
        // float dy = abs(cy - h);

        // if (dx > max_rad || dy > max_rad)
        if (d > max_rad*max_rad)
            continue;
        
        px = w + (cx - w)*(dist + f)/dist; // pixel
        py = h + (cy - h)*(dist + f)/dist;
        
        // intensity at (px, py)
        I = interpolate(LF_intensity, px, py, height, width);
        sum_I += I;
        sum_I2 += I*I;
        count++;
      }
      if (count > 0) {
          result[pixel_index] = (sum_I2 - (sum_I*sum_I/count))/count; // var = E(x^2) - (E(x))^2
      }
    }
  }
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = compute_variance(LF_intensity, distance_map, center_x, center_y,
  //                                         num_centers, f)               
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get LF intensity 
  float *LF_intensity = (float *) mxGetData(prhs[0]);
  
  // get input distance map
  float *distance_map = (float *) mxGetData(prhs[1]);
  
  // get coordinates of centers
  float *center_x = (float *) mxGetData(prhs[2]);
  float *center_y = (float *) mxGetData(prhs[3]);
  
  int num_centers = *((int *)mxGetData(prhs[4]));
  
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[5]));
  
  // allocate output
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  

  compute_variance(LF_intensity, distance_map, center_x, center_y, num_centers, 
          result, f, height, width);
}