function output = upsample_distance(distance_map, radius, result_height, result_width)
    scale = 2*radius;
    [height, width] = size(distance_map);
    output = zeros(result_height, result_width);
    for i = 1:result_height
        for j = 1:result_width
            cx = (j - 0.5)/scale + 0.5;
            cy = (i - 0.5)/scale + 0.5;
            
            cx0 = floor(cx);
            cy0 = floor(cy);
            cx1 = cx0 + 1;
            cy1 = cy0 + 1;
            
            if ((cy0 >= 1) && (cx0 >= 1) && ...
                (cy0 <= height) && (cx0 <= width))
                d00 = distance_map(cy0, cx0);
            else
                d00 = 0;
            end

            if ((cx0 >= 1) && (cy1 >= 1) && ...
                (cx0 <= width) && (cy1 <= height))
                d01 = distance_map(cy1, cx0);
            else
                d01 = 0;
            end

            if ((cy0 >= 1) && (cx1 >=1 ) && ...
                (cy0 <= height) && (cx1 <= width))
                d10 = distance_map(cy0, cx1);
            else
                d10 = 0;
            end

            if ((cx1 >= 1) && (cy1 >= 1) && ...
                (cx1 <= width) && (cy1 <= height))
                d11 = distance_map(cy1, cx1);
            else
                d11 = 0;
            end
            d0 = d00*(cy1 - cy) + d01*(cy - cy0);
            d1 = d10*(cy1 - cy) + d11*(cy - cy0);

            output(i, j) = d0*(cx1 - cx) + d1*(cx - cx0);
        end
    end
end