% refocus using depth
% use white image as confidence map for ray
% use depth-adaptive kernel

clear all
clc
close all


CALIBRATION_DIR = './Calibration/Dec23/';
DEPTH_DIR = '../Data/Depth/Dec23_Depth_Data/zoom_413_focus_1230/';
LF_DIR = '../Data/Images/Dec23_LF_Data/zoom_413_focus_1230/';
RESULTS_DIR = './Results/Dec23/';
JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 22;

% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

% denoising depth with BF
filter_width = 15;
sigma_s = 6;
sigma_r = 50;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));
% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc

% 
% % threshold to remove unvalid depth
% projected_depth(projected_depth >= 700) = 700;    
filled_projected_depth = hole_filling(projected_depth, 20);

figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;

subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

%
[xx, yy] = meshgrid(1:328, 1:328);
figure;
mesh(xx, yy, -projected_depth); title('Projected depth');
% keyboard;

figure;
mesh(xx, yy, -filled_projected_depth); title('Filled projected depth');
% keyboard;

% map depth to distance

disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);

% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);
disp('Getting depth and distance corresponding to each microlens center ...');
tic
[center_depth, center_distance] = get_distance_and_depth_at_centers(center_list, 5, projected_depth, distance_map);
toc


% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end


% refocus by splatting
WHITE_DIR = './Calibration/White_Images/zoom_360/';

% load white image
white_img = imread([WHITE_DIR, 'white_image.tif']);
white_img = double(white_img);

% normalize
white_img = white_img./max(white_img(:));



disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

mla_to_sensor_dist = 25e-3;


% parameter for kernel
b_min = 4;
b_max = 8;
n = 0.2;
ref_dist = min(center_distance);

disp('Splatting ...');
tic
result = splat_w_depth_v3(single(processed_LF), single(u), single(v), ...
                       single(center_distance), int32(center_index), ...
                       single(mla_to_sensor_dist), ....
                       int32(b_min), int32(b_max), single(n), single(ref_dist), ...
                       single(white_img));
toc

final_result = uint16(result*double(intmax('uint16')));
figure;
imshow(final_result); title('Refocused result');
axis image;
axis off;

imwrite(final_result, ...
   [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_minrad_', num2str(b_min), ...
    '_maxrad_', num2str(b_max), '_n_', num2str(n), '_v3.png']);

