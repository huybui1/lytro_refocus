function [cx, cy, center_index] = find_closest_point(x, y, centerX, centerY)
    [height, width] = size(centerX);

    % find region closed to (cx , cy)
    left = 1;
    right = width;
    
    while (left < right) 
        mid = floor((left + right)/2);
%         centerX(1, mid)
        if (centerX(1, mid) < x)
            left = mid + 1;
        else
            right = mid - 1;
        end
    end
    right = mid + 2;
    left = mid - 2;
    
    
    up = 1;
    down = height;
    
    while (up < down)
        mid = floor((up + down)/2);
        if (centerY(mid, 1) < y)
            up = mid + 1;
        else
            down = mid - 1;
        end
    end
    up = mid - 2;
    down = mid + 2;
    
    
    candidateX = centerX(max(up, 1):min(down, height), max(left, 1):min(right, width));
    candidateY = centerY(max(up, 1):min(down, height), max(left, 1):min(right, width));

    dist = (candidateX(:) - repmat(x, length(candidateX(:)), 1)).^2 + ...
           (candidateY(:) - repmat(y, length(candidateY(:)), 1)).^2;
    [minDist, index] = min(dist);
    cx = candidateX(index);
    cy = candidateY(index);
    [dy, dx] = ind2sub(size(dist), index);
    center_row = max(up,1) + dy - 1;
    center_col = max(left, 1) + dx - 1;
    center_index = (center_col - 1)*height + center_row;

end
