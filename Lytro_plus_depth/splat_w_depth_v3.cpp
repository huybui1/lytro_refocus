// do splatting using depth
// + weight ray using white image intensity
// + depth_adaptive kernel
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void splat(const float *LF, /* input raw light field */
           const float *white_img,        
           const float *center_distance,  /* distance map */
		   const int *center_index, /* map pixel to center index */
		   const float *u, const float *v, /* u and v array */
		   float *result,
		   float f,
		   int b_min,  /* kernel parameter: radius = b_min + |depth - ref_depth|^n + w(alpha) */
           int b_max,
           float n,
           float ref_distance,
		   int height, int width) {
  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	  int pixel_index = w * height + h;
	  int index_of_center = center_index[pixel_index] - 1; // base 0 instead of 1 in Matlab
	  float refocused_distance = center_distance[index_of_center];
	  float D = 1.0f + refocused_distance / f;
	
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];
      float confidence = white_img[pixel_index];
      float I1 = LF[pixel_index];
      float I2 = LF[width * height + pixel_index];
      float I3 = LF[2 * width * height + pixel_index];
	  
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);
      
      // determine kernel size
      int rad = b_min + pow(abs(refocused_distance - ref_distance)*1000, n);
      if (rad > b_max)
          rad = b_max;
      
      for (int i = -rad; i <=rad; ++i) {
        int cur_x = cx + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = -rad; j <= rad; ++j) {
          int cur_y = cy + j;
           
          if ( cur_y >= 0 && cur_y < height ) {
//             float r = sqrt((float)(i*i + j*j));
            float r = (float)(i < j)?i:j;
            float wi = 1 - r/rad;
            if (wi < 0.0)
                wi = 0.0;
 
            weight[ cur_x * height + cur_y ] += wi*confidence;
            result[ cur_x * height + cur_y ] += I1*wi*confidence;
            result[ width * height + cur_x * height + cur_y] += I2*wi*confidence;
            result[ 2 * width * height + cur_x * height + cur_y] += I3*wi*confidence;
          }
        }
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        result[ w * height + h ] /= wi;
        result[ width * height + w * height + h] /= wi;
        result[ 2 * width * height + w * height + h] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = splat(LF, u, v, center_distance, center_index, kernel, mla_to_sensor_dist)
  
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[1]);
  float *v = (float *) mxGetData(prhs[2]);
  
  // get center_distance map
  float *center_distance = (float *) mxGetData(prhs[3]);
  
  // get center index map
  int *center_index = (int *)mxGetData(prhs[4]);
  
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[5]));
  
  // kernel parameters
  int b_min = *((int *)mxGetData(prhs[6]));
  int b_max = *((int *)mxGetData(prhs[7]));
  float n = *((float *)mxGetData(prhs[8]));
  float ref_distance = *((float *)mxGetData(prhs[9]));

  // get white image 
  float *white_img = (float *)mxGetData(prhs[10]);
  
  
  // allocate output
  plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  
  // splatting
  splat(LF, white_img, center_distance, center_index, 
        u, v, result, f, b_min, b_max, n, ref_distance, height, width);
}