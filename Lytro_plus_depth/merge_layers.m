function output = merge_layers(layers, masks)
% Merging refocused results in layers together
% Layers (height x width x 3 x #layers)
% Masks  (height x width x #layers)

    dims = size(layers);

    height = dims(1);
    width  = dims(2);
    k = 1;
    if (length(dims) == 3)
      no_layers = dims(3);
    else
      k = dims(3);
      no_layers = dims(4);
    end

    if (k > 1)
        output = zeros(height, width , k);
    else
        output = zeros(height ,width);
    end
    weight = zeros(height, width);

    for i = 1:no_layers
        for c = 1:k
            output(:,:,c) = output(:,:,c) + masks(:,:,i).*layers(:,:,c,i);
        end
        weight = weight + masks(:,:,i).*ones(height, width);
    end

    for col  = 1:width
        for row = 1:height
            w = weight(row, col);
            if (w > 0) 
                for c = 1:k
                  output(row, col, c) = output(row, col, c)/ w;
                end
            end
        end
    end
end