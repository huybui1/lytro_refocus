// splat to a specified distance
// binary mask to indicate which pixels in focus near that distance
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void splat(const float *LF, /* input raw light field */
           const float *center_distance,  /* distance map */
		   const int *center_index, /* map pixel to center index */
		   const float *kernel,
		   const float *u, const float *v, /* u and v array */
		   float *result,
		   bool *mask, /* mask of pixels in focus at refocused dist. */
		   float refocus_distance,
		   float epsilon_dist, /* threshold to create the mask */
		   float f,
		   int rad,
		   int height, int width) {
  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  float D = 1.0f + refocus_distance / f;
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	  int pixel_index = w * height + h;
	  int index_of_center = center_index[pixel_index] - 1; // base 0 instead of 1 in Matlab
	  bool in_focus = false;

	  if (abs(center_distance[index_of_center] - refocus_distance) <= epsilon_dist)
		in_focus = true;
	  	
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];
      float I1 = LF[pixel_index];
      float I2 = LF[width * height + pixel_index];
      float I3 = LF[2 * width * height + pixel_index];
	  
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);
      
      for (int i = 0; i < 2*rad + 1; ++i) {
        int cur_x = cx - rad + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = 0; j < 2*rad + 1; ++j) {
          int cur_y = cy - rad + j;
           
          if ( cur_y >= 0 && cur_y < height ) {
            float wi = kernel[ i * (2*rad + 1) + j ];
 
            weight[ cur_x * height + cur_y ] += wi;
            result[ cur_x * height + cur_y ] += I1*wi;
            result[ width * height + cur_x * height + cur_y] += I2*wi;
            result[ 2 * width * height + cur_x * height + cur_y] += I3*wi;
			mask[cur_x * height + cur_y]  = in_focus;
          }
        }
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        result[ w * height + h ] /= wi;
        result[ width * height + w * height + h] /= wi;
        result[ 2 * width * height + w * height + h] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // [result, mask]= splat_layer(LF, u, v, center_distance, center_index, kernel, 
  //                mla_to_sensor_dist, refocused_distance, epsilon_distance)
  
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get radius of kernel
  int rad = (mxGetM(prhs[5]) - 1) / 2;

  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[1]);
  float *v = (float *) mxGetData(prhs[2]);
  
  // get center_distance map
  float *center_distance = (float *) mxGetData(prhs[3]);
  
  // get center index map
  int *center_index = (int *)mxGetData(prhs[4]);
  
  // get kernel array
  float *kernel = (float *) mxGetData(prhs[5]);

  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[6]));
  
  // refocus distance
  float F = *((float *)mxGetData(prhs[7]));
  
  // epsilon distance
  float epsilon_dist = *((float *)mxGetData(prhs[8]));

  // allocate outputs
  plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  
  plhs[1] = mxCreateLogicalMatrix(height, width);
  bool *mask = (bool *) mxGetData(plhs[1]);
  
  // splatting
  splat(LF, center_distance, center_index, kernel, 
        u, v, result, mask, F, epsilon_dist, f, rad, height, width);
}