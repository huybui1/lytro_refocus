% generate novel view using light field + depth
clear all
clc
close all


CALIBRATION_DIR = './Calibration/Dec23/';
DEPTH_DIR = '../Data/Depth/Dec23_Depth_Data/';
LF_DIR = '../Data/Images/Dec23_LF_Data/';
RESULTS_DIR = './Results/May28/';
JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 17;

% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

% denoising depth with BF
filter_width = 15;
sigma_s = 6;
sigma_r = 50;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));
% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc

% fill in invalid depth value
projected_depth = hole_filling(projected_depth, 5);

figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;

subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

% map depth to distance
disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);

% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);

disp('Getting depth and distance corresponding to each microlens center ...');
tic
[center_depth, center_distance] = get_distance_and_depth_at_centers(center_list, 5, projected_depth, distance_map);
toc

% generate mapping from pixel to center index

img_height = 3280;
img_width = 3280;
MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end

mla_to_sensor_dist = 25e-3;

% upsampled depth map

[upsampled_depth, upsampled_distance] = ...
      depth_upsampling_v3(single(center_depth), single(center_distance), ...
                          int32(center_index),  int32(img_height), int32(img_width));
                      
                      
figure; imagesc(upsampled_depth); axis image; axis off;

figure; imagesc(upsampled_distance); axis image; axis off;



if (~exist('mapped_dist_img17.mat', 'file'))
%     lr_upsampled_distance = upsampled_distance(1000:1063,1000:1063);
    tic
    mapped_distance = map_distance_to_lf_image(single(upsampled_distance), ...
                        single(center_list(1,:)), single(center_list(2,:)), ...
                        int32(size(center_list,2)), single(mla_to_sensor_dist));
%     mapped_distance = map_dist_to_raw_LF(lr_upsampled_distance, center_list, mla_to_sensor_dist);
    toc
    save mapped_dist_img17.mat mapped_distance
else
    load mapped_dist_img17.mat
end

LFP = LFReadLFP([LF_DIR,RAW_LF_FILE]);
PIXEL_PITCH = LFP.Metadata.devices.sensor.pixelPitch*10e3; % in millimeters

% load white image
WHITE_DIR = './Calibration/White_Images/zoom_490/';
white_img = LFReadRaw([WHITE_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);

% normalize
white_img = white_img./max(white_img(:));

disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

% kernel for splatting
rad = 4;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;

mla_to_sensor_dist = 25e-3;


%% Render novel view

% specify virtual camera
cam_height = 3280;
cam_width = 3280;
cam_cx = cam_width/2;
cam_cy = cam_height/2;
cam_fx = 5;
cam_fy = 5;

% Extrinsic
R = eye(3);
t = [3280*PIXEL_PITCH/2, 3280*PIXEL_PITCH/2, -125e-3]';
T = [R t; 0 0 0 1];
invT = inv(T)

[result, result_dist, result_weight] = render_view_from_LF_v2(single(processed_LF), single(u), single(v), ...
                       single(mapped_distance), ...
                       single(kernel), single(mla_to_sensor_dist), ...
                       single(PIXEL_PITCH), single(cam_fx), single(cam_fy), ...
                       single(cam_cx), single(cam_cy), single(invT), ...
                       int32(cam_height), int32(cam_width), single(white_img));

                   
final_result = uint16(result*double(intmax('uint16')));
figure;
imshow(final_result); title('Result');
axis image;
axis off;

figure;
imagesc(result_dist);
title('Distance map');
axis image;
axis off;
% 
% % 
% imwrite(final_result, [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_novel_view.png']);
% save([RESULTS_DIR, RAW_LF_FILE(1:end-4), '_novel_view.mat'], 'result_dist')
%                 
