function [cx, cy, u, v, center_index] = generate_maps2(centerX, centerY, center_rows, center_cols, img_height, img_width, calib_dir)
    % precompute the index of the microlens center (in center_list)
    % corresponding to each image pixel   
    cx = zeros(img_height*img_width, 1);
    cy = zeros(img_height*img_width, 1);
    center_index = zeros(img_height*img_width, 1);
    
	[xx, yy] = meshgrid(1:img_width, 1:img_height);

     % rearrange centers
    parfor index = 1:img_height*img_width
        x = xx(index);
        y = yy(index);
        
        [cx1, cy1, index1] = find_closest_point(x, y, centerX, centerY);
    
        cx(index) = cx1;
        cy(index) = cy1;
        center_index(index) = index1;
    end
  cx = reshape(cx, img_height, img_width);
  cy = reshape(cy, img_height, img_width);
  u = xx - cx;
  v = yy - cy;
  center_index = reshape(center_index, img_height, img_width);
  
  save([calib_dir, 'maps_',num2str(img_height), '_', num2str(img_width),'.mat'], ...
       'u', 'v', 'cx', 'cy', 'center_index');    
end