function [center_depth, center_distance] = get_distance_and_depth_at_centers2(center_height, center_width, depth_map, distance_map)
    center_depth = zeros(center_height, center_width);
    center_distance = zeros(center_height, center_width);    
    [depth_height, depth_width] = size(depth_map);
    for cy = 1:min(center_height, depth_height)
        for cx = 1:min(center_width, depth_width)
            center_depth(cy, cx) = depth_map(cy, cx);
            center_distance(cy, cx) = distance_map(cy, cx);
        end
    end
end