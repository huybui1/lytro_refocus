function projected_depth = project_depth_to_lytro(depth_map, calib_dir, calib_file, center_view_image)
    % project depth map from depth camera to lytro centerview
    % depth_map: depth map captured by depth cam
    % calib_file: result of stereo calibration
    % height_l, width_l: size of lytro centerview image
    
    % kernel
    rad = 4;
    dx = -rad:rad;
    dy = -rad:rad;
    [dxx, dyy] = meshgrid(dx, dy);
%     kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
    kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
%     kernel = ones(2*rad+1, 2*rad+1);
    kernel(kernel < 0) = 0.0;
    
    [height_l, width_l, channels] = size(center_view_image);
    
    load([calib_dir, calib_file]);
    Tr = [R, T];
    figure(50);
    imagesc(center_view_image);
    
    
    figure(100);
    imagesc(depth_map);
    hold on;
    disp('Select the 4 extreme corners of the region of interest');
    u = []; v = [];
    for count = 1:4,
        [ui,vi] = ginput(1);
        figure(100);
        plot(ui,vi,'+','color',[ 1.000 0.314 0.510 ],'linewidth',2);
        u = [u;ui];
        v = [v;vi];
        plot(u,v,'-','color',[ 1.000 0.314 0.510 ],'linewidth',2);
        drawnow;
    end;
    plot([u;u(1)],[v;v(1)],'-','color',[ 1.000 0.314 0.510 ],'linewidth',2);
    drawnow;
    hold off;
    
    projected_depth = zeros(height_l, width_l);
    weight = zeros(height_l, width_l);
    
    u_min = min(u(:)); v_min = min(v(:));
    u_max = max(u(:)); v_max = max(v(:));
    
    for v_d = max(1, floor(v_min)):min(ceil(v_max), size(depth_map, 1))
        for u_d = max(1, floor(u_min)):min(ceil(u_max), size(depth_map,2))
            x_n = normalize([u_d; v_d], fc_left, cc_left, kc_left, alpha_c_left);
            Zd = max(depth_map(v_d, u_d), 0);
            Xd = x_n(1)*Zd;
            Yd = x_n(2)*Zd;
            
            % 3d point from depth cam
            pts_3d_d = [Xd; Yd; Zd; 1];
            
            % 3d point in Lytro coordinate
            pts_3d_s = Tr*pts_3d_d;

            % normalize
%             if (pts_3d_s(3) > 0)
%                 xs_n = [pts_3d_s(1)/pts_3d_s(3); pts_3d_s(2)/pts_3d_s(3)];
%                 xs_d = apply_distortion(xs_n, kc_right);
                %-------
    %             
    %             % project to pixel coordinate
    %             % normalize
    %             xs_n = pts_3d_s(1)/pts_3d_s(3);
    %             ys_n = pts_3d_s(2)/pts_3d_s(3);
    %             
    %             % apply distortion
    %             r2 = xs_n*xs_n + ys_n*ys_n;
    %             xs_g  = 2*kc_right(3)*xs_n*ys_n + kc_right(4)*(r2 + 2*xs_n*xs_n);
    %             ys_g = kc_right(3)*(r2 + 2*ys_n*ys_n) + 2*kc_right(4)*xs_n*ys_n;
    %             
    %             coef = (1 + kc_right(1)*r2 + kc_right(2)*r2^2 + kc_right(5)*r2^3);
    %             xs_k = coef*xs_n + xs_g;
    %             ys_k = coef*ys_n + ys_g;
    %             
    %             u_s = fc_right(1)*xs_k + alpha_c_right*ys_k + cc_right(1);
    %             v_s = fc_right(2)*ys_k + cc_right(2);
    %----------------
%                 u_s = fc_right(1)*(xs_d(1) + alpha_c_right*xs_d(2)) + cc_right(1);
%                 v_s = fc_right(2)*xs_d(2) + cc_right(2);            


                uu_s = KK_right*pts_3d_s;
                uu_s = uu_s./uu_s(3);
                u_s = round(uu_s(1)); v_s = round(uu_s(2)); 
%                 u_s = round(u_s); v_s = round(v_s);

                for j = max(1, v_s - rad):min(height_l, v_s + rad)
                    for i = max(1, u_s - rad): min(width_l, u_s + rad)
                       wi = kernel(rad + j - v_s + 1, rad + i - u_s + 1);
                       projected_depth(j, i) = projected_depth(j, i, 1) + pts_3d_s(3)*wi;     
                       weight(j, i) = weight(j, i) + wi;                    
                    end
                end
%             end % pts_3d_s(3) ~= 0
        end
    end
    
    for j = 1:height_l
        for i = 1:width_l;
          if (weight(j, i) ~= 0)
              projected_depth(j, i) = projected_depth(j, i)/weight(j,i);
          end
        end
    end

end
