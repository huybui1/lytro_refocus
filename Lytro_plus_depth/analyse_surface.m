% splat each ray to corresponding distance (depth) + weight ray using white image
% return also a distance (depth) map 
clear all
clc
close all


CALIBRATION_DIR = './Calibration/Dec23/';
DEPTH_DIR = '../Data/Depth/Dec23_Depth_Data/';
LF_DIR = '../Data/Images/Dec23_LF_Data/';
RESULTS_DIR = './Results/Apr20/';
JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 1;

% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

% threshold depth
% depth(depth >= 800) = 0;
% denoising depth with BF
filter_width = 6;
sigma_s = 3;
sigma_r = 20;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));
% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc

% fill in invalid depth value
projected_depth = hole_filling(projected_depth, 10);

% figure;
% imagesc(projected_depth);
% colormap gray;
% axis image;
% axis off;

figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;


subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

%
[xx, yy] = meshgrid(1:328, 1:328);
figure;
mesh(xx, yy, -projected_depth); axis([0 400 0 400 -1200 -600]);
% keyboard;


% map depth to distance

disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);
figure; 
imagesc(distance_map);


% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);
disp('Getting depth and distance corresponding to each microlens center ...');
tic
[center_depth, center_distance] = get_distance_and_depth_at_centers(center_list, 5, projected_depth, distance_map);
toc


% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end

mla_to_sensor_dist = 25e-3;
[upsampled_depth, upsampled_distance] = ...
      depth_upsampling_v3(single(center_depth), single(center_distance), ...
                          int32(center_index),  int32(img_height), int32(img_width));
                      
                      
figure; imagesc(upsampled_depth); axis image; axis off;

figure; imagesc(upsampled_distance); axis image; axis off;



% refocus by splatting
WHITE_DIR = './Calibration/White_Images/zoom_490/';

% load white image
% white_img = imread([WHITE_DIR, 'white_image.tif']);
white_img = LFReadRaw([WHITE_DIR, 'white_image.RAW'], '12bit');

white_img = double(white_img);

% normalize
white_img = white_img./max(white_img(:));



disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

LF_intensity = rgb2gray(processed_LF);
figure;
imagesc(LF_intensity); colormap gray;
title('LF intensity');

pause;

tic
variance_map = compute_intensity_variance_per_location(single(LF_intensity),  single(upsampled_distance), ...
                        single(center_list(1,:)), single(center_list(2,:)), ...
                        int32(size(center_list,2)), single(mla_to_sensor_dist));
toc

figure;
imagesc(variance_map);
title('Variance map');                
% axis image;
% axis off;
% 

