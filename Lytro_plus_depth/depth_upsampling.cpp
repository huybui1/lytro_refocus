// upsample centerview depth to lf resolution
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

float interpolate(const float *input, float x, float y, int height, int width) {
  int x0 = floor(x);
  int y0 = floor(y);
  int x1 = x0 + 1;
  int y1 = y0 + 1;
  
  float d00, d01, d10, d11, d0, d1;
  
  if (x0 >= 0 && x0 < width &&
      y0 >= 0 && y0 < height) 
    d00 = input[x0*height + y0];
  else
    d00 = 0.0f;
    
  if (x0 >= 0 && x0 < width &&
      y1 >= 0 && y1 < height)
    d01 = input[x0*height + y1];
  else
    d01 = 0.0f;    
    
  if (x1 >= 0 && x1 < width &&
      y0 >= 0 && y0 < height)
    d10 = input[x1*height + y0];
  else
    d10 = 0.0f;
    
  if (x1 >= 0 && x1 < width &&
      y1 >= 0 && y1 < height)
    d11 = input[x1*height + y1];
  else
    d11 = 0.0f;
    
  d0 = d01*(y - y0) + d00*(y1 - y);
  d1 = d11*(y - y0) + d10*(y1 - y);
  return d0*(x1 - x) + d1*(x - x0);
}

void depth_upsampling(const float *center_distance,  /* distance map */
           const float *lr_depth,
           const float *lr_distance,
		       const int *center_index, /* map pixel to center index */
		       const float *u, const float *v, /* u and v array */
		       float *result_depth,
		       float *result_dist,          
           float f,
		       int lr_height, int lr_width,
           int height, int width) {
 
  // ray tracing 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      int pixel_index = w * height + h;
      int index_of_center = center_index[pixel_index] - 1; // base 0 instead of 1 in Matlab
      float distance = center_distance[index_of_center];
      float D = 1.0f + distance / f;
	
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];    
      sx /= 10;
      sy /= 10;
      
      // interpolate depth and distance at (sy, sx)
      result_depth[pixel_index] = interpolate(lr_depth, sx, sy, lr_height, lr_width);
      result_dist[pixel_index] = interpolate(lr_distance, sx, sy, lr_height, lr_width);
    }
  }  
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // [result_depth, result_dist] = depth_upsampling(lr_depth, lr_distance, center_distance,
  //    center_index, u, v, f, height, width)                                      
  const int* dims = mxGetDimensions(prhs[0]);
  int lr_height = dims[0];
  int lr_width =  dims[1];
  
  // get input depth map
  float *lr_depth = (float *) mxGetData(prhs[0]);
  
  // get input distance map
  float *lr_distance = (float *) mxGetData(prhs[1]);
  
  // get center_distance map
  float *center_distance = (float *) mxGetData(prhs[2]);
  
  // get center index map
  int *center_index = (int *)mxGetData(prhs[3]);  

  // get u, v array
  float *u = (float *) mxGetData(prhs[4]);
  float *v = (float *) mxGetData(prhs[5]);

  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[6]));

  // get heigth and width of output
  int height = *((int *) mxGetData(prhs[7]));
  int width  = *((int *) mxGetData(prhs[8]));
  
  // allocate output
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result_depth = (float *) mxGetData(plhs[0]);
  
  plhs[1] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result_dist = (float *) mxGetData(plhs[1]);

  // upsampling
  depth_upsampling(center_distance, lr_depth, lr_distance, center_index,
                   u, v, result_depth, result_dist, f, lr_height, lr_width, height, width);
}