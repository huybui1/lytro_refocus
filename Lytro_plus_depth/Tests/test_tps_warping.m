% test
close all
clc
clear all

addpath('../../DownloadedCodes/tpsWarp/');

load test_data.mat

input_img = double(input_img);
input_img  = input_img./max(input_img(:));
H = size(input_img, 1);
W = size(input_img, 2);

outdim = [W, H];
source_landmarks = [samples_u(:), samples_v(:)];
dest_landmarks = [final_samples_u(:), final_samples_v(:)];

% interpolation params
interp.method = 'invdist'; %'nearest', 'none'
interp.radius = 10; % radius or median filter dimension
interp.power = 2; % power for inverse weighing interpolation method

[output, output_w_holes, hole_map] = tpswarp(input_img, outdim, ...
                source_landmarks, dest_landmarks, interp);
            
figure;
subplot(1,3,1);
subimage(input_img); title('Input'); 
axis image; axis off;

subplot(1,3,2);
subimage(output_w_holes); title('Output with no hole filling');
axis image; axis off;

subplot(1,3,3);
subimage(output); title('Output after hole filling');
axis image; axis off;

