function g = tps_energy(p1, p2)
r2 = sum((p1 - p2).^2);

if (r2 == 0) 
    r2 = realmin; 
end
    
g = r2*log(r2);

