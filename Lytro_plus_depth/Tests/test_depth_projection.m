clear all
clc
close all

calib_file = 'Calib_Results_stereo.mat';
DEPTH_DIR = './Dec9_Depth_Data/';
LF_DIR = 'Dec9_LF_Data/';

img_no = 4;

LF_SUB_FILE = [LF_DIR, 'CI_IMG_', num2str(img_no, '%04i'), '.bmp'];
DEPTH_FILE = [DEPTH_DIR, 'Depth_', num2str(img_no, '%04i'), '.mat'];


subaperture_img = imread(LF_SUB_FILE);
load(DEPTH_FILE);
depth = double(depth_frame);
% denoising with BF
filter_width = 5;
sigma_s = 6;
sigma_r = 100;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));

projected_depth = project_depth_to_lytro(filtered_depth, calib_file, subaperture_img);

figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;

subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

% test
[height, width] = size(projected_depth);

x = 1:width;
y = 1:height;
[xx, yy] = meshgrid(x, y);
figure; mesh(xx, yy,  -double(projected_depth));
             
