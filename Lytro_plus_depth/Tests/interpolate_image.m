function output = interpolate_image(x, y, input)
% get value at point (x, y) by interpolation

% input is RGB image
height = size(input, 1);
width = size(input, 2);
x0 = floor(x);
y0 = floor(y);
x1 = x0 + 1;
y1 = y0 + 1;

if (x0 > 0 && x0 <= width &&...
    y0 > 0 && y0 <= height)
   d00 = squeeze(input(y0, x0,:));
else
   d00 = zeros(3,1);
end

if (x0 > 0 && x0 <= width && ...
    y1 > 0 && y1 <= height)
    d01 = squeeze(input(y1, x0,:));
else
    d01 = zeros(3,1);
end

if (x1 > 0 && x1 <= width && ...
    y0 > 0 && y0 <= height)
    d10 = squeeze(input(y0, x1,:));
else
    d10 = zeros(3,1);
end

if (x1 > 0 && x1 <= width && ...
    y1 > 0 && y1 <= height)
    d11 = squeeze(input(y1, x1,:));
else
    d11 = zeros(3,1);
end

d0 = d01*(y - y0) + d00*(y1 - y);
d1 = d11*(y - y0) + d10*(y1 - y);

output = d0*(x1 - x) + d1*(x - x0);

    