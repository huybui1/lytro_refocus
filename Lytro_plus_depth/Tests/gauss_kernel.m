function g = gauss_kernel(p1, p2, sigma)
diff = p1 - p2;
g = exp(-0.5*sum(diff.^2)/sigma^2);

