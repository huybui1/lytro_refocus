% test
close all
clc
clear all

load test_data.mat
sigma = 2;

input_img = double(input_img);
input_img = input_img./max(input_img(:));
tic
% output = RBF_warp(input_img, samples_u, samples_v, final_samples_u, final_samples_v, sigma);
output = RBF_warp_tps(input_img, samples_u, samples_v, final_samples_u, final_samples_v);
toc
figure;
imagesc(input_img); title('Input');
axis image;
axis off;

figure;
imagesc(output); title('Output');
axis image;
axis off;