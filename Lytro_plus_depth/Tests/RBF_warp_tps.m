function output = RBF_warp_tps(input, x1, y1, x2, y2) 
% warp input image using Radial basis function method
% (x1, y1): input landmarks
% (x2, y2): output landmarks
% sigma: scale (radius) of the RBF kernel
x1 = x1(:);
y1 = y1(:);
x2 = x2(:);
y2 = y2(:);

num_pts = length(x1);

K = eye(num_pts);
% find alpha vector
for i = 1:num_pts
    for j = i+1:num_pts
        p1 = [x1(i), y1(i)];
        p2 = [x2(j), y2(j)];
        K(i, j) = tps_energy(p1, p2);
        K(j, i) = K(i, j);
    end
end

alpha_x = K \ (x2 - x1);
alpha_y = K \ (y2 - y1);
alpha = [alpha_x' ; alpha_y'];

output = zeros(size(input));
height = size(input,1);
width = size(input, 2);

for y = floor(min(y2(:))):ceil(max(y2(:)))
    for x = floor(min(x2(:))):ceil(max(x2(:)))
        out_loc = [x; y];
        in_loc = out_loc;
        
        for i = 1:size(alpha, 2)
            v = [x2(i); y2(i)];
            k = tps_energy(out_loc, v);
            in_loc = in_loc - k*alpha(:,i);
        end
        output(y, x, :) = interpolate_image(in_loc(1), in_loc(2), input);
    end
end