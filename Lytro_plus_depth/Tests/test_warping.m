% test image warping
load mandrill

colormap(map)

% select input region
figure(1); imagesc(X); 
disp('Select 4 points:')

movingPts = [];
figure(1); hold on;
for i = 1:4
    [x y] = ginput(1);
    x = round(x); y = round(y);
    plot(x, y,'r*');
    movingPts = [movingPts; x y];
    hold on;
end
hold off;

minMovingx = min(movingPts(:,1));
minMovingy = min(movingPts(:,2));
maxMovingx = max(movingPts(:,1));
maxMovingy = max(movingPts(:,2));

movingPts = [minMovingx , minMovingy;
             maxMovingx , minMovingy;
             maxMovingx , maxMovingy;
             minMovingx , maxMovingy];
       
figure(1);
imagesc(X);
hold on;
plot(movingPts(:,1), movingPts(:,2));
hold on;
line([movingPts(end, 1), movingPts(1,1)], [movingPts(end,2), movingPts(1,2)]);


% select output region
blank = ones(size(X))
figure(2); imagesc(blank); colormap gray;
disp('Select 4 points:')

fixedPts = [];
figure(2); hold on;
for i = 1:4
    [x y] = ginput(1);
    x = round(x); y = round(y);
    plot(x, y,'r*');
    fixedPts = [fixedPts; x y];
    hold on;
end
hold off;

% minFixedx = min(fixedPts(:,1));
% minFixedy = min(fixedPts(:,2));
% maxFixedx = max(fixedPts(:,1));
% maxFixedy = max(fixedPts(:,2));
% 
% fixedPts = [minFixedx , minFixedy;
%             maxFixedx , minFixedy;
%             maxFixedx , maxFixedy;
%             minFixedx , maxFixedy];
       
figure(2);
imagesc(blank); colormap gray;
hold on;
plot(fixedPts(:,1), fixedPts(:,2));
hold on;
line([fixedPts(end, 1), fixedPts(1,1)], [fixedPts(end,2), fixedPts(1,2)]);

% crop input
input = zeros(size(X));
input(minMovingy:maxMovingy, minMovingx:maxMovingx,:) = X(minMovingy:maxMovingy, minMovingx:maxMovingx, :);

figure; imagesc(input); title('Cropped input');

% find transformation
tform = fitgeotrans(movingPts, fixedPts, 'Affine');

% warp
height = size(X,1);
width = size(X,2);

output = imwarp(input, tform, 'OutputView', imref2d([height, width]));

figure; imagesc(output);
hold on;
plot(fixedPts(:,1), fixedPts(:,2));
hold on
line([fixedPts(end, 1), fixedPts(1,1)], [fixedPts(end,2), fixedPts(1,2)]);