function result = map_dist_to_raw_LF(distance_map, center_list, mu)
% map a distance map to the sensor via MLA
% center_list: list of microlens coordinates
% mu: distance from MLA to sensor array

    [height, width] = size(distance_map);
    num_centers = size(center_list, 2);
    result = zeros(height, width);
    weight = zeros(height, width);
    for h = 1:height
        for w = 1:width
            loc = [w; h];
            dist = distance_map(h, w);
            max_rad = dist*5/mu; % only considered centers inside max_rad
            
            for center_idx = 1:num_centers
                center = center_list(:, center_idx);
                diff  = max(abs(center - loc));
                if (diff > max_rad)
                  continue;
                end
                
                p = loc + (center - loc)*(dist + mu)/dist;
                
                rounded_p = floor(p + [0.5; 0.5]);
                if ((rounded_p(1) > 0) && (rounded_p(1) < width) && ...
                    (rounded_p(2) > 0) && (rounded_p(2) < width) && ...
                    (max(abs(rounded_p - center)) < 5))
                  weight(rounded_p(2), rounded_p(1)) = weight(rounded_p(2), rounded_p(1)) + 1;
                  result(rounded_p(2), rounded_p(1)) = result(rounded_p(2), rounded_p(1)) + dist;
                end
            end
        end
    end

    result(weight > 0) = result(weight > 0)./weight(weight > 0);
end