function output = Local_affine_warp(input_img, input_x, input_y, output_x, output_y)
% warp an image
% input_img: original image
% input_x, input_y: coordinates of samples in original image
% ouput_x, output_y: coordinates of samples in output image
% input grid is regular sampled (using meshgrid)
[height ,width] = size(input_x);
img_height = size(input_img,1);
img_width  = size(input_img,2);
output = zeros(size(input_img));
input_img = double(input_img);
input_img = input_img./max(input_img(:));

weight = zeros(img_height, img_width);
white_img = ones(img_height, img_width);
% figure(10); imagesc(input_img)
for i = 2:height
    for j = 2:width
       movingPts = [input_x(i, j-1)  , input_y(i, j-1);
                    input_x(i, j)    , input_y(i, j);
                    input_x(i-1, j)  , input_y(i-1, j);
                    input_x(i-1, j-1), input_y(i-1, j-1)];
                
       fixedPts = [ output_x(i, j-1)  , output_y(i, j-1);
                    output_x(i, j)    , output_y(i, j);
                    output_x(i-1, j)  , output_y(i-1, j);
                    output_x(i-1, j-1), output_y(i-1, j-1)];
                
       cropped_in = zeros(size(input_img));
       cropped_white = zeros(height, width);
       minInput_x = min(movingPts(:,1));
       maxInput_x = max(movingPts(:,1));
       minInput_y = min(movingPts(:,2));
       maxInput_y = max(movingPts(:,2));
       
%        figure(10);
%        hold on;
%        plot(movingPts(:,1), movingPts(:,2), 'b');
%        hold on
%        plot(fixedPts(:,1), fixedPts(:,2), 'r');
%        hold off;
       
       cropped_in(minInput_y:maxInput_y, minInput_x:maxInput_x,:) = ...
           input_img(minInput_y:maxInput_y, minInput_x:maxInput_x,:);
       cropped_white(minInput_y:maxInput_y, minInput_x:maxInput_x) = ...
           white_img(minInput_y:maxInput_y, minInput_x:maxInput_x);
%        figure(100); imagesc(cropped_in); hold on;
%        keyboard
       tform = fitgeotrans(movingPts, fixedPts, 'Affine');
       warped = imwarp(cropped_in, tform, 'OutputView', imref2d(size(output)));
       warped_white = imwarp(cropped_white, tform, 'OutputView', imref2d(size(output)));
       
       output = output + warped;
       weight = weight + warped_white;
       
%        keyboard
%        temp = zeros(size(output));
%        for i1 = 1:img_height
%            for j1 = 1:img_width
%                if (weight(i1,j1) ~= 0)
%                     temp(i1,j1,1) = output(i1,j1,1)/weight(i1,j1);
%                     temp(i1,j1,2) = output(i1,j1,2)/weight(i1,j1);
%                     temp(i1,j1,3) = output(i1,j1,3)/weight(i1,j1);
%                 end
%             end
%        end
%         
%        figure; imagesc(temp); keyboard
    end
end

for i = 1:img_height
    for j = 1:img_width
        if (weight(i,j) ~= 0)
            output(i,j,1) = output(i,j,1)/weight(i,j);
            output(i,j,2) = output(i,j,2)/weight(i,j);
            output(i,j,3) = output(i,j,3)/weight(i,j);
        end
    end
end
% figure; imagesc(output);
        




