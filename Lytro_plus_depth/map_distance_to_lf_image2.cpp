// map high-res distance map to raw img
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void mapping(const float * distance_map,
             const float *center_x,
             const float *center_y,           
		         int num_centers,
             float *result,
		         float f, /* mla - sensor distance */
             int height, int width) {
    
  int pixel_index;
  float dist, max_rad;
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	    pixel_index = w * height + h;
      dist = distance_map[pixel_index];
      float max_rad = abs(dist)*5/f; // only consider centers inside max_rad
      if (dist == 0)
        continue;
        
      for (int k = 0; k < num_centers; k++) {
        float cx = center_x[k];
        float cy = center_y[k];
        float d = (cx - w)*(cx - w) + (cy - h)*(cy - h);
        if (d > max_rad*max_rad)
            continue;
        
        float px = w + (cx - w)*(dist + f)/dist;// pixel
        float py = h + (cy - h)*(dist + f)/dist;
        
        int px0 = floor(px);
        int py0 = floor(py);
        int px1 = px0 + 1;
        int py1 = py0 + 1;
        
        if (px0 >= 0 && px0 < width && py0 >= 0 && py0 < width &&
            abs(px0 - cx) < 5 && abs(py0 - cy) < 5) {
          if (dist < result[px0*height + py0])
              result[px0*height + py0] = dist;
        }
        
        if (px0 >= 0 && px0 < width && py1 >= 0 && py1 < width &&
            abs(px0 - cx) < 5 && abs(py1 - cy) < 5) {
          if (dist < result[px0*height + py1])
              result[px0*height + py1] = dist;          
        }        
        
        if (px1 >= 0 && px1 < width && py0 >= 0 && py0 < width &&
            abs(px1 - cx) < 5 && abs(py0 - cy) < 5) {
          if (dist < result[px1*height + py0])
              result[px1*height + py0] = dist;          
        }        
        
        if (px1 >= 0 && px1 < width && py1 >= 0 && py1 < width &&
            abs(px1 - cx) < 5 && abs(py1 - cy) < 5) {
          if (dist < result[px1*height + py1])
              result[px1*height + py1] = dist;          
        }
      }          
    }
  }
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = map_distance_to_lf_image(distance_map, center_x, center_y,
  //                                   num_centers, f)               
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get input distance map
  float *distance_map = (float *) mxGetData(prhs[0]);
  
  // get coordinates of centers
  float *center_x = (float *) mxGetData(prhs[1]);
  float *center_y = (float *) mxGetData(prhs[2]);
  
  int num_centers = *((int *)mxGetData(prhs[3]));
  
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[4]));
  
  // allocate output
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  

  mapping(distance_map, center_x, center_y, num_centers, 
          result, f, height, width);
}