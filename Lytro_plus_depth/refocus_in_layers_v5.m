% splat to a specified distance + 
% smooth (Gaussian) mask to indicate which pixels in focus near that distance 
%+ weight pixel w. white image intensity + depth adaptive kernel


clear all
clc
close all



CALIBRATION_DIR = './Calibration/Dec23/';
DEPTH_DIR = '../Data/Depth/Dec23_Depth_Data/zoom_413_focus_1230/';
LF_DIR = '../Data/Images/Dec23_LF_Data/zoom_413_focus_1230/';
RESULTS_DIR = './Results/Dec23/Layered_refocusing_v5/';

JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 17;

% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

% denoising depth with BF
filter_width = 15;
sigma_s = 6;
sigma_r = 50;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));
% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc

% % threshold to remove unvalid depth
% projected_depth(projected_depth >= 700) = 700;

figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;

subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

% map depth to distance

disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);

% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);
disp('Getting depth and distance corresponding to each microlens center ...');
tic
[center_depth, center_distance] = get_distance_and_depth_at_centers(center_list, 5, projected_depth, distance_map);
toc


% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end


% refocus by splatting
WHITE_DIR = './Calibration/White_Images/zoom_360/';
% load white image
white_img = imread([WHITE_DIR, 'white_image.tif']);
white_img = double(white_img);

% normalize
white_img = white_img./max(white_img(:));

disp('Preprocessing light field image ...');
tic
[processed_LF, gamma_corrected_white_img] = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

% kernel for splatting
min_rad = 3;
max_rad = 12;

n = 0.25;

mla_to_sensor_dist = 25e-3; % in millimeter

disp('Splatting in layers...');

min_distance = min(center_distance(:));
max_distance = max(center_distance(:));

no_layers = 10;
delta_distance = (max_distance - min_distance)/(no_layers - 1);
epsilon = delta_distance/2;

all_distances = min_distance:delta_distance:max_distance;
layers = zeros(img_height, img_width, 3, no_layers);
masks  = zeros(img_height, img_width, no_layers);

for k = 1:no_layers
    disp(['Refocusing on layer ', num2str(k), '...']);
    
    refocus_distance = all_distances(k);
    tic
    [this_layer, this_mask] = splat_layer_adaptive_kernel(single(processed_LF), ...
                                    single(u), single(v), ...
                                    single(center_distance), ...
                                    int32(center_index), ...
                                    single(mla_to_sensor_dist), ...
                                    single(refocus_distance), ...
                                    single(epsilon), ...
                                    int32(min_rad), ...
                                    int32(max_rad), ...
                                    single(n), ...
                                    single(gamma_corrected_white_img));
    toc                                     
    imwrite(uint16(this_layer*double(intmax('uint16'))), ...
            [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_layer_', num2str(k),'_min_rad_', num2str(min_rad), '_v5.png']);   
    imwrite(uint8(this_mask*255), ...
            [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_mask_layer_', num2str(k),'_min_rad_', num2str(min_rad), '_v5.png']);
%     figure; imshow(this_layer); title(['Layer ', num2str(k)]); 
%     axis image; axis off;
%     drawnow;
%     de
%     figure; imagesc(this_mask); colormap gray;
%     title(['Mask for layer ', num2str(k)]);
%     axis image; axis off;
%     drawnow;
    
%     pause;
    
    
   
    layers(:,:, :, k) = this_layer;
    masks(:,:, k) = this_mask;

                       
end

% merging
disp('Merging layers ...')

final_result = merge_layers(layers, masks);
final_result = uint16(final_result*double(intmax('uint16')));

figure;
imshow(final_result); title('Refocused result');
axis image;
axis off;

imwrite(final_result, [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_min_rad_',num2str(min_rad), '_merged_v5.png']);


