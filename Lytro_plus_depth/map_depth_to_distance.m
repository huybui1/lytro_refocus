function distance_map = map_depth_to_distance(depth_map, calib_dir, lf_calib_file)
    % load intrinsic params
    load([calib_dir, lf_calib_file]);
    
    K1 = IntParamLF(1);
    K2 = IntParamLF(2);
    
    mu = 25e-3; % distance from MLA to sensor
    
    % L*(L/mu + 1) = K2
    coefs = [1/mu, 1, -K2];
    r = roots(coefs);
    
    if (r(1) > 0)
        L = r(1);
    else
        L = r(2);
    end
    
    F = 1/(K1 + 1/L);
    
    distance_map = L - F*depth_map ./ (depth_map - F);
    distance_map(depth_map == 0) = 0;
        
%     distance_map(abs(distance_map) > 1e-3) = 0;
end