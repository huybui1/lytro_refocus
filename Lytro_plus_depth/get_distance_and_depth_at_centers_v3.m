function [center_depth, center_distance] = get_distance_and_depth_at_centers_v3(center_list, radius, depth_map, distance_map)
    scale = 2*radius;
    center_X = (center_list(1,:) - 0.5)/scale + 0.5;
    center_Y = (center_list(2,:) - 0.5)/scale + 0.5;
    
    num_centers = length(center_X);
    center_depth = zeros(1, num_centers);
    center_distance = zeros(1, num_centers);

    [height, width] = size(depth_map);
    
    for i = 1:num_centers
        cx = center_X(i);
        cy = center_Y(i);
        cx0  = floor(cx);
        cy0  = floor(cy);
        cx1 = cx0 + 1;
        cy1 = cy0 + 1;

        if ((cy0 >= 1) && (cx0 >= 1))
           D00 = depth_map(cy0, cx0);
           d00 = distance_map(cy0, cx0);
        else
           D00 = 0;
           d00 = 0;
        end

        if ((cx0 >= 1) && (cy1 <= height))
           D01 = depth_map(cy1, cx0);
           d01 = distance_map(cy1, cx0);
        else
           D01 = 0;
           d01 = 0;
        end

        if ((cy0 >= 1) && (cx1 <= width))
           D10 = depth_map(cy0, cx1);
           d10 = distance_map(cy0, cx1);
        else
           D10 = 0;
           d10 = 0;
        end

        if ((cx1 <= width) && (cy1 <= height))
           D11 = depth_map(cy1, cx1);
           d11 = distance_map(cy1, cx1);
        else
           D11 = 0;
           d11 = 0;
        end


        D0 = D00*(cy1 - cy) + D01*(cy - cy0);
        D1 = D10*(cy1 - cy) + D11*(cy - cy0);

        d0 = d00*(cy1 - cy) + d01*(cy - cy0);
        d1 = d10*(cy1 - cy) + d11*(cy - cy0);

        center_depth(i) = D0*(cx1 - cx) + D1*(cx - cx0);
        center_distance(i) = d0*(cx1 - cx) + d1*(cx - cx0);
    end
end