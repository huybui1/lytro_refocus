// do splatting using depth
// + weight ray using white image intensity
// + depth_adaptive kernel

#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void splat(const float *LF, /* input raw light field */
           const float *white_img,   
           const float *center_distance,  /* distance map */
		   const int *center_index, /* map pixel to center index */
		   const float *u, const float *v, /* u and v array */
		   float *result,
		   float *mask, /* weights of pixels in focus at refocused dist. */
		   float refocused_distance,
		   float epsilon_dist, /* threshold to create the mask */
		   float f,
		   int min_rad, /* parameter for kernel */
		   int max_rad,
		   float n,
		   int height, int width) {
  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  float D = 1.0f + refocused_distance / f;
  float sigma_dist2 = epsilon_dist*epsilon_dist;
  // int max_rad = 10;
  
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	  int pixel_index = w * height + h;
	  int index_of_center = center_index[pixel_index] - 1; // base 0 instead of 1 in Matlab

      float dist = center_distance[index_of_center];
      float dist_weight = exp(-(dist - refocused_distance)*(dist - refocused_distance) / (2*sigma_dist2));
      float confidence = white_img[pixel_index];      
      
	  // determine kernel size
      int rad = min_rad + pow(abs(refocused_distance - dist)*1000, n);

	  
	  if (rad > max_rad)
		rad = max_rad;
		
	  // mexPrintf("Rad(%d, %d) = %d\n", h, w, rad);
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];
      float I1 = LF[pixel_index];
      float I2 = LF[width * height + pixel_index];
      float I3 = LF[2 * width * height + pixel_index];
	  
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);
      
      for (int i = -rad; i <= rad; ++i) {
        int cur_x = cx + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = -rad; j <= rad; ++j) {
          int cur_y = cy + j;
           
          if ( cur_y >= 0 && cur_y < height ) {
            // float r = sqrt((float)(i*i + j*j));
            float r = (float) (i < j)?i:j;
            float wi = 1.0 - r/(rad*1.0);
            if (wi < 0.0)
                wi = 0.0;
 
            weight[ cur_x * height + cur_y ] += wi*confidence;
            result[ cur_x * height + cur_y ] += I1*wi*confidence;
            result[ width * height + cur_x * height + cur_y] += I2*wi*confidence;
            result[ 2 * width * height + cur_x * height + cur_y] += I3*wi*confidence;
            mask[cur_x * height + cur_y] += dist_weight*wi*confidence;
          }
        }
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        result[ w * height + h ] /= wi;
        result[ width * height + w * height + h] /= wi;
        result[ 2 * width * height + w * height + h] /= wi;
        mask[ w * height + h] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // [result, mask]= splat_layer_adaptive_kernel(LF, u, v, center_distance,
  //                center_index, kernel, 
  //                mla_to_sensor_dist, refocused_distance, epsilon_distance,
  //                min_rad, n, white_img)
  
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[1]);
  float *v = (float *) mxGetData(prhs[2]);
  
  // get center_distance map
  float *center_distance = (float *) mxGetData(prhs[3]);
  
  // get center index map
  int *center_index = (int *)mxGetData(prhs[4]);
  
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[5]));
  
  // refocus distance
  float F = *((float *)mxGetData(prhs[6]));
  
  // epsilon distance
  float epsilon_dist = *((float *)mxGetData(prhs[7]));

  
  // kernel parameters
  int min_rad = *((int *)mxGetData(prhs[8]));
  int max_rad = *((int *)mxGetData(prhs[9]));
  float exponent = *((float *)mxGetData(prhs[10]));
  
    // get white image 
  float *white_img = (float *)mxGetData(prhs[11]);
  
  // allocate outputs
  plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  
  plhs[1] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *mask = (float *) mxGetData(plhs[1]);
  
  // splatting
  splat(LF, white_img, center_distance, center_index, 
        u, v, result, mask, F, epsilon_dist, f, min_rad, max_rad, exponent, height, width);
}