// render novel view, given LF, distance map and virtual camera params
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void render(const float *LF, /* input raw light field */
           const float *white_img,
           const float *center_distance,  /* distance map */
		       const float *transformation, /* transformation to virtual cam coordinates */
		       const int *center_index, /* map pixel to center index */
		       const float *kernel,
		       const float *u, const float *v, /* u and v array */
		       float *result,  /* resulting image */
		       float *result_dist, /* resulting distance map */
		       float mu, /* mla - sensor distance */
		       int rad, /* radius of the splatting kernel */
           float fx, /* focal length of virtual camera */
           float fy, /* focal length .. */
           float cx, /* center of virtual camera */
           float cy,
           float pixel_pitch, /* in millimeter */
		       int height, int width,
           int result_height, int result_width) {
  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      int pixel_index = w * height + h;
      int index_of_center = center_index[pixel_index] - 1; // base 0 instead of 1 in Matlab
      float refocused_distance = center_distance[index_of_center];
      float D = 1.0f + refocused_distance / mu;
    
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];
      float confidence = white_img[pixel_index];
      confidence *= confidence;
      float I1 = LF[pixel_index];
      float I2 = LF[width * height + pixel_index];
      float I3 = LF[2 * width * height + pixel_index];
	  
      // 3d coordinates of point (in LF coords)
      float px = sx*pixel_pitch - pixel_pitch*0.5;
      float py = sy*pixel_pitch - pixel_pitch*0.5;
      float pz = refocused_distance;
    
      // transformation to virtual camera coordinates
      float local_px = transformation[0]*px +
                       transformation[4]*py +
                       transformation[8]*pz + 
                       transformation[12];
      float local_py = transformation[1]*px +
                       transformation[5]*py +
                       transformation[9]*pz +
                       transformation[13];
      float local_pz = transformation[2]*px +
                       transformation[6]*py +
                       transformation[10]*pz +
                       transformation[14];

      /* project onto virtual image plane */
      if (local_pz != 0.0) {
        float pixel_x = (local_px * fx + cx * local_pz)/local_pz;
        float pixel_y = (local_py * fy + cy * local_pz)/local_pz;
      
        int cpx = floor(pixel_x + 0.5);
        int cpy = floor(pixel_y + 0.5);  
          
        for (int i = 0; i < 2*rad + 1; ++i) {
          int cur_x = cpx - rad + i;
          if ( cur_x < 0 || cur_x >= result_width )
            continue;

          for (int j = 0; j < 2*rad + 1; ++j) {
            int cur_y = cpy - rad + j;
           
            if ( cur_y >= 0 && cur_y < result_height ) {
              float wi = kernel[ i * (2*rad + 1) + j ];
              weight[ cur_x * result_height + cur_y ] += wi*confidence;
              result[ cur_x * result_height + cur_y ] += I1*wi*confidence;
              result[ result_width * result_height + cur_x * result_height + cur_y] += I2*wi*confidence;
              result[ 2 * result_width * result_height + cur_x * result_height + cur_y] += I3*wi*confidence;
              result_dist[ cur_x * result_height + cur_y ] += local_pz*wi*confidence;
            }
          }
        }          
      }
    }
  }

  // divide by weight
  for ( int w = 0; w < result_width; ++w) {
    for (int h = 0; h < result_height; ++h) {
      float wi = weight[ w * result_height + h ];
      if (wi > 0.0) {
        result[ w * result_height + h ] /= wi;
        result[ result_width * result_height + w * result_height + h] /= wi;
        result[ 2 * result_width * result_height + w * result_height + h] /= wi;
		    result_dist[ w * result_height + h ] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // [result, result_distance] = render_view_from_LF(LF, u, v, center_distance, center_index, kernel, mla_to_sensor_dist, pixel_pitch, fx, fy, cx, cy, transformation, result_height, result_width, white_img)
  
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get radius of kernel
  int rad = (mxGetM(prhs[5]) - 1) / 2;

  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[1]);
  float *v = (float *) mxGetData(prhs[2]);
  
  // get center_distance map
  float *center_distance = (float *) mxGetData(prhs[3]);
    
  // get center index map
  int *center_index = (int *)mxGetData(prhs[4]);
  
  // get kernel array
  float *kernel = (float *) mxGetData(prhs[5]);

  // distance from MLA to sensor
  float mu = *((float *)mxGetData(prhs[6]));
  
  // pixel pitch
  float pixel_pitch = *((float *)mxGetData(prhs[7]));
  
  // focal lengths
  float fx = *((float *)mxGetData(prhs[8]));
  float fy = *((float *)mxGetData(prhs[9]));
  
  // center
  float cx = *((float *)mxGetData(prhs[10]));
  float cy = *((float *)mxGetData(prhs[11]));
  
  // transformation
  float *transformation = (float *) mxGetData(prhs[12]);
  
  // result image dimension
  int result_height = *((int *)mxGetData(prhs[13]));
  int result_width = *((int *)mxGetData(prhs[14]));

  // get white image 
  float *white_img = (float *)mxGetData(prhs[15]);
  
  // allocate output
  int result_dims[3] = {result_height, result_width, 3};
  plhs[0] = mxCreateNumericArray(3, result_dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  
  plhs[1] = mxCreateNumericMatrix(result_height, result_width, mxSINGLE_CLASS, mxREAL);
  float *result_dist = (float *) mxGetData(plhs[1]);
  
  // rendering
  render(LF, white_img, center_distance, transformation, center_index, kernel, 
        u, v, result, result_dist, mu, rad, fx, fy, cx, cy,
        pixel_pitch, height, width, result_height, result_width);
}