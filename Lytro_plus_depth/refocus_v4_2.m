% splat each ray to corresponding distance (depth) + weight ray using white image
% return also a distance (depth) map 
clear all
clc
close all


CALIBRATION_DIR = './Calibration/Dec23/';
DEPTH_DIR = '../Data/Depth/Dec23_Depth_Data/';
LF_DIR = '../Data/Images/Dec23_LF_Data/';
RESULTS_DIR = './Results/Feb19/';
JOINT_CALIB_DATA_FILE = 'Calib_Results_stereo.mat';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 16;

% Center-view subaperture image
LF_SUB_FILE = ['CI_IMG_', num2str(img_no, '%04i'), '.bmp'];

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

% Load input files
subaperture_img = imread([LF_DIR, LF_SUB_FILE]);
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth_frame);

% denoising depth with BF
filter_width = 3;
sigma_s = 1;
sigma_r = 10;
filtered_depth = BF_C(single(depth), int32(filter_width), ...
                      single(sigma_s), single(sigma_r));
% projected depth map to subaperture image
disp('Projecting depth map to light field camera view ...');
tic
projected_depth = project_depth_to_lytro(filtered_depth, CALIBRATION_DIR, ...
                                  JOINT_CALIB_DATA_FILE, subaperture_img);
toc

% fill in invalid depth value
projected_depth = hole_filling(projected_depth, 3);


figure; 
subplot(1,2,1);
imagesc(subaperture_img);
title('Center-view subaperture image');
axis image;
axis off;

subplot(1,2,2);
imagesc(projected_depth); 
title('Projected depth map');
colormap gray;
axis image;
axis off;

%
[xx, yy] = meshgrid(1:328, 1:328);
figure;
mesh(xx, yy, -projected_depth);
% keyboard;


% map depth to distance

disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(projected_depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);

% get depth and distance (from mla) corresponding to each microlens center
load([CALIBRATION_DIR, 'microlens_center_list.mat']);
% load([CALIBRATION_DIR, 'center.mat']);
% center_height = height;
% center_width = width;
% center_list = [centerX(:) centerY(:)]';
disp('Getting depth and distance corresponding to each microlens center ...');
tic
[center_depth, center_distance] = get_distance_and_depth_at_centers(center_list, 5, projected_depth, distance_map);
% [center_depth, center_distance] = get_distance_and_depth_at_centers2(center_height, center_width, projected_depth, distance_map);
toc


% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
%     [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    [cx, cy, u, v, center_index] = generate_maps2(centerX, centerY, center_height, center_width, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end


% refocus by splatting
WHITE_DIR = './Calibration/White_Images/zoom_490/';

% load white image
% white_img = imread([WHITE_DIR, 'white_image.tif']);
white_img = LFReadRaw([WHITE_DIR, 'white_image.RAW'], '12bit');

white_img = double(white_img);

% normalize
white_img = white_img./max(white_img(:));



disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

% kernel for splatting
rad = 5;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
% kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
% sigma = 5;
% sigma2 = sigma*sigma;
% kernel = exp(-(dxx.^2 + dyy.^2)./(2*sigma2));

kernel(kernel < 0) = 0.0;

mla_to_sensor_dist = 25e-3;
size(processed_LF)
disp('Splatting ...');
tic
[result, result_depth, result_dist] = splat_w_depth_v4(single(processed_LF), single(u), single(v), ...
                       single(center_distance), single(center_depth), int32(center_index), ...
                       single(kernel), single(mla_to_sensor_dist), single(white_img));
toc

final_result = uint16(result*double(intmax('uint16')));
figure;
imshow(final_result); title('Refocused result');
axis image;
axis off;

figure;
imagesc(result_dist);
title('Distance map');
axis image;
axis off;

figure;
imagesc(result_dist);
title('Depth map');
axis image;
axis off;

imwrite(final_result, [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_rad_', num2str(rad),'_v42.png']);
% imwrite(result_depth, [RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_rad_', num2str(rad),'_v4_depth_map.png']);

save([RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_rad_', num2str(rad),'_v42_depth_map.mat'], 'result_depth')
save([RESULTS_DIR, RAW_LF_FILE(1:end-4), '_refocused_rad_', num2str(rad),'_v42_distance_map.mat'], 'result_dist')
