// NOTE: for matlab, arrays are stored in column major
#include "mex.h"
//C includes
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <iostream>

#define _INDEX2(row, col, height, width) ((col)*(height) + (row))
	
void BF(float* input, float* output, int height, int width, 
		int w, float sigma_s, float sigma_r) {
		
	// radius
	int rad = (w-1)/2;
	
	for (int col=0; col < width; col++) {
		for (int row=0; row < height; row++) {
			float num = 0.0;  // accumulated data
			float den = 0.0;  // accumulated weigth
			
			int start_x = (col>=rad)?(col-rad):0;
			int end_x = (col<width-rad)?(col+rad):(width-1);
			int start_y = (row>=rad)?(row-rad):0;
			int end_y = (row<height-rad)?(row+rad):(height-1);
			
			for (int i=start_x; i<=end_x; i++) {
				for (int j=start_y; j<end_y; j++) {
					float gs = exp(-0.5*((i-col)*(i-col) + (j-row)*(j-row))/(sigma_s*sigma_s));
					float dI = input[_INDEX2(row,col,height,width)] - input[_INDEX2(j,i,height,width)];
					float gr = exp(-0.5*dI*dI/(sigma_r*sigma_r));
					
					num = num + gs*gr*input[_INDEX2(j,i,height, width)];
					den = den + gs*gr;					
				}
			}
			
			output[_INDEX2(row,col,height,width)] = num/den;		
		}
	}
		
}		

                 
	
// Interface
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// get dimensions
	const int* dims;
	int number_of_dims;
	
	number_of_dims = mxGetNumberOfDimensions(prhs[0]);
	dims = mxGetDimensions(prhs[0]);

	int height = dims[0]; // height of input video
	int width = dims[1]; // width of input video
		
	// get input
    float* input = (float*)mxGetData(prhs[0]);// input data
	
	int w = *((int*)mxGetData(prhs[1]));   // width
	float sigma_s = *((float*)mxGetData(prhs[2]));  // spatial smoothing param
	float sigma_r = *((float*)mxGetData(prhs[3])); // range smoothing params
	

	
	// Debug output

    mexPrintf("height = %d\n", height);
	mexPrintf("width = %d\n", width);
		    
    mexPrintf("filter width = %d\n", w);
	mexPrintf("spatial sigma_s = %f\n", sigma_s);	
	mexPrintf("range sigma_r = %f\n", sigma_r);
	
     // allocate output array 
    plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);  // Z
    float* output = (float*)mxGetData(plhs[0]); // pointer to the returned matrix z   
	

	// call function here
	
	BF(input, output, height, width, w, sigma_s, sigma_r);
    										   
}

	
