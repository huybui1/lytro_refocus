function dist = interpolate_dist(x, y, distance_map)
% get depth at point (x, y) by interpolation

[height, width] = size(distance_map);
x0 = floor(x);
y0 = floor(y);
x1 = x0 + 1;
y1 = y0 + 1;

if (x0 > 0 && x0 <= width &&...
    y0 > 0 && y0 <= height)
   d00 = distance_map(y0, x0);
else
   d00 = 0;
end

if (x0 > 0 && x0 <= width && ...
    y1 > 0 && y1 <= height)
    d01 = distance_map(y1, x0);
else
    d01 = 0;
end

if (x1 > 0 && x1 <= width && ...
    y0 > 0 && y0 <= height)
    d10 = distance_map(y0, x1);
else
    d10 = 0;
end

if (x1 > 0 && x1 <= width && ...
    y1 > 0 && y1 <= height)
    d11 = distance_map(y1, x1);
else
    d11 = 0;
end

d0 = d01*(y - y0) + d00*(y1 - y);
d1 = d11*(y - y0) + d10*(y1 - y);
dist = d0*(x1 - x) + d1*(x - x0);

    