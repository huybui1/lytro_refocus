% Extract depth frame (Primesense)

clear all;
close all;

DIR = '../Data/Depth/Mar17_Depth_Data/';

for img_no = 1:31
    INFILE = ['IMG_', num2str(img_no,'%04i'), '.h5'];
    OUTFILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];

    if ~exist([DIR, INFILE], 'file')
        continue;
    end
    
    depth_height = hdf5read([DIR, INFILE], 'INFORMATION/DEPTH_SENSOR/HEIGHT');
    depth_width = hdf5read([DIR, INFILE], 'INFORMATION/DEPTH_SENSOR/WIDTH');

    depth_frame = zeros(depth_height, depth_width);
    start_frame = 0;
    end_frame = 9;
    for k = start_frame:end_frame
      frame = hdf5read([DIR, INFILE], ['FRAME',num2str(k,'%04i'),'/DEPTH'])';
      depth_frame = depth_frame + double(frame);
    end

    num_frames = end_frame - start_frame + 1;
    depth_frame = depth_frame./num_frames;


    figure(100);
    imagesc(depth_frame); title([' Depth frame ', num2str(img_no)]);
    axis image;
    axis off;

    save([DIR OUTFILE], 'depth_frame');
    pause
end
