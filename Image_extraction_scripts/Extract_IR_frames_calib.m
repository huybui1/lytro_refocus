% Extract IR frame (Primesense)

clear all;
close all;

DIR = '../Data/Depth/Mar17_Depth_Calib/';
INFILE = 'IMG_0021.h5';
OUTFILE = ['IR_', INFILE(5:end-3), '.tif'];

ir_height = hdf5read([DIR, INFILE], 'INFORMATION/IR_SENSOR/HEIGHT');
ir_width = hdf5read([DIR, INFILE], 'INFORMATION/IR_SENSOR/WIDTH');

start_frame = 225;
end_frame = start_frame + 29;
for k = start_frame:end_frame
  ir_frame = hdf5read([DIR, INFILE], ['FRAME',num2str(k,'%04i'),'/IR'])';
  
  
  figure(100);
  imagesc(ir_frame); colormap gray;
  axis image;
  axis off;
  title(['Frame ', num2str(k)]);
  
  extract = input('Extract this frame ? y/n [n]: ', 's');
  
  if (strcmp(extract, 'y') || strcmp(extract, 'Y'))
      disp('Extracting frame ...');
      ir_frame = double(ir_frame);
      ir_frame = uint8(ir_frame./max(ir_frame(:))*255);
      imwrite(ir_frame, [DIR, OUTFILE]);
      
      disp('Done.');
      cont = input('Continue ? y/n [y]: ', 's');
      if (strcmp(cont, 'N') || strcmp(cont, 'n'))
          break;
      end
  end
end
