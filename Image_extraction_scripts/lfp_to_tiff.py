import os
import sys
import subprocess

try:
	DIR = sys.argv[1]
	print(DIR)
except:
	DIR = ".\\"
	
for filename in os.listdir(DIR):
    if (filename.endswith(".lfp") | filename.endswith(".LFP")):
		subprocess.call(["lfpsplitter", DIR + filename])

for filename in os.listdir(DIR):
    # print(filename)
    if (filename.endswith(".raw")):
        subprocess.call(["raw2tiff", [" -w 3280", " -l 3280",  " -d short"], DIR +  filename, DIR + os.path.splitext(filename)[0] + ".tiff"])
        os.remove(DIR + filename)

json_filelist = [ f for f in os.listdir(DIR) if f.endswith(".json") ]
for f in json_filelist:
    os.remove(DIR + f)

