clear all;
close all;

FILENAME = 'Data.h5';
DIR = './';
num_frames = hdf5read([DIR, FILENAME], 'INFO/COUNT');

for i = 0:1:num_frames-1
   % read in from .h5 file
   frame = hdf5read([DIR, FILENAME],['FRAME',num2str(i,'%04i'),'/XYZ']);
   depth = squeeze(frame(3,:,:));
   figure(100);
   imagesc(depth);
   axis image; axis off;
   title(sprintf('Depth Frame %i',i)); 

   ir_frame = hdf5read([DIR, FILENAME],['FRAME',num2str(i,'%04i'),'/Intensity'])';
   figure(200);
   imagesc(ir_frame);
   axis image; axis off; colormap gray;
   title(sprintf('IR Frame %i',i));
   pause (0.3)
   keyboard
end







