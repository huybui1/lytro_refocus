clear all;
close all;

FILENAME = 'IMG_0001.h5';
DIR = '../Data/Depth/Dec9_Depth_Data/';

num_frames = 300;
color_height = hdf5read([DIR, FILENAME], 'INFORMATION/COLOR_SENSOR/HEIGHT');
color_width = hdf5read([DIR, FILENAME], 'INFORMATION/COLOR_SENSOR/WIDTH');

for i = 0:3:num_frames-1
   % read in from .h5 file
   depth_frame = hdf5read([DIR, FILENAME],['FRAME',num2str(i,'%04i'),'/DEPTH'])';
   figure(100);
   imagesc(abs(depth_frame));
   axis image; axis off;
   title(sprintf('Depth Frame %i',i)); 

   color_frame = zeros(color_height, color_width, 3);
   color_frame_temp = hdf5read([DIR, FILENAME],['FRAME',num2str(i,'%04i'),'/COLOR']);
   color_frame(:,:,1) = squeeze(color_frame_temp(1,:,:))';
   color_frame(:,:,2) = squeeze(color_frame_temp(2,:,:))';
   color_frame(:,:,3) = squeeze(color_frame_temp(3,:,:))';
   color_frame = uint8(color_frame);
   
   figure(200);
   imagesc(color_frame);
   axis image; axis off; colormap gray;
   title(sprintf('Color Frame %i',i));
   pause (0.3)
end







