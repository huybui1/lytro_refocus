% Extract Color frame (Primesense)

clear all;
close all;

DIR = '../Data/Depth/Mar17_Depth_Data/';
for img_no = 2:31
    
    INFILE = ['IMG_', num2str(img_no,'%04i'), '.h5'];
    OUTFILE = ['IMG_', num2str(img_no, '%04i'), '_Color.tif'];

    if (~exist([DIR, INFILE], 'file'))
        continue;
    end
    color_height = hdf5read([DIR, INFILE], 'INFORMATION/COLOR_SENSOR/HEIGHT');
    color_width = hdf5read([DIR, INFILE], 'INFORMATION/COLOR_SENSOR/WIDTH');

    start_frame = 0;
    end_frame = start_frame + 49;
    for k = start_frame:end_frame
      frame = hdf5read([DIR, INFILE], ['FRAME',num2str(k,'%04i'),'/COLOR']);
      color_frame = zeros(color_height, color_width, 3);
      color_frame(:,:,1) = squeeze(frame(1,:,:))';
      color_frame(:,:,2) = squeeze(frame(2,:,:))';
      color_frame(:,:,3) = squeeze(frame(3,:,:))';
      color_frame = uint8(color_frame);

      figure(100);
      imshow(color_frame);
      axis image;
      axis off;
      title(['Frame ', num2str(k)]);

      extract = input('Extract this frame ? y/n [n]: ', 's');

      if (strcmp(extract, 'y') || strcmp(extract, 'Y'))
          disp('Extracting frame ...');
          imwrite(color_frame, [DIR, OUTFILE]);

          disp('Done.');
          cont = input('Continue ? y/n [y]: ', 's');
          if (strcmp(cont, 'N') || strcmp(cont, 'n'))
              break;
          end
      end
    end
end