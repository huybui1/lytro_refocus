% Extract depth frame (Primesense)

clear all;
close all;

DIR = '../Data/Depth/Mar17_Depth_Calib/';
INFILE = 'IMG_0021.h5';
OUTFILE = ['Depth_', INFILE(5:end-3), '.mat'];

depth_height = hdf5read([DIR, INFILE], 'INFORMATION/DEPTH_SENSOR/HEIGHT');
depth_width = hdf5read([DIR, INFILE], 'INFORMATION/DEPTH_SENSOR/WIDTH');

depth_frame = zeros(depth_height, depth_width);
start_frame = 90;
end_frame = start_frame + 9;
for k = start_frame:end_frame
  frame = hdf5read([DIR, INFILE], ['FRAME',num2str(k,'%04i'),'/DEPTH'])';
  depth_frame = depth_frame + double(frame);
end

num_frames = end_frame - start_frame + 1;
depth_frame = depth_frame./num_frames;

  
figure(100);
imagesc(depth_frame); title([' Depth']);
axis image;
axis off;

save([DIR OUTFILE], 'depth_frame');


