DIR = '../Data/Images/Mar17_LF_Data/';
for i = 1:31
    filename = [DIR, 'IMG_', num2str(i, '%04i'), '_imageRef0.tiff'];
%     filename = [DIR, 'IMG_', num2str(i, '%04i'), '.tif'];
    if exist(filename, 'file')
        disp(['Processing ', filename, '...']);
        A = im2double(imread(filename));
        A = uint8(A.*double(intmax('uint8')));
        A_d = demosaic(A,'bggr');
        imwrite(A_d,[DIR, 'IMG_',num2str(i, '%04i'), '.png']);
    end
end
%  