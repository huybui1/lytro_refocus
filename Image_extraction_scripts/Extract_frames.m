% Extract depth and IR frame (Primesense)

clear all;
close all;

DIR = './Nov_27_w_IR_depth/';
num_files = 15;
num_frames_to_avg = 10;
max_depth = 4000;
min_depth = 100;

for i = 1:num_files
%   H5_FILENAME = ['depth_', num2str(i,'%04i'),'.h5'];
  H5_FILENAME = ['F', num2str(i, '%04i'),'.h5'];
  FRAME_FILENAME = ['Frame_', num2str(i, '%04i'), '.mat'];
  IR_FILENAME = ['IR_', num2str(i, '%04i'), '.png'];
  
  depth_height = hdf5read([DIR, H5_FILENAME], 'INFORMATION/DEPTH_SENSOR/HEIGHT');
  depth_width = hdf5read([DIR, H5_FILENAME], 'INFORMATION/DEPTH_SENSOR/WIDTH');
  ir_height = hdf5read([DIR, H5_FILENAME], 'INFORMATION/IR_SENSOR/HEIGHT');
  ir_width = hdf5read([DIR, H5_FILENAME], 'INFORMATION/IR_SENSOR/WIDTH');
  
  depth_frame = zeros(depth_height, depth_width);
  ir_frame = zeros(ir_height, ir_width);
  for k = 1:num_frames_to_avg
      frame = hdf5read([DIR, H5_FILENAME], ['FRAME',num2str(k,'%04i'),'/DEPTH'])';
      frame(frame > max_depth) = 0;
      frame(frame < min_depth) = 0;
      depth_frame = depth_frame + double(frame);
      
      frame = hdf5read([DIR, H5_FILENAME], ['FRAME',num2str(k,'%04i'),'/IR'])';
      ir_frame = ir_frame + double(frame);
  end
  depth_frame = depth_frame./num_frames_to_avg;
  ir_frame = ir_frame./num_frames_to_avg;
  
  
  figure(100);
  imagesc(depth_frame); title([H5_FILENAME, ' Depth']);
  axis image;
  axis off;
  
  figure(101);
  imagesc(ir_frame); title([H5_FILENAME, ' IR']);
  axis image;
  axis off;
  
  ir_frame = ir_frame./max(ir_frame(:));
  ir_frame = uint8(ir_frame*double(intmax('uint8')));

  save([DIR FRAME_FILENAME], 'depth_frame', 'ir_frame');
  imwrite(ir_frame, [DIR IR_FILENAME]);
end


