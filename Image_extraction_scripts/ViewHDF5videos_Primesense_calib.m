clear all;
close all;

FILENAME = 'IMG_0022.h5';
DIR = '../Data/Depth/Mar17_Depth_Calib/';
num_frames = 600;

for i = 0:3:num_frames-1
   % read in from .h5 file
   depth_frame = hdf5read([DIR, FILENAME],['FRAME',num2str(i,'%04i'),'/DEPTH'])';
   figure(100);
   subplot(1,2,1);
   imagesc(abs(depth_frame));
   axis image; axis off;
   title(sprintf('Depth Frame %i',i)); 

   ir_frame = hdf5read([DIR, FILENAME],['FRAME',num2str(i,'%04i'),'/IR'])';
   figure(100);
   subplot(1,2,2);
   imagesc(ir_frame);
   axis image; axis off; colormap gray;
   title(sprintf('IR Frame %i',i));
   pause (0.3)
%    keyboard
end







