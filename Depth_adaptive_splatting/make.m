% compile
mex splat.cpp % refocus to a specified distance using splatting
mex computeDSI.cpp % compute disparity space image from raw light field
                   % and the refocused image
mex estimate_depth.cpp % estimate depth from array of disparity space images
mex depth_adaptive_splat.cpp % refocus using the estimated depth map