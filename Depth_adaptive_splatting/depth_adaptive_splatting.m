 % v2 - read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau

clear all
clc
close all

create_video = 0;
write_images_to_disk = 0;

% image file
IMG_NAME = 'IMG_0024.lfp';
DEPTH_NAME = ['Depth_', IMG_NAME(5:end - 4), '.mat'];
IMG_DIR = '../Data/Images/Dec23_LF_Data/';
CALIBRATION_DIR = '../Lytro_plus_depth/Calibration/Dec23/';
OUTPUT_DIR = './Results/June_8/';
load([CALIBRATION_DIR, 'microlens_center_list.mat']);

% load white image
white_img = LFReadRaw([CALIBRATION_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);
% normalize
white_img = white_img./max(white_img(:));

processed_LF = preprocess_lightfield(IMG_DIR, CALIBRATION_DIR, IMG_NAME);
img_height = size(processed_LF, 1);
img_width = size(processed_LF, 2);

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];
f = 25e-6;  % distance from mla to sensor

if (~exist([CALIBRATION_DIR, MAP_FILE], 'file'))
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end


if ~exist([OUTPUT_DIR, DEPTH_NAME], 'file')
    % kernel
    rad = 5;
    dx = -rad:rad;
    dy = -rad:rad;
    [dxx, dyy] = meshgrid(dx, dy);
    % kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
    kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
    kernel(kernel < 0) = 0.0;

    
%     distance_array =10e-6:10e-6:100e-6;
    distance_array = 1e-6:2e-6:10e-6;
    DSIs = zeros(img_height, img_width, length(distance_array));



    % compute disparity images
    for F_index = 1:length(distance_array)
        F = distance_array(F_index);
        disp(['Refocused Distance: ', num2str(F) , ' (m).']);

        % create refocused image
        tic
        refocused_img = splat(single(processed_LF), single(u), single(v), ...
                       single(kernel), single(F), single(f), single(white_img));
        toc

        % create disparity space image
        disp('Computing disparity space image (DSI) ...');
        tic
        disparity_img = computeDSI(single(processed_LF), ...
                                   single(refocused_img), ...
                                   single(u), single(v), single(kernel), ...
                                   single(F), single(f));
        toc                           
        figure;
        imagesc(refocused_img);
        axis image; axis off;
        pause
        
        DSIs(:,:,F_index) = disparity_img;
    end %for F

    % estimate the depth map
    [depth, cost] = estimate_depth(single(DSIs), single(distance_array), ...
                           single(u), single(v), single(f));

    figure;
    imagesc(depth);
    axis image; axis off;
    title('Estimated depth map');

    save([OUTPUT_DIR, DEPTH_NAME], 'depth');
    save([OUTPUT_DIR, 'DSIs.mat'], 'DSIs');
    clear DSIs;
else
    load([OUTPUT_DIR, DEPTH_NAME]);
end
    
% depth - adaptive refocus
b_min = 2;
n = 3.5;

p0 = 15;
p1 = 20;
p2 = 100;

if (create_video == 1)
    vidObj = VideoWriter([OUTPUT_DIR, IMG_NAME(1:end-4),'.avi'], 'Uncompressed AVI');
    vidObj.FrameRate = 2;
    vidObj.VideoCompressionMethod
    open(vidObj);

    fig100 = figure(100);
    set(fig100, 'Visible', 'off');
end


% refocus_distance = 36e-6;
for refocus_distance = 11.5e-6
    D_alpha = 1 + refocus_distance / f;
    w_alpha = p0 / ( 1 + exp(- (D_alpha - p2)/p1))

    result = depth_adaptive_splat(single(processed_LF), single(depth), ...
                                  single(u), single(v), ...
                                  single(refocus_distance), ...
                                  single(f), ...
                                  int32(b_min), ...
                                  single(n), ...
                                  single(w_alpha), ...
                                  single(white_img));

%     figure(100);
%     imagesc(result);
%     axis image; axis off;
%     title(['Depth - adaptive refocused image at ', num2str(refocus_distance*1e+6), ' nm']);

    if (create_video == 1)
        figure(100); 
        currFrame = getframe;
        writeVideo(vidObj, currFrame);
    end     
    

    if (write_images_to_disk == 1)
        if (refocus_distance >= 0)
            imwrite(result, [OUTPUT_DIR, IMG_NAME(1:end-4), '_' , num2str(refocus_distance*1e+6),'um_rad_', num2str(rad),'.png'] );
        else
            imwrite(result, [OUTPUT_DIR, IMG_NAME(1:end-4), '__' , num2str(abs(refocus_distance)*1e+6),'um_rad_', num2str(rad),'.png'] );
        end
    end
end

if (create_video == 1)
    close(vidObj);
end

addpath('..\DownloadedCodes\LightFieldCalib_Release\');
center_depth = CenterImage(depth, center_list, center_connection, 5);
figure; 
imagesc(center_depth);