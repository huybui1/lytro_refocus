% v2 - read LFP file directly
% using LFToolbox v 0.3 by Donald G. Dansereau
addpath('..\DownloadedCodes\LightFieldCalib_Release\');


clear all
clc
close all

create_video = 1;
write_images_to_disk = 1;

% image file
IMG_NAME = 'IMG_0017.lfp';
DEPTH_NAME = ['Depth_', IMG_NAME(5:end - 4), '.mat'];
IMG_DIR = '../Data/Images/Dec23_LF_Data/';
CALIBRATION_DIR = '../Lytro_plus_depth/Calibration/Dec23/'
OUTPUT_DIR = './Results/June_8/';
load([CALIBRATION_DIR, 'microlens_center_list.mat']);

% load white image
white_img = LFReadRaw([CALIBRATION_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);
% normalize
white_img = white_img./max(white_img(:));

processed_LF = preprocess_lightfield(IMG_DIR, CALIBRATION_DIR, IMG_NAME);
img_height = size(processed_LF, 1);
img_width = size(processed_LF, 2);

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];
f = 25e-6;  % distance from mla to sensor

if (~exist([CALIBRATION_DIR, MAP_FILE], 'file'))
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end


if ~exist([OUTPUT_DIR, DEPTH_NAME], 'file')
    % kernel
    rad = 20;
    dx = -rad:rad;
    dy = -rad:rad;
    [dxx, dyy] = meshgrid(dx, dy);
    % kernel = ones(2*rad+1, 2*rad+1) - sqrt(dxx.^2 + dyy.^2)/rad;
    kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
    kernel(kernel < 0) = 0.0;

    
    distance_array =0e-6:5e-6:100e-6;

    % compute disparity images
    DISPARITY_DIR = [OUTPUT_DIR, '/Temp/'];
    DEBUG_DIR = [OUTPUT_DIR, '/Debug/'];
    for F_index = 1:length(distance_array)
        DISPARITY_NAME = ['DSI_', IMG_NAME(1:end - 4),'_', num2str(F_index, '%04i'), '.mat'];
        if ~exist([DISPARITY_DIR, DISPARITY_NAME], 'file')
            F = distance_array(F_index);
            disp(['Refocused Distance: ', num2str(F) , ' (m).']);

            % create refocused image
            tic
            refocused_img = splat(single(processed_LF), single(u), single(v), ...
                           single(kernel), single(F), single(f), single(white_img));
            toc
            
%             figure;
%             imagesc(refocused_img);
%             axis image; axis off;
%             title(['Refocused image at distance ', num2str(F)]);
            
            
            center_refocused_img = CenterImage(refocused_img, center_list, center_connection, 5);
%             figure; 
%             imagesc(center_refocused_img);
%             title(['Center refocused image at distance ', num2str(F)]);
%             pause;            
            imwrite(center_refocused_img, [DEBUG_DIR, IMG_NAME(1:end-4), '_center_refocused_at_' , num2str(F*1e+6),'um.png'] );
            
            
            % create disparity space image
            disp('Computing disparity space image (DSI) ...');
            tic
            disparity_img = computeDSI(single(processed_LF), ...
                                       single(refocused_img), ...
                                       single(u), single(v), single(kernel), ...
                                       single(F), single(f));
            toc   
            
            
            center_disparity_img = CenterImage(disparity_img, center_list, center_connection, 5);
            figure(1000); 
            imagesc(center_disparity_img);
            title(['Center disparity image at distance ', num2str(F)]);
%             pause;                
            imwrite(center_disparity_img, [DEBUG_DIR, IMG_NAME(1:end-4), '_center_disparity_at_' , num2str(F*1e+6),'um.png'] );
            save([DEBUG_DIR, IMG_NAME(1:end-4), '_center_disparity_at_' , num2str(F*1e+6),'um.mat'], 'center_disparity_img' );
            
            disparity_dist = F; % distance corresponds to this disparity map
            % save([DISPARITY_DIR, DISPARITY_NAME], 'disparity_img', 'disparity_dist');
            save_disparity_map(DISPARITY_DIR, DISPARITY_NAME, disparity_img, disparity_dist);
        end
    end %for F

    % estimate the depth map
    depth = zeros(img_height, img_width); % depth map to estimate
%     min_disparity_map = - ones(img_height, img_width); % min disparity for each pixel
    min_disparity_map = 1e+6*ones(img_height, img_width);
    
%     keyboard
    
    for index = 1:length(distance_array)
        DISPARITY_NAME = ['DSI_', IMG_NAME(1:end - 4),'_', num2str(index, '%04i'), '.mat'];
        load([DISPARITY_DIR, DISPARITY_NAME]);
%         keyboard;   
  
       tic
       [depth, min_disparity_map] = update_depth(single(depth), ...
                                                 single(min_disparity_map), ...
                                                 single(disparity_img), ...
                                                 single(disparity_dist), ...
                                                 single(u), single(v), ...
                                                 single(f));
        
                                             
        center_depth = CenterImage(depth, center_list, center_connection, 5);
        toc        
        figure(1001);
        imagesc(center_depth);
        title(['Center depth at disparity ', num2str(disparity_dist)]);
%         pause;
        save([DEBUG_DIR, IMG_NAME(1:end-4), '_center_depth_after_disparity_img_' , num2str(disparity_dist*1e+6),'um.mat'], 'center_depth' );
                                             
        clear disparity_img;
        figure(200);
        imagesc(depth);
        axis image; axis off;
        title('Estimated depth map');
        
        
    end
    figure;
    imagesc(depth);
    axis image; axis off;
    title('Estimated depth map');

    save([OUTPUT_DIR, DEPTH_NAME], 'depth');
    clear DSIs;
else
    load([OUTPUT_DIR, DEPTH_NAME]);
end
    


%% depth - adaptive refocus
b_min = 2;
n = 3.5;

p0 = 15;
p1 = 20;
p2 = 100;


if (create_video == 1)
    vidObj = VideoWriter([OUTPUT_DIR, IMG_NAME(1:end-4),'.avi'], 'Uncompressed AVI');
    vidObj.FrameRate = 2;
%     vidObj.VideoCompressionMethod
    open(vidObj);

    fig100 = figure(100);
    set(fig100, 'Visible', 'off');
end
% 
% 
% % refocus_distance = 36e-6;
% for refocus_distance = 5e-6:10e-6:100e-6
%     D_alpha = 1 + refocus_distance / f;
%     w_alpha = p0 / ( 1 + exp(- (D_alpha - p2)/p1))
%     tic
%     result = depth_adaptive_splat(single(processed_LF), single(depth), ...
%                                   single(u), single(v), ...
%                                   single(refocus_distance), ...
%                                   single(f), ...
%                                   int32(b_min), ...
%                                   single(n), ...
%                                   single(w_alpha), ...
%                                   single(white_img));
%     toc
%     figure(100);
%     imagesc(result);
%     axis image; axis off;
%     title(['Depth - adaptive refocused image at ', num2str(refocus_distance*1e+6), ' nm']);
% 
% %     if (create_video == 1)
% %         figure(100); 
% %         currFrame = getframe;
% %         writeVideo(vidObj, currFrame);
% %     end     
%     
% 
%     if (write_images_to_disk == 1)
%         if (refocus_distance >= 0)
%             imwrite(result, [OUTPUT_DIR, IMG_NAME(1:end-4), '_' , num2str(refocus_distance*1e+6),'um.png'] );
%         else
%             imwrite(result, [OUTPUT_DIR, IMG_NAME(1:end-4), '__' , num2str(abs(refocus_distance)*1e+6),'um.png'] );
%         end
%     end
% end
% 
% if (create_video == 1)
%     close(vidObj);
% end