#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>
#include <limits>

// interpolate the input_image to get value at sx, sy
void interpolate(const float *input_image,  /* input image (height x width) */
                 float height, float width,
                 float sx, float sy,  /* location */
                 float* res /* ouput values */
                 ) {
  int img_size = height * width;
  float sx0 = floor(sx);
  float sx1 = sx0 + 1.0;
  float sy0 = floor(sy);
  float sy1 = sy0 + 1.0;

  int i;
  float I_00, I_01, I_10, I_11;

  // 00
  i = (int)sx0 * height + (int)sy0;
  if ( i > 0 && i < img_size)
    I_00 = input_image[i];
  else
    I_00 = 0.0;

  // 01
  i = (int)sx0 * height + (int) sy1;
  if ( i > 0 && i < img_size)
    I_01 = input_image[i];
  else
    I_01 = 0.0;

  // 10
  i = (int)sx1 * height + (int) sy0;
  if ( i > 0 && i < img_size)
    I_10 = input_image[i];
  else
    I_10 = 0.0;

  // 11
  i = (int)sx1 * height + (int) sy1;
  if ( i > 0 && i < img_size)
    I_11 = input_image[i];
  else
    I_11 = 0.0;

  // do interpolation
  float u = sx - sx0;
  float v = sy - sy0;
  
  if ((I_00 == -1.0) || (I_01 == -1.0) || (I_10 == -1.0) || (I_11 == -1.0))
    *res = -1.0;
  else
    *res  = (1 - u) * (1 - v) * I_00 +
          (1 - u) * (    v) * I_01 +
          (    u) * (1 - v) * I_10 +
          (    u) * (    v) * I_11;
}

// estimate depth map given the set of disparity image
void estimate_depth(const float *DSIs, /* disparity space images */
                    const float *distances, /* array of distances, each corresponds to a DSI */
                    float *depth,      /* estimated depth map */
                    float *cost,       /* the min disparity cost corresponding to depth */
                    const float *u,    /* precomputed u and v : local pixel coordinate */
                    const float *v,
                    int height, int width,
                    int num_DSIs, /* Number of disparity space images */
                    float f) /* MLA - Sensor distance */
{
  for (int w = 0; w < width; w++) {
    for (int h = 0; h < height; h++) {
      int index = w * height + h;
      float ui = u[index];
      float vi = v[index];
      float min_disp = std::numeric_limits<float>::max();
      float sx, sy; // splatting location
      float D;
      // splat to each distance and find the one
      // with min disparity value
      for (int d = 0; d < num_DSIs; d++) {
        D = 1.0f + distances[d]/f;
        sx = w - D * ui;
        sy = h - D * vi;        
      
        // interpolate corresponding disparity map
        float disp;
        interpolate(&DSIs[d*height*width], height, width, 
                    sx, sy, &disp);
        if (disp < min_disp) {
          min_disp = disp;
          depth[index] = distances[d];
          cost[index] = min_disp;
        }
      }      
    }
  }
}
          

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // depth = estimate_depth(DSIs, distances, u, v, mla_to_sensor_dist)

  // get height, width and number of Disparity Space Images (DSIs) 
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  int num_planes = dims[2];

  // get array of disparity images
  float *DSIs = (float *) mxGetData(prhs[0]);
  
  // get array of distances (dim = num_planes)
  float *distances = (float *) mxGetData(prhs[1]);

  // get u, v array
  float *u = (float *) mxGetData(prhs[2]);
  float *v = (float *) mxGetData(prhs[3]);

  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[4]));

  // allocate output

  // estimated depth map
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *depth_map = (float *) mxGetData(plhs[0]);

  plhs[1] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *cost = (float *) mxGetData(plhs[1]);
  // depth estimation
  estimate_depth( DSIs, distances, depth_map, cost,
                  u, v, height, width, num_planes, f);
}