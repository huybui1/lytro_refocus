// do splatting using depth
// + weight ray using white image intensity
// + depth_adaptive kernel
#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

void depth_adaptive_splat(const float *LF, /* input raw light field */
           const float *white_img,        
           const float *depth_map,
		       const float *u, const float *v, /* u and v array */
		       float *result,
		       int b_min,  /* kernel parameter: radius = b_min + |depth - ref_depth|^n + w(alpha) */
           float w_alpha,
           float n,
           float refocused_distance,
           float f, /* MLA - sensor distance */
		       int height, int width,
           bool add_jitter) {
  float *weight = (float *)mxMalloc(height*width*sizeof(float));
  float D = 1.0f + refocused_distance / f;
  
  // splatting 
  for (int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
	  int pixel_index = w * height + h;
      float depth = depth_map[pixel_index];
      float Di = 1.0 + depth / f;
      
      float sx = w - D*u[pixel_index];
      float sy = h - D*v[pixel_index];
      float ci = white_img[pixel_index];
      float I1 = LF[pixel_index];
      float I2 = LF[width * height + pixel_index];
      float I3 = LF[2 * width * height + pixel_index];
	  
      int cy  = floor(sy + 0.5f);
      int cx  = floor(sx + 0.5f);
      
      // determine kernel size      
      int rad = b_min + pow(abs(D - Di), n) + w_alpha;
      // if (rad > b_max)
      //    rad = b_max;
      
      for (int i = -rad; i <=rad; ++i) {
        int cur_x = cx + i;
        if ( cur_x < 0 || cur_x >= width )
          continue;

        for (int j = -rad; j <= rad; ++j) {
          int cur_y = cy + j;
           
          if ( cur_y >= 0 && cur_y < height ) {
//             float r = sqrt((float)(i*i + j*j));
            float r = (float)(i < j)?i:j;
            float wi = 1 - r/rad;
            if (wi < 0.0)
                wi = 0.0;
 
            weight[ cur_x * height + cur_y ] += wi*ci;
            result[ cur_x * height + cur_y ] += I1*wi*ci;
            result[ width * height + cur_x * height + cur_y] += I2*wi*ci;
            result[ 2 * width * height + cur_x * height + cur_y] += I3*wi*ci;
          }
        }
      }          
    }
  }

  // divide by weight
  for ( int w = 0; w < width; ++w) {
    for (int h = 0; h < height; ++h) {
      float wi = weight[ w * height + h ];
      if (wi > 0.0) {
        result[ w * height + h ] /= wi;
        result[ width * height + w * height + h] /= wi;
        result[ 2 * width * height + w * height + h] /= wi;
      }
    }
  }
   mxFree(weight);
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // result = depth_adaptive_splat(LF, depth_map, u, v, refocused_distance, 
  //            mla_to_sensor_dist, b_min, n, w_alpha, white_img)
  
  // get height and width
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];
  
  // get input light field image
  float *LF = (float *) mxGetData(prhs[0]);

  // get depth map
  float *depth_map = (float *) mxGetData(prhs[1]);
  
  // get u, v array
  float *u = (float *) mxGetData(prhs[2]);
  float *v = (float *) mxGetData(prhs[3]);
  
  // refocused distance
  float refocused_distance = *((float *) mxGetData(prhs[4]));
  
  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[5]));
  
  // kernel parameters
  int b_min = *((int *)mxGetData(prhs[6]));
  float n = *((float *)mxGetData(prhs[7]));
  float w_alpha = *((float *)mxGetData(prhs[8]));

  // get white image 
  float *white_img = (float *)mxGetData(prhs[9]);
  
  
  // allocate output
  plhs[0] = mxCreateNumericArray(3, dims, mxSINGLE_CLASS, mxREAL);
  float *result = (float *) mxGetData(plhs[0]);
  
  // splatting
  depth_adaptive_splat(LF, white_img, depth_map, 
      u, v, result, b_min, w_alpha, n, refocused_distance, f, height, width);
}