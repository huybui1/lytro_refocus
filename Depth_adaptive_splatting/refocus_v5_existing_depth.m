% splat each ray to corresponding distance (depth) + weight ray using white image
% return also a distance (depth) map 
clear all
clc
close all

addpath('../Lytro_plus_depth/');
CALIBRATION_DIR = '../Lytro_plus_depth/Calibration/Dec23/';
DEPTH_DIR = 'Results/June_8/';
LF_DIR = '../Data/Images/Dec23_LF_Data/';
RESULTS_DIR = './Results/June_8/';
LF_CALIB_DATA_FILE = 'IntParamLF.mat';

img_no = 24;

% Raw light-field image
RAW_LF_FILE = ['IMG_', num2str(img_no, '%04i'), '.lfp'];

% Depth map
DEPTH_FILE = ['Depth_', num2str(img_no, '%04i'), '.mat'];
load([DEPTH_DIR, DEPTH_FILE]);
depth = double(depth);


% map depth to distance
disp('Generate distance map from depth ...');
distance_map = map_depth_to_distance(depth, CALIBRATION_DIR, LF_CALIB_DATA_FILE);
figure; 
imagesc(distance_map);

% generate mapping from pixel to center index
img_height = 3280;
img_width = 3280;

MAP_FILE = ['maps_', num2str(img_height), '_', num2str(img_width),'.mat'];

if ~exist([CALIBRATION_DIR, MAP_FILE], 'file')
    disp('Generating neccessary maps ...');
    tic
    [cx, cy, u, v, center_index] = generate_maps(center_list, img_height, img_width, CALIBRATION_DIR);
    toc
else
    load([CALIBRATION_DIR, MAP_FILE]);
end

mla_to_sensor_dist = 25e-3;

% refocus by splatting
WHITE_DIR = '../Lytro_plus_depth/Calibration/White_Images/zoom_490/';

% load white image
white_img = LFReadRaw([WHITE_DIR, 'white_image.RAW'], '12bit');
white_img = double(white_img);
% normalize
white_img = white_img./max(white_img(:));



disp('Preprocessing light field image ...');
tic
processed_LF = preprocess_lightfield(LF_DIR, WHITE_DIR, RAW_LF_FILE);
toc

figure; imshow(processed_LF);
title('Preprocessed raw light field image');

% kernel for splatting
rad = 4;
dx = -rad:rad;
dy = -rad:rad;
[dxx, dyy] = meshgrid(dx, dy);
kernel = ones(2*rad+1, 2*rad+1) - min(abs(dxx), abs(dyy))/rad;
kernel(kernel < 0) = 0.0;


size(processed_LF)
disp('Splatting ...');
tic
[result, result_dist, num_rays] = splat_w_depth_v5(single(processed_LF), single(u), single(v), ...
                       single(distance_map), ...
                       single(kernel), single(mla_to_sensor_dist), single(white_img));
toc

final_result = uint16(result*double(intmax('uint16')));
figure;
imshow(final_result); title('Refocused result');
axis image;
axis off;

figure;
imagesc(result_dist);
title('Distance map');
axis image;
axis off;
