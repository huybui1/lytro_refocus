function out = color_correct(in, ColMatrix, ColBalance, Gamma)
  inSize = size(in);
  ndims = numel(inSize);
%   in = in.^Gamma;
  in = reshape(in, [prod(inSize(1:ndims-1)), 3]);
  temp = in * ColMatrix;
  temp = reshape(temp, [inSize(1:ndims-1), 3]);
  
%   % saturating
%   satlevel = [ColBalance(4), ColBalance(2), ColBalance(1)]*ColMatrix;
%   satlevel = min(satlevel);
  
%   temp = min(satlevel, max(0, temp));
%   temp  = temp./max(temp(:));
  temp = min(1, max(0, temp));
  
%   %% TEST
%   temp_ycbcr = rgb2ycbcr(temp);
%   temp_ycbcr(:,:,1) = temp_ycbcr(:,:,1).^Gamma;
%   out = ycbcr2rgb(temp_ycbcr);
  out = temp.^Gamma;
%   out = temp;
end