function [centerX1, centerY1, rows1, cols1] = rearrange_centers(centerX, centerY, rows, cols)
  rows1 = rows;
  cols1 = 2*cols;
  
  centerX1 = reshape(centerX, rows1, cols1);
  centerY1 = reshape(centerY, rows1, cols1);
  [centerX1, I] = sort(centerX1, 2);
  centerY1 = centerY1(:,I(1,:));
end