#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>
#include <limits>
// update a depth map, given a new Disparity Space Image
// to avoid keeping all DSIs in memory


// interpolate the input_image to get value at sx, sy
void interpolate(const float *input_image,  /* input image (height x width) */
                 float height, float width,
                 float sx, float sy,  /* location */
                 float* res /* ouput values */
                 ) {
  int img_size = height * width;
  float sx0 = floor(sx);
  float sx1 = sx0 + 1.0;
  float sy0 = floor(sy);
  float sy1 = sy0 + 1.0;

  int i;
  float I_00, I_01, I_10, I_11;

  // 00
  i = (int)sx0 * height + (int)sy0;
  if ( i > 0 && i < img_size)
    I_00 = input_image[i];
  else
    I_00 = 0.0;

  // 01
  i = (int)sx0 * height + (int) sy1;
  if ( i > 0 && i < img_size)
    I_01 = input_image[i];
  else
    I_01 = 0.0;

  // 10
  i = (int)sx1 * height + (int) sy0;
  if ( i > 0 && i < img_size)
    I_10 = input_image[i];
    else
    I_10 = 0.0;

  // 11
  i = (int)sx1 * height + (int) sy1;
  if ( i > 0 && i < img_size)
    I_11 = input_image[i];
  else
    I_11 = 0.0;

  // do interpolation
  float u = sx - sx0;
  float v = sy - sy0;

  *res  = (1 - u) * (1 - v) * I_00 +
          (1 - u) * (    v) * I_01 +
          (    u) * (1 - v) * I_10 +
          (    u) * (    v) * I_11;
}

// update current depth map given new DSI
void update_depth(const float *DSI, /* new disparity space image */
                    const float *prev_depth, /* previous depth estimation */
                    const float *prev_min_disparity, /* previous min disparity at each pixel */
                    float *depth,      /* updated depth map */
                    float *min_disparity, /* updated min disparity map */
                    const float *u,    /* precomputed u and v : local pixel coordinate */
                    const float *v,
                    int height, int width,
                    float distance, /* distance that was used to compute this disparity image */
                    float f) /* MLA - Sensor distance */
{
  float D = 1.0f + distance / f;
  
  for (int w = 0; w < width; w++) {
    for (int h = 0; h < height; h++) {
      int index = w * height + h;
      float ui = u[index];
      float vi = v[index];
      float sx = w - D * ui;
      float sy = h - D * vi;  
     
      // interpolate corresponding disparity map
      float disp;
      interpolate(DSI, height, width, 
                  sx, sy, &disp);
      float min_disp = prev_min_disparity[index];
      
      if (disp < min_disp) { // || min_disp == -1) {
        min_disparity[index] = disp;
        depth[index] = distance;
      } else {
        min_disparity[index] = min_disp;
        depth[index] = prev_depth[index];
      }
    }
  }
}
          

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  // depth = update_depth(depth, min_disparity, DSI, distance, u, v, mla_to_sensor)
  
  const int* dims = mxGetDimensions(prhs[0]);
  int height = dims[0];
  int width =  dims[1];

  // get previous depth map
  float *prev_depth = (float *) mxGetData(prhs[0]);
  
  // get previous min disparity map
  float *prev_min_disparity = (float *) mxGetData(prhs[1]);
  
  // get the disparity images
  float *DSI = (float *) mxGetData(prhs[2]);
  
  // get distance
  float distance = *((float *) mxGetData(prhs[3]));

  // get u, v array
  float *u = (float *) mxGetData(prhs[4]);
  float *v = (float *) mxGetData(prhs[5]);

  // distance from MLA to sensor
  float f = *((float *)mxGetData(prhs[6]));

  // allocate output

  // updated depth map
  plhs[0] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *depth = (float *) mxGetData(plhs[0]);

  // updated min disparity map
  plhs[1] = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
  float *min_disparity = (float *) mxGetData(plhs[1]);
  
  update_depth(DSI, prev_depth, prev_min_disparity,
                 depth, min_disparity, u, v,
                 height, width, distance, f);
}