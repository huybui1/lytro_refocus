% calibrate Lytro using white raw image
clear all
clc
close all

img = LFReadRaw('white_image_zoom_490.RAW', '12bit');
MICROLENS_PITCH = 13.8985996246337894177714e-6;
PIXEL_PITCH = 1.3999999761581418766809e-6;
ROI_WIN = 50; % to find peak

% demosaic
img = demosaic(img, 'bggr');
% convert to gray
img = im2double(img);
img = rgb2gray(img);

figure; imagesc(img);
colormap gray
axis image; axis off;

[height, width] = size(img);

A0 = MICROLENS_PITCH/PIXEL_PITCH*[1 0.5; 0 sqrt(3)/2];

% % filter by min filter
% min_filt = minFilter(raw_img, min_filt_width);
% 
% % threshold
% thresh = prctile(min_filt(:), threshold_param);
% thresh_img = zeros(size(min_filt));
% thresh_img(min_filt <= thresh) = 1;
% 
% figure; imagesc(thresh_img); colormap gray;
% 
% raw_dft  = fftshift(log(abs(fft2(thresh_img))));
raw_dft = fftshift(log(abs(fft2(img))));
figure(100); imagesc(raw_dft); colormap gray;

inv_A_trans = inv(A0)'*height;
hPeak0 = inv_A_trans*[0; 1]
vPeak0 = inv_A_trans*[1; 0]

figure(100);
hold on;
plot(width/2, height/2, 'bo');

hRoi.y = floor(hPeak0(2) + height/2 - ROI_WIN) + 1;
hRoi.x = floor(hPeak0(1) + width/2 - ROI_WIN) + 1;
hRoi.height = 2*ROI_WIN;
hRoi.width = 2*ROI_WIN;

figure(100); hold on;
line([hRoi.x, hRoi.x + hRoi.width], [hRoi.y, hRoi.y]); hold on
line([hRoi.x, hRoi.x], [hRoi.y, hRoi.y + hRoi.height]); hold on
line([hRoi.x + hRoi.width, hRoi.x + hRoi.width], [hRoi.y, hRoi.y + hRoi.height]); hold on
line([hRoi.x, hRoi.x + hRoi.width], [hRoi.y + hRoi.height, hRoi.y + hRoi.height]); hold off

hMaxes = findLocalMax(raw_dft,ROI_WIN,hRoi);

vRoi.y = floor(vPeak0(2) + height/2 - ROI_WIN) + 1;
vRoi.x = floor(vPeak0(1) + width/2 - ROI_WIN) + 1;
vRoi.height  = 2*ROI_WIN;
vRoi.width = 2*ROI_WIN;

figure(100); hold on;
line([vRoi.x, vRoi.x + vRoi.width], [vRoi.y, vRoi.y]); hold on
line([vRoi.x, vRoi.x], [vRoi.y, vRoi.y + vRoi.height]); hold on
line([vRoi.x + vRoi.width, vRoi.x + vRoi.width], [vRoi.y, vRoi.y + vRoi.height]); hold on
line([vRoi.x, vRoi.x + vRoi.width], [vRoi.y + vRoi.height, vRoi.y + vRoi.height]); hold off

vMaxes = findLocalMax(raw_dft,ROI_WIN,vRoi);
  
% threshold
meanValue = mean(raw_dft(:));
stdValue = std(raw_dft(:));
th = meanValue+ 3*stdValue;

maxesTh = (hMaxes==1 | vMaxes==1) & raw_dft>=th ;   

% sub pixel precision
options.RAW_SIZE_ROW = height;
options.RAW_SIZE_COL = width;

[index Xsub Ysub] = subPixMax(raw_dft,maxesTh,options);

 Xsub = Xsub - floor(width/2)-1;
 Ysub = Ysub - floor(height/2)-1;
    
 % find best one
 m = min( abs((Xsub - hPeak0(2)).^2 + (Ysub - hPeak0(1)).^2) );
 index = find(m ==  abs((Xsub - hPeak0(2)).^2 + (Ysub - hPeak0(1)).^2) );
 hPeak(2) = Ysub(index);
 hPeak(1) = Xsub(index);
    
 m = min( abs((Xsub - vPeak0(2)).^2 + (Ysub - vPeak0(1)).^2) );
 index = find(m == abs((Xsub - vPeak0(2)).^2 + (Ysub - vPeak0(1)).^2));
 vPeak(2) = Ysub(index);
 vPeak(1) = Xsub(index);
 
 figure(100);
 hold on;
 plot(hPeak(1) + width/2, hPeak(2) + height/2, 'r*');
 
 hold on;
 plot(vPeak(1) + width/2, vPeak(2) + height/2, 'r*');
  
 invAT = [hPeak(1), vPeak(1); hPeak(2) vPeak(2)];
 A = inv(invAT)'*width;
 
% find t
corners = [0 0 width width; 0 height height 0];  % 4 corners of raw image
pre_corners = inv(A)*corners;

min_x = floor(min(pre_corners(1,:)));
max_x = ceil(max(pre_corners(1,:)));
min_y = floor(min(pre_corners(2,:)));
max_y = ceil(max(pre_corners(2,:)));

loc_x = []; loc_y = [];
for iy = min_y:1:max_y
    for ix = min_x:1:max_x
        loc = A*[ix; iy];
        if (loc(1) > 0 && loc(1) <= width && ...
            loc(2) > 0 && loc(2) <= height) 
          loc_x = [loc_x; loc(1)];
          loc_y = [loc_y; loc(2)];
        end        
    end
end

[XX, YY] = meshgrid(1:width, 1:height);
dt = 1;

all_tx = -5:dt:5;
all_ty = -5:dt:5;
cost = zeros(length(all_ty), length(all_tx));
for iy = 1:length(all_ty)
    for ix = 1:length(all_tx)
        ty = all_ty(iy);
        tx = all_tx(ix);
        
        Ct_x = loc_x + tx;
        Ct_y = loc_y + ty;
        
%         shift1 = A*[1/3; -2/3];
%         V1_x = Ct_x + shift1(1);
%         V1_y = Ct_y + shift1(2);       
%         I_V1 = interp2(XX, YY, raw_img, V1_x, V1_y);
%         
%         shift2 = A*[-1/3; 2/3];
%         V2_x = Ct_x + shift2(1);
%         V2_y = Ct_y + shift2(2);      
%         I_V2 = interp2(XX, YY, raw_img, V2_x, V2_y);
        vals = interp2(XX, YY, img, Ct_x, Ct_y);
%         cost(iy, ix) = sum(I_V1(:).^2) + sum(I_V2(:).^2);
        cost(iy, ix) = sum(vals(~isnan(vals)).^2);
    end
end

figure; imagesc(cost);
[max_cost, ind]  = max(cost(:));
[iy, ix] = ind2sub(size(cost), ind);
tx = all_tx(ix)
ty = all_ty(iy)

Ct_x = loc_x + tx;
Ct_y = loc_y + ty;
figure; imagesc(img); colormap gray;
hold on;
plot(Ct_x, Ct_y, 'r.');

[centerX, centerY, height, width] = rearrange_centers(Ct_x, Ct_y);
save('center.mat', 'centerX', 'centerY', 'width', 'height');
