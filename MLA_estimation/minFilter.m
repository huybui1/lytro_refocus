function out = minFilter(in, width)
    rad = (width - 1)/2;
    
    [h, w] = size(in);
    out = zeros(h, w);
    for r = 1:h
        for c = 1:w
            min_x = max(1, c - rad);
            max_x = min(w, c + rad);
            max_y = min(h, r + rad);
            min_y = max(1, r - rad);
            
            patch = in(min_y:max_y, min_x:max_x);
            out(r,c) = min(patch(:));
        end
    end
end