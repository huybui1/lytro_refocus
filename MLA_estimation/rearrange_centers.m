function [center_x, center_y, height, width] = rearrange_centers(cx, cy)
  height = 1;
  width = 1;
  
  max_w = 0;
  prev_x = 0;
  
  for i = 1:length(cx)
     x = cx(i);
     y = cy(i);
     
     if (x < prev_x)
         height = height + 1;
         max_w = max(max_w, width);
         width = 1;
     else
         width = width + 1;
     end
     prev_x = x;
  end
  width = max_w;
  center_x = zeros(height, width);
  center_y = zeros(height, width);
  
  row = 1;
  col = 1;
  prev_x = 0;
  for i = 1:length(cx)
     x = cx(i);
     y = cy(i);
     
     if (x < prev_x)
         row = row + 1;
         col = 1;
     end
     prev_x = x;
     center_x(row, col) = x;
     center_y(row, col) = y;
     col = col + 1;
  end  
end