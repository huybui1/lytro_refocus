function x = surface3(y, C0)
    x(y >= C0) = -0.75;
    x(y < C0) = - 0.95;
end