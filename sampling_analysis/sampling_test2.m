% Test sampling a planar surface with light-field camera
% slanted surface
close all
clc
clear all

% Parameters
lens_pitch = 14e-6;
% pixel_pitch = 1.4e-6;
pixel_pitch = lens_pitch/9;
mu = 25e-6; % MLA - Sensor Distance
F = 19.8281e-3; % Main lens focal lens in meters
L = 20.3795e-3; % Main lens - MLA distance

raw_resolution = 55;
raw_size = raw_resolution*pixel_pitch;
C0 = raw_size / 2;

num_centers = floor(raw_size/lens_pitch) + 1;
centers = [1:num_centers]*lens_pitch - lens_pitch/2;

pixels = 1:raw_resolution;
x = pixels*pixel_pitch - pixel_pitch /2;
center_index = floor(x/lens_pitch) + 1;
center = centers(center_index);
main_lens_intersections = center - L/mu*(x - center);

figure;
scatter((L + mu)*ones(1, raw_resolution), x, 'b.');
hold on;
scatter(L*ones(1, raw_resolution), center, 'r.');
% hold on;
% plot([0, 0], [0, raw_size],  'b-');
% axis([-0.5, 0.3, 0, raw_size]);

hold on;
colors = ['y', 'g', 'b', 'm', 'k', 'r', 'c'];


% Surface defined by 2 points p1, p2
xp1 = -0.85
yp1 = 5e-3;

xp2 = -0.95;
yp2 = -5e-3;

hold on;
plot([xp1, xp2], [yp1, yp2], 'b');
hold on
plot([0, 0], [-10e-3, 10e-3], 'k');

for i = 1:length(x)
   plot( [L + mu, 0], [x(i), main_lens_intersections(i)], colors(mod(center_index(i), length(colors))+1));
   hold on;
   

   beta = (main_lens_intersections(i) - C0)/F + (x(i) - center(i))/mu;
   k = (main_lens_intersections(i) - yp1 + beta*xp1)/(yp2 - yp1 - beta*(xp2 - xp1));
   
   yp = yp1 + k*(yp2 - yp1);
   xp = xp1 + k*(xp2 - xp1);
   
   hold on
      
   plot( [0, xp], ...
         [main_lens_intersections(i), yp], ...
       colors(mod(center_index(i), length(colors))+1));   
end

xp1_i = 1/(1/F - 1/abs(xp1));
yp1_i = C0 - xp1_i/abs(xp1)*(yp1 - C0);

xp2_i = 1/(1/F - 1/abs(xp2));
yp2_i = C0 - xp2_i/abs(xp2)*(yp2 - C0);

hold on
plot([xp1_i, xp2_i], [yp1_i, yp2_i], 'b-');

% hold on
% plot([xp1, xp1_i], [yp1, yp1_i]);
% 
% hold on
% plot([-F, -F], [-10e-3, 10e-3]);
% 
% hold on
% plot([xp2, xp2_i], [yp2, yp2_i]);

