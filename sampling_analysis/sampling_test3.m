% Test sampling a planar surface with light-field camera
% slanted surface
close all
clc
clear all

% Parameters
lens_pitch = 14e-6;
% pixel_pitch = 1.4e-6;
pixel_pitch = lens_pitch/9;
mu = 25e-6; % MLA - Sensor Distance
F = 19.8281e-3; % Main lens focal lens in meters
L = 20.3795e-3; % Main lens - MLA distance

raw_resolution = 60;
raw_size = raw_resolution*pixel_pitch;
C0 = raw_size / 2;

num_centers = floor(raw_size/lens_pitch) + 1;
centers = [1:num_centers]*lens_pitch - lens_pitch/2;

pixels = 1:raw_resolution;
x = pixels*pixel_pitch - pixel_pitch /2;
center_index = floor(x/lens_pitch) + 1;
center = centers(center_index);
main_lens_intersections = center - L/mu*(x - center);

figure;
scatter((L + mu)*ones(1, raw_resolution), x, 'b.');
hold on;
scatter(L*ones(1, raw_resolution), center, 'r.');
% hold on;
% plot([0, 0], [0, raw_size],  'b-');
% axis([-0.5, 0.3, 0, raw_size]);

hold on;
colors = ['y', 'g', 'b', 'm', 'k', 'r', 'c'];


% % Surface defined by 2 points p1, p2
% xp1 = -0.85
% yp1 = 5e-3;
% 
% xp2 = -0.95;
% yp2 = -5e-3;

% surface
ys = -4e-3:1e-6:4e-3;
xs = surface3(ys, C0);
xs_i = 1./(1/F - 1./abs(xs));
ys_i = C0 - xs_i./abs(xs).*(ys - C0);


hold on
plot(xs, ys, 'k-', 'LineWidth', 1.5);

hold on
plot(xs_i, ys_i, 'b-', 'LineWidth', 1.5);
D = 1.0;
Di = 1/(1/F - 1/D);
hold on
plot([0, 0], [-6e-3, 6e-3], 'k');

for i = 1:length(x)
   plot( [L + mu, 0], [x(i), main_lens_intersections(i)], colors(mod(center_index(i), length(colors))+1));
   hold on;
   

   beta = (main_lens_intersections(i) - C0)/F + (x(i) - center(i))/mu;
   
   hold on
      
   plot( [0, -D], ...
         [main_lens_intersections(i), main_lens_intersections(i) ... 
          - D*((main_lens_intersections(i) - C0)/F + (x(i) - center(i))/mu) ], ...
       colors(mod(center_index(i), length(colors))+1));    
end

xp1_i = 1/(1/F - 1/abs(xp1));
yp1_i = C0 - xp1_i/abs(xp1)*(yp1 - C0);

xp2_i = 1/(1/F - 1/abs(xp2));
yp2_i = C0 - xp2_i/abs(xp2)*(yp2 - C0);
% 
% hold on
% plot([xp1_i, xp2_i], [yp1_i, yp2_i], 'b-');

hold on
plot([xp1, xp1_i], [yp1, yp1_i]);

% hold on
% plot([-F, -F], [-10e-3, 10e-3]);

hold on
plot([xp2, xp2_i], [yp2, yp2_i]);

axis equal
