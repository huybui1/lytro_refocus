% Test sampling a planar surface with light-field camera
close all
clc
clear all

% Parameters
lens_pitch = 14e-6;
% pixel_pitch = 1.4e-6;
pixel_per_microlens = 4;
num_microlens = 3;
pixel_pitch  = lens_pitch/pixel_per_microlens;
mu = 25e-6; % MLA - Sensor Distance
F = 19.8281e-3; % Main lens focal lens in meters
L = 20.3795e-3; % Main lens - MLA distance

raw_resolution = pixel_per_microlens*num_microlens;
raw_size = raw_resolution*pixel_pitch;
C0 = raw_size / 2

num_centers = floor(raw_size/lens_pitch) + 1;
centers = [1:num_centers]*lens_pitch - lens_pitch/2;

pixels = 1:raw_resolution;
x = pixels*pixel_pitch - pixel_pitch /2;
center_index = floor(x/lens_pitch) + 1;
center = centers(center_index);
main_lens_intersections = center - L/mu*(x - center);

figure;
scatter((L + mu)*ones(1, raw_resolution), x, 'b.');
hold on;
scatter(L*ones(1, raw_resolution), center, 'r.');
% hold on;
% plot([0, 0], [0, raw_size],  'b-');
% axis([-0.5, 0.3, 0, raw_size]);

hold on;
colors = ['y', 'g', 'b', 'm', 'k', 'r', 'c'];

% D = 1/(1/F - 1/L);
D = 220e-2; % plane at distance 50 cm in front of camera
Di = 1/(1/F - 1/D);
for i = 1:length(x)
   if (L + mu > Di)
       plot( [L + mu, 0], [x(i), main_lens_intersections(i)], colors(mod(center_index(i), length(colors))+1));
   else
       plot( [Di, 0], [center(i) + (x(i) - center(i))*(Di - L)/mu, main_lens_intersections(i)], ...
             colors(mod(center_index(i), length(colors))+1));
   end
   hold on;
   
   hold on  
%    plot( [0, -F], [main_lens_intersections(i), C0 - F/mu*(x(i) - center(i))],colors(mod(center_index(i), length(colors))+1));
   plot( [0, -D], ...
         [main_lens_intersections(i), main_lens_intersections(i) ... 
          - D*((main_lens_intersections(i) - C0)/F + (x(i) - center(i))/mu) ], ...
       colors(mod(center_index(i), length(colors))+1));   
    
   
end
hold on
plot([Di, Di], [-10e-3, 10e-3], 'r--');
hold on
plot([0, 0], [-10e-3, 10e-3], 'k');
hold on
plot([-D, -D], [-10e-3, 10e-3], 'b-');
