function x = surface(y, C0)
    x = -1000*(y - C0).^2 - 0.8;
end